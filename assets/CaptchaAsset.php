<?php

namespace app\assets;

use yii\web\AssetBundle;


class CaptchaAsset extends AssetBundle {
    public $sourcePath = '@webroot/js/pohjacaptcha/';
    public $css = [
        'puzzleCaptcha/puzzleCAPTCHA.css',
        'pohjacaptcha.css',
    ];
    public $js = [
        'puzzleCaptcha/puzzleCAPTCHA.js',
        'pohjacaptcha.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
