<?php

namespace app\assets;

use yii\web\AssetBundle;


class FontAwesomeAsset extends AssetBundle {
    public $sourcePath = '@webroot/css/FontAwesome';
    public $css = [
        'css/font-awesome.min.css',
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
