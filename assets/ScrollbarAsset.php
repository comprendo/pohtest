<?php

namespace app\assets;

use yii\web\AssetBundle;


class ScrollbarAsset extends AssetBundle
{
    public $sourcePath = '@webroot/vendors/plugins/mcustom-scrollbar/';
    public $css = [
        'mcustom_scrollbar.min.css',
    ];
    public $js = [
        'jquery.mCustomScrollbar.concat.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
