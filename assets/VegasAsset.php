<?php

namespace app\assets;

use yii\web\AssetBundle;


class VegasAsset extends AssetBundle {
    public $sourcePath = '@webroot/vendors/plugins/vegas';
    public $css = [
        'vegas.min.css',
    ];
    public $js = [
        'vegas.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
