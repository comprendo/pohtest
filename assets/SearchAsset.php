<?php

namespace app\assets;

use yii\web\AssetBundle;


class SearchAsset extends AssetBundle {
    public $sourcePath = '@webroot/vendors/plugins/autocomplete';
    public $css = [
        //'vegas.min.css',
    ];
    public $js = [
        'jquery.autocomplete.min.js',
        'search.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
    ];
}
