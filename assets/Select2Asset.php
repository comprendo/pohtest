<?php

namespace app\assets;

use yii\web\AssetBundle;


class Select2Asset extends AssetBundle {
    public $sourcePath = '@webroot/vendors/plugins/select2-403';
    public $css = [
        'css/select2.min.css',
    ];
    public $js = [
        'js/select2.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
