$(function() {
    var ww = ( $(window).width() < window.screen.width ) ? $(window).width() : window.screen.width; //get proper width
    var mw = 500; // min width of site
    var ratio =  ww / mw; //calculate ratio
    if( ww < mw){ //smaller than minimum size
        var ua = navigator.userAgent.toLowerCase();

        var is_android = ua.indexOf("android") > -1;
        var is_firefox = ua.indexOf('firefox') > -1;

        $('#viewport').attr('content', 'initial-scale=' + ratio + ', maximum-scale=' + ratio + ', minimum-scale=' + ratio + ', user-scalable=yes, width=' + ww);
        if(is_firefox && is_android) {

        }
    }else{ //regular size
        $('#viewport').attr('content', 'initial-scale=1.0, maximum-scale=2, minimum-scale=1.0, user-scalable=yes, width=' + ww);
    }

    $('.preloader').fadeOut(350).promise().done(function() {
        $('.preloader').addClass('hidden');
    });
});