var isMobile = {
    Android: function() {return navigator.userAgent.match(/Android/i);},
    BlackBerry: function() {return navigator.userAgent.match(/BlackBerry/i);},
    iOS: function() {return navigator.userAgent.match(/iPhone|iPad|iPod/i);},
    Opera: function() {return navigator.userAgent.match(/Opera Mini/i);},
    Windows: function() {return navigator.userAgent.match(/IEMobile/i);},
    any: function() {return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());}
};

$(function() {

    /* ----------------------- mobile check ----------------------- */
    //if(isMobile.any() || (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1)){
    //if(isMobile.any()){
        $("#slideshow").html('').vegas({
            delay: 7000,
            timer: true,
            shuffle: true,
            animation: 'random',
            transitionDuration: 2000,
            slides: [
                { src: "/images/site/slidew0.jpg" },
                { src: "/images/site/slidew1.jpg" },
                { src: "/images/site/slidew2.jpg" },
                { src: "/images/site/slidew3.jpg" },
                { src: "/images/site/slidew4.jpg" },
                { src: "/images/site/slidew5.jpg" },
                { src: "/images/site/slidew6.jpg" },
                { src: "/images/site/slidew7.jpg" },
                { src: "/images/site/slidew8.jpg" },
                { src: "/images/site/slidew9.jpg" },
                { src: "/images/site/slidew10.jpg" },
                { src: "/images/site/slidew11.jpg" },
            ]
        });
    //} else {
    //   $("#slideshow").html('<video width="100%" height="auto" autoplay="autoplay" loop="loop" controls="true" preload="auto" muted="muted"><source src="/video/intro.mp4" type="video/mp4"></source><source src="/video/intro.webm" type="video/webm"></source></video>');
        //$("#slideshow").html('<iframe src="https://www.youtube.com/embed/pT1mQ-xEAnU?controls=0&showinfo=0&rel=0&autoplay=1&loop=1&playlist=pT1mQ-xEAnU" frameborder="0" allowfullscreen></iframe>');
    //}
    /* ----------------------- mobile check ----------------------- */

    $('.burger').click(function(){
        if($('.burger').hasClass("cross")){
            $(".burger").removeClass("cross");
            $(".nav").removeClass("opened");
        }else{
            $(".burger").addClass("cross");
            $(".nav").addClass("opened");
        }
    });

    // $('.modal-news').click(function () {
    //     $(this).fadeOut(300);
    // });
});
