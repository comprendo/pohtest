$(function() {
    $('#city_select').select2({
        width: "160px",
        "language": {
            "noResults": function(){
                return "Ничего не найдено";
            }
        },
    });
    $('#city_select').on("change", function(e) {
        //window.location.href = "/discount?city="+$('#city_select').val();
        window.location.href = "?city="+$('#city_select').val();
    });
});