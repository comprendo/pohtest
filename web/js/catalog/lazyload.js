$(function() {
    $('.btn-lazy-load').click(function(e){
        e.preventDefault();

        if($(this).hasClass('all')){
            var pages = $(this).data('pages');
            $.ajax({
                url: "/lazyload",
                type: "GET", //send it through get method
                data: {
                    code: "fegehebug8",
                    category: category,
                    page: page++,
                    display: display,
                    sort: sort,
                    q: q,
                    limit: 9,
                    all: 1
                },
                success: function(response) {
                    if(response){
                        $('.items').append(response);
                    }
                    $('.btn-lazy-load').fadeOut();
                    page = pages;
                },
                error: function(xhr) {
                    //Do Something to handle error
                }
            });
        } else {
            $.ajax({
                url: "/lazyload",
                type: "GET", //send it through get method
                data: {
                    code: "fegehebug8",
                    category: category,
                    page: page++,
                    display: display,
                    sort: sort,
                    q: q,
                    limit: 9
                },
                success: function(response) {
                    if(response){
                        $('.items').append(response);
                    } else {
                        $('.btn-lazy-load').fadeOut();
                    }
                },
                error: function(xhr) {
                    //Do Something to handle error
                }
            });
        }
    })
});