$(function() {

    $(".product-slider").owlCarousel({

        loop: ($(".product-slider").children().length>1)?true:false,
        nav: true,
        dots: true,
        navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],

        items : 1,
        itemsDesktop : 1,
        itemsDesktopSmall : 1,
        itemsTablet: 1,
        itemsMobile : 1,

        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true

    });


    $('body').on('click', '.material-link', function(e) {
        e.preventDefault();

        var src = $(this);
        $(".material-modal").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});
        $(".material-modal .thumb").css('background-image', 'url(' + src.attr('data-image') + ')');
        $(".material-modal .code").html( src.find('.code').html() );
        $(".material-modal .name").html( src.find('.name').html() );
        $(".material-modal .longdesc").html( src.find('.longdesc').html() );
    });

    $(".material-modal .btn-close").click(function(){
        $(".material-modal").fadeOut();
        $("body").css({overflow:"auto"});
    });

    //---------------------------------------------------------------------------


    $('body').on('click', '.mechanism-link', function(e) {
        e.preventDefault();

        var videoID = $(this).attr('data-video');

        if(videoID){
            $(".mechanism-modal").fadeIn().css("top", 0);
            $("body").css({overflow:"hidden"});
            $(".mechanism-modal .video-container").html('<iframe width="854" height="480" src="//www.youtube.com/embed/'+videoID+'?modestbranding=1&autoplay=1&loop=1&showinfo=0&controls=0&playlist='+videoID+'" frameborder="0" allowfullscreen>')
        }
    });

    $(".mechanism-modal .btn-close").click(function(){
        $(".mechanism-modal").fadeOut();
        $("body").css({overflow:"auto"});
        $(".mechanism-modal .video-container").html('');
    });

    //---------------------------------------------------------------------------

    $('.product-slider .item .zoom').click(function(){
        var photoID = $(this).attr('data-image');

        if(photoID){
            $(".photo-modal").fadeIn().css("top", 0);
            $("body").css({overflow:"hidden"});
            $(".photo-modal .photo-container").html('<img src="'+photoID+'" alt=""/>');
        }
    });

    $(".photo-modal .btn-close").click(function(){
        $(".photo-modal").fadeOut();
        $("body").css({overflow:"auto"});
        $(".photo-modal .photo-container").html('');
    });

    //------------------------------------------------------------
    if(success){
        $('.preloader-feedback').fadeOut(500).promise().done(function() {
            $('.preloader-feedback').addClass('hidden');
        });

        $(".order-modal").show().css("top", 0);
        $("body").css({overflow:"hidden"});
    }

    $('.btn-order').click(function(e){
        e.preventDefault();

        $(".order-modal").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});
    });

    $(".order-modal .btn-close").click(function(){
        $(".order-modal").fadeOut();
        $("body").css({overflow:"auto"});
    });

    $('.leave-order-scroll').click(function(e) {
        e.preventDefault();

        $('html, body').animate({ scrollTop: $('#btn-leave-order').offset().top - 200 }, 1000, function() {
            $('#btn-leave-order').css('background-color', '#d2a637').css('color', 'white');
            setTimeout(function() {
                $('#btn-leave-order').css('background-color', 'rgba(210, 166, 55, 0)').css('color', '#d2a637');
                $('#btn-leave-order').removeAttr('style');
            }, 400);
        });
    });
});