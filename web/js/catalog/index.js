$(function() {
    $('#display_select').select2({
        width: "160px",
        minimumResultsForSearch: -1
    });
    $('#display_select').on("change", function(e) {
        window.location.href = "?display="+$('#display_select').val();
    });
    $('#sort_select').select2({
        width: "160px",
        minimumResultsForSearch: -1
    });
    $('#sort_select').on("change", function(e) {
        if($('#display_select').val()!=0)
            window.location.href = "?display="+$('#display_select').val()+"&sort="+$('#sort_select').val();
        else
            window.location.href = "?sort="+$('#sort_select').val();
    });

    $(document).on('click', '.item', function (e) {
        history.pushState({
            'page': page,
            'item_id': $(this).attr('id')
        }, '', '?page=' + page);
    });
});