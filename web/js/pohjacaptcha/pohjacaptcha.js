$(function() {


    $("#pohjacaptcha .pbtn").click(function () {

        if(!$("#pohjacaptcha").hasClass('checked')){

            $("#pohjacaptcha .popup").toggle();
            if(!$("#pohjacaptcha").hasClass('opened')){
                $("#PuzzleCaptcha").html('');
                $("#PuzzleCaptcha").PuzzleCAPTCHA({
                    imageURL: '/js/pohjacaptcha/puzzleCaptcha/images/'+randomInteger(1, 17)+'.png',
                    columns:3,
                    rows:3,
                    targetInput:'.pct',
                    targetVal:'sxb6xco8x9',
                    targetButton:'.btnSubmit'
                });
                if(!$("#pohjacaptcha").hasClass('offer')){
                    $("#pohjacaptcha #feedbackform-pct").remove();
                    $("#pohjacaptcha").append('<input type="hidden" id="feedbackform-pct" class="pct" name="FeedbackForm[pct]" value="bc4d3qtn5f">');
                } else {
                    $("#pohjacaptcha #offerform-pct").remove();
                    $("#pohjacaptcha").append('<input type="hidden" id="offerform-pct" class="pct" name="OfferForm[pct]" value="bc4d3qtn5f">');
                }

            }
            $("#pohjacaptcha").toggleClass('opened');

        }

    })

});

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}