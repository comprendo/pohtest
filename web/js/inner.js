$(function () {
    $("#err-slideshow").html('').vegas({
        delay: 7000,
        timer: false,
        shuffle: true,
        animation: 'random',
        transitionDuration: 2000,
        slides: [{
                src: "/images/site/slidew0.jpg"
            },
            {
                src: "/images/site/slidew1.jpg"
            },
            {
                src: "/images/site/slidew2.jpg"
            },
            {
                src: "/images/site/slidew3.jpg"
            },
            {
                src: "/images/site/slidew4.jpg"
            },
            {
                src: "/images/site/slidew5.jpg"
            },
            {
                src: "/images/site/slidew6.jpg"
            },
            {
                src: "/images/site/slidew7.jpg"
            },
            {
                src: "/images/site/slidew8.jpg"
            },
            {
                src: "/images/site/slidew9.jpg"
            },
            {
                src: "/images/site/slidew10.jpg"
            },
            {
                src: "/images/site/slidew11.jpg"
            },
        ]
    });

    $(".hero").vegas({
        delay: 3000,
        timer: false,
        shuffle: true,
        loop: false,
        animation: 'kenburns',
        transitionDuration: 3000,
        slides: [{
            src: herobg
        }]
    });

    $('a.btn-scrolltop').click(function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 'fast');
    });

    /* ------------------- catalog search ------------------- */
    $('#search[data-search=catalog]').autocomplete({
        serviceUrl: '/catalog/search',
        onSelect: function (suggestion) {},
        onSearchComplete: function (query, suggestions) {
            $.each(suggestions, function (key, value) {
                //console.log(value);
                $('.autocomplete-suggestion[data-index=' + key + ']').prepend('<div class="autocomplete-thumb" style="background-image: url(' + value.data.img + ')"/>');
                $('.autocomplete-suggestion[data-index=' + key + ']').wrapInner('<a href="/catalog/model/' + value.data.alias + '"></a>');

            });
        }
    });
    $('#search[data-search=catalog]').bind("enterKey", function (e) {
        window.location.href = "?q=" + $(this).val();
    });
    $('#search[data-search=catalog]').keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });
    /* ------------------- catalog search ------------------- */


    /* ------------------- discount search ------------------- */
    $('#search[data-search=discount]').autocomplete({
        serviceUrl: '/discount/search',
        onSelect: function (suggestion) {},
        onSearchComplete: function (query, suggestions) {
            $.each(suggestions, function (key, value) {
                $('.autocomplete-suggestion[data-index=' + key + ']').prepend('<div class="autocomplete-thumb" style="background-image: url(' + value.data.img + ')"/>');
                $('.autocomplete-suggestion[data-index=' + key + ']').append('<div class="autocomplete-price"><div class="autocomplete-pricenew">' + value.data.pricenew + ' Р</div><div class="autocomplete-priceold">' + value.data.priceold + ' Р</div></div>');
                $('.autocomplete-suggestion[data-index=' + key + ']').wrapInner('<a href="/discount/model/' + value.data.alias + '"></a>');

            });
        }
    });
    $('#search[data-search=discount]').bind("enterKey", function (e) {
        window.location.href = "?q=" + $(this).val();
    });
    $('#search[data-search=discount]').keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });
    /* ------------------- discount search ------------------- */

    /* ------------------- material search ------------------- */
    $('#search[data-search=material]').autocomplete({
        serviceUrl: '/special/search',
        onSelect: function (suggestion) {},
        onSearchComplete: function (query, suggestions) {
            $.each(suggestions, function (key, value) {
                $('.autocomplete-suggestion[data-index=' + key + ']').prepend('<div class="autocomplete-thumb" style="background-image: url(' + value.data.img + ')"/>');
                $('.autocomplete-suggestion[data-index=' + key + ']').wrapInner('<a href="/special/material?id=' + value.data.id + '"></a>');

            });
        }
    });
    $('#search[data-search=material]').bind("enterKey", function (e) {
        //window.location.href = "?q="+$(this).val();
    });
    $('#search[data-search=material]').keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });
    /* ------------------- material search ------------------- */

    $('.phone_rus').mask('+7 (000) 000-00-00');

    $('.burger').click(function () {
        if ($('.burger').hasClass("cross")) {
            $(".burger").removeClass("cross");
            $(".nav").removeClass("opened");
        } else {
            $(".burger").addClass("cross");
            $(".nav").addClass("opened");
        }
    });

    $('.submenu .sub-cat').click(function (e) {
        e.preventDefault();
        var subitem = $(this).parent('.sub-item');
        if (subitem.hasClass('active')) {
            $('.submenu .sub-item').removeClass('active');
        } else {
            $('.submenu .sub-item').removeClass('active');
            subitem.addClass('active');
        }
    });
    $('.submenu .sub-item .dropdown .btn-close').click(function (e) {
        $(this).closest('.sub-item').removeClass('active');
    });

    setTimeout(function () {
        $("#feedbackform-captcha-image").trigger('click');
    }, 0);

});

$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 300) {
        $('a.btn-scrolltop').addClass('shown');
        $('a.btn-back-fixed').addClass('shown');
    } else {
        $('a.btn-scrolltop').removeClass('shown');
        $('a.btn-back-fixed').removeClass('shown');
    }
});

function historyBackFallback(fallbackUrl) {
    fallbackUrl = fallbackUrl || '/';
    var prevPage = window.location.href;

    window.history.go(-1);

    setTimeout(function () {
        if (window.location.href == prevPage) {
            window.location.href = fallbackUrl;
        }
    }, 500);
}