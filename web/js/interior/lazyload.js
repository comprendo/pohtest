$(function() {
    $('.btn-lazy-load').click(function(e){
        e.preventDefault();

        $.ajax({
            url: "/lazyload",
            type: "GET", //send it through get method
            data: {
                code: "xubt7ew3ua",
                page: page++,
                limit: 4
            },
            success: function(response) {
                if(response){
                    $('.items').append(response);
                } else {
                    $('.btn-lazy-load').fadeOut();
                }
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
    })
});