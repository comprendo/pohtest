$(function() {
   $(".btn-run-timeline").click(function(e){
        e.preventDefault();

        $(".timeline").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});

        $(".timeline").mCustomScrollbar({
           theme: "minimal",
           scrollInertia: 0
        })
   });

    if(success){
        $('.preloader-feedback').fadeOut(500).promise().done(function() {
            $('.preloader-feedback').addClass('hidden');
        });

        $(".order-modal").show().css("top", 0);
        $("body").css({overflow:"hidden"});
    }

    $('.btn-order').click(function(e){
        e.preventDefault();

        $(".order-modal").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});
    });

    $(".order-modal .btn-close").click(function(){
        $(".order-modal").fadeOut();
        $("body").css({overflow:"auto"});
    });
});