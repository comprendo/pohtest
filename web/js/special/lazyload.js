$(function() {
    $('.materials-page .btn-lazy-load').click(function(e){
        e.preventDefault();
        var type = $(this).attr('data-type');

        $.ajax({
            url: "/lazyload",
            type: "GET", //send it through get method
            data: {
                code: "thut7ewusp",
                type: type,
                page: pages[type]++,
                producer: producer,
                limit: 8
            },
            success: function(response) {
                if(response){
                    $('.items-'+type).append(response);
                } else {
                    $('.btn-lazy-load[data-type='+type+']').fadeOut();
                }
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
    });

    $('.mechanisms-page .btn-lazy-load').click(function(e){
        e.preventDefault();
        var type = $(this).attr('data-type');

        $.ajax({
            url: "/lazyload",
            type: "GET", //send it through get method
            data: {
                code: "f3trarenux",
                type: type,
                page: pages[type]++,
                limit: 3
            },
            success: function(response) {
                if(response){
                    $('.items-'+type).append(response);
                } else {
                    $('.btn-lazy-load[data-type='+type+']').fadeOut();
                }
            },
            error: function(xhr) {
                //Do Something to handle error
            }
        });
    })
});