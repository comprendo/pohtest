$(function() {
    $('#producer_select').select2({
        width: "110px",
        "language": {
            "noResults": function(){
                return "Ничего не найдено";
            }
        },
    });
    $('#producer_select').on("change", function(e) {
        window.location.href = "?producer="+$('#producer_select').val();
    });

    $('body').on('click', '.material .btn-material.zoom', function() {
        var src = $(this).parents('.material');
        $(".material-modal").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});
        $(".material-modal .thumb").css('background-image', 'url(' + src.attr('data-image') + ')');
        $(".material-modal .code").html( src.find('.code').html() );
        $(".material-modal .name").html( src.find('.name').html() );
        $(".material-modal .longdesc").html( src.find('.longdesc').html() );
    });

    $(".material-modal .btn-close").click(function(){
        $(".material-modal").fadeOut();
        $("body").css({overflow:"auto"});
    });

    $('.tab-link').click(function(){
        $('.side-desc').hide();
        $('.'+this.hash.substr(1)+'-desc').show();
    });

    if (!!location.hash) {
        $('.'+window.location.hash.substr(1)+'-desc').show();
        return;
    }

    var link = document.querySelector('#tabs > .tab-link');
    if (link) link.click();

});