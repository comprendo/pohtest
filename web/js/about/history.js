$(function() {
   $(".btn-run-timeline").click(function(e){
        e.preventDefault();

        $(".timeline").fadeIn().css("top", 0);
        $("body").css({overflow:"hidden"});

        $(".timeline").mCustomScrollbar({
           theme: "minimal",
           scrollInertia: 0
        })
    });
});