$(function() {
    $(".timeline .btn-close").click(function(){
        $(".timeline").fadeOut();
        $("body").css({overflow:"auto"});
        $(".timeline").mCustomScrollbar('destroy');
    });
});