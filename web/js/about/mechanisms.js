$(function() {

    $('.mechanism').hover(function(){
        $('.item-desc h3').html($(this).find('.name').html());
        $('.item-desc h4').html($(this).find('.subline').html());
        $('.item-desc p').html($(this).find('.fulldesc').html());
        $('.item-desc').show();
    }, function(){
        $('.item-desc').hide();
    });

    $('body').on('click', '.mechanism', function() {
        var videoID = $(this).attr('data-video');

        if(videoID){
            $(".mechanism-modal").fadeIn().css("top", 0);
            $("body").css({overflow:"hidden"});
            $(".mechanism-modal .video-container").html('<iframe width="854" height="480" src="//www.youtube.com/embed/'+videoID+'?modestbranding=1&autoplay=1&loop=1&showinfo=0&controls=0&playlist='+videoID+'" frameborder="0" allowfullscreen>')
        }
    });

    $(".mechanism-modal .btn-close").click(function(){
        $(".mechanism-modal").fadeOut();
        $("body").css({overflow:"auto"});
        $(".mechanism-modal .video-container").html('');
    });

    $('.tab-link').click(function(){
        $('.side-desc').hide();
        $('.'+this.hash.substr(1)+'-desc').show();
    });

    if (!!location.hash) {
        $('.'+window.location.hash.substr(1)+'-desc').show();
        return;
    }

    var link = document.querySelector('#tabs > .tab-link');
    if (link) link.click();
});