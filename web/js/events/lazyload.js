$(function() {
    $('.btn-lazy-load').click(function(e){
        e.preventDefault();

        if($(this).hasClass('all')){
            $.ajax({
                url: "/lazyload",
                type: "GET", //send it through get method
                data: {
                    code: "thapequ3ug",
                    category: category,
                    page: page++,
                    limit: 3,
                    all: 1
                },
                success: function(response) {
                    if(response){
                        $('.items').append(response);
                    }
                    $('.btn-lazy-load').fadeOut();
                },
                error: function(xhr) {
                    //Do Something to handle error
                }
            });
        } else {
            $.ajax({
                url: "/lazyload",
                type: "GET", //send it through get method
                data: {
                    code: "thapequ3ug",
                    category: category,
                    page: page++,
                    limit: 3
                },
                success: function(response) {
                    if(response){
                        $('.items').append(response);
                    } else {
                        $('.btn-lazy-load').fadeOut();
                    }
                },
                error: function(xhr) {
                    //Do Something to handle error
                }
            });
        }

    })
});