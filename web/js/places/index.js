$(function() {
    $('#city_select').select2({
        width: "160px",
        /*sorter: function(results, container, query) {
            return results.sort(function(a, b) {
                if(parseInt(a.id)===56)
                    return 0;
                if(parseInt(a.id)===1)
                    return -1;
                if(parseInt(a.id)===3)
                    return -1;
                if(parseInt(a.id)===0)
                    return -1;

                a = a.text.toLowerCase();
                b = b.text.toLowerCase();
                if(a > b) {
                    return 1;
                } else if (a < b) {
                    return -1;
                }
                return 0;
            });
        }*/
        "language": {
            "noResults": function(){
                return "Ничего не найдено";
            }
        },
    });
    $('#country_select').select2({
        width: "160px",
        /*sorter: function(results, container, query) {
            return results.sort(function(a, b) {
                if(parseInt(a.id)===56)
                    return 0;
                if(parseInt(a.id)===1)
                    return -1;
                if(parseInt(a.id)===3)
                    return -1;
                if(parseInt(a.id)===0)
                    return -1;

                a = a.text.toLowerCase();
                b = b.text.toLowerCase();
                if(a > b) {
                    return 1;
                } else if (a < b) {
                    return -1;
                }
                return 0;
            });
        }*/
        "language": {
            "noResults": function(){
                return "Ничего не найдено";
            }
        },
    });
    $('#city_select').on("change", function(e) {
        if($('#city_select').val()!=0)
            window.location.href = "/places?country="+$('#country_select').val()+"&city="+$('#city_select').val();
        else
            window.location.href = "/places?country="+$('#country_select').val();
    });
    $('#country_select').on("change", function(e) {
        window.location.href = "/places?country="+$('#country_select').val();
    });
    $('.item-href').click(function(e){
        e.preventDefault();
    });
    new ClipboardJS('.copy-text');
    $(document).on('click', '.copy-text', function() {
        $('.tooltiptext').text('Скопировать');
        $(this).find('.tooltiptext').text('Скопировано');
    });
});