<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $title;

$this->registerScssFile("/css/events/index.scss");


$this->registerJsFile("/js/events/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

?>
<div class="events-grid">
    <div class="container">
        <div class="row items">
            <? foreach($items as $item):?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="item">
                        <a href="/events/article/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->image?>')">
                            <div class="date-badge">
                                <div class="day"><?=$item::d($item->date, 'dd')?></div>
                                <div class="month"><?=$item::d($item->date, 'MMMM')?>'<?=substr($item::d($item->date, 'y'), -2)?></div></div>
                            <div class="overlay"></div>
                            <? if($item->enddate):?>
                                <div class="timeleft">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>  <?=$item->getDaysLeft()?>
                                </div>
                            <? endif?>
                        </a>
                        <div class="desc">
                            <div class="name"><?=$item->name?></div>
                            <ul class="tags">
                                <li><i class="fa fa-folder-o" aria-hidden="true"></i> <a href="/events?category=<?=$item->category?>"><?=$item->getEventCategory()?></a></li>
                                <li><i class="fa fa-eye" aria-hidden="true"></i> <?=$item->views?></li>
                            </ul>
                            <div class="shortdesc"><?=$item->shortdesc?></div>
                            <a class="btn-more" href="/events/article/<?=$item->alias?>">Читать далее</a>
                        </div>
                    </div>
                </div>
            <? endforeach?>
        </div>

        <div class="row center-xs">
            <? if($items and count($items)==6):?>
                <a class="btn-lazy-load" href="#">Показать еще</a>
                <a class="btn-lazy-load all" href="#">Показать все</a>
            <? endif?>
        </div>
    </div>
</div>
<script>
    page = 2;
    category = <?= $category ? htmlspecialchars($category) : "0"?>
</script>