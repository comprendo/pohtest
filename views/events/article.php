<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->name;

$this->registerScssFile("/css/events/index.scss");

$this->registerJsFile("//static.addtoany.com/menu/page.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>

<div class="article-page">
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-11 col-md-9">
                <h2><?=$model->name?></h2>
                <ul class="tags">
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> <?=$model::d($model->date, 'd MMMM')?></li>
                    <li><i class="fa fa-folder-o" aria-hidden="true"></i> <a href="/events?category=<?=$model->category?>"><?=$model->getEventCategory()?></a></li>
                    <li><i class="fa fa-eye" aria-hidden="true"></i> <?=$model->views?></li>
                </ul>
                <div class="article-image" style="background-image: url(<?=$model->image?>)"></div>
                <div class="aa-social aa-center m-t-35">
                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_button_vk"></a>
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_viber"></a>
                        <a class="a2a_button_odnoklassniki"></a>
                        <a class="a2a_button_whatsapp"></a>
                    </div>
                    <!-- AddToAny END -->
                </div>
                <div class="description"><?=$model->text?></div>
            </div>
        </div>
    </div>
</div>