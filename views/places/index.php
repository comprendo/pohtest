<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Cities;
use app\models\Countries;

$this->title = 'Салоны';

$this->registerScssFile("/css/places/index.scss");

$mapcenter = [];

// эта штука с центром карты скорее всего уже не нужна - теперь карта зумится автоматом
if($itemcount = count($itemsmap)){
    foreach ($itemsmap as $item){
        $coords = explode(",",$item->coords);
        $mapcenter[0] += floatval($coords[0]);
        $mapcenter[1] += floatval($coords[1]);
    }

    $mapcenter[0] = $mapcenter[0]/$itemcount;
    $mapcenter[1] = $mapcenter[1]/$itemcount;
}


$this->registerJsFile("/js/places/index.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/places/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/places/clipboard.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

/*$cities = Cities::getArray('id','name');
asort($cities);

// костыль для сортировки городов
$keys = [1,3]; // 1 - питер, 2 - москва.
foreach ($keys as $key){

    if(isset($cities[$key])) {
        $value = $cities[$key];
        unset($cities[$key]);
        $cities = array($key=>$value) + $cities;
    }
}*/
$cities = Cities::find()->where(['country' => $country ? htmlspecialchars($country) : "1"]);
$cities->orderBy(['sort' => SORT_ASC]);

$countries = Countries::find();
$countries->orderBy(['sort' => SORT_ASC]);



// var_dump($url);
?>
<div class="places-grid">
    <div class="container">
        <div class="row filter-row middle-xs">
            <div class="col-xs-6">
                <h2><?=($city)?Cities::findOne(intval($city))->name:"Все города"?></h2>
            </div>
            <div class="col-xs filter">
                <label for="country_select">Выбрать страну:</label>
                <select name="country" id="country_select" class="_select2">
                    <? foreach($countries->all() as $item):?>
                        <option <?=($country and $country==$item->id)?"selected":"" ?> value="<?=$item->id?>"><?=$item->name?></option>
                    <? endforeach?>
                </select>
            </div>
            <div class="col-xs filter">
                <label for="city_select">Выбрать город:</label>
                <select name="city" id="city_select" class="_select2">
                    <option <?=($city)?"":"selected" ?> value="0">Все города</option>
                    <? foreach($cities->all() as $item):?>
                        <option <?=($city and $city==$item->id)?"selected":"" ?> value="<?=$item->id?>"><?=$item->name?></option>
                    <? endforeach?>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs">
                <div id="map"></div>
                <script>
                    function initMap() {

                        var styledMapType = new google.maps.StyledMapType(
                            [
                                {
                                    "featureType": "administrative",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit.line",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        },
                                    ]
                                },
                                {
                                    "featureType": "transit.station",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        },
                                        {
                                            "hue": "#ff0000"
                                        },
                                        {
                                            "saturation": "0"
                                        },
                                        {
                                            "lightness": "0"
                                        },
                                        {
                                            "gamma": "0.00"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#fcfcfc"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#fcfcfc"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#dddddd"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#dddddd"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.local",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#eeeeee"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        },
                                        {
                                            "color": "#dddddd"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#434343"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                }
                            ],
                            {name: 'Салоны'}
                        );

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: <?=$city?"11":"4"?>,
                            center: {lat: <?=$mapcenter[0]?>, lng: <?=$mapcenter[1]?>},
                            scrollwheel: false,
                            mapTypeControlOptions: {
                                mapTypeIds: ['styled_map']
                            },
                            fullscreenControl: true
                        });
                        map.mapTypes.set('styled_map', styledMapType);
                        map.setMapTypeId('styled_map');

                        var locations = [
                            <? foreach($itemsmap as $item):?>
                                <? $coords = explode(",",$item->coords); ?>
                                {coords: {lat: <?=$coords[0]?>, lng: <?=$coords[1]?>}, address: '<?=$item->name?>', content: '<?=$item->name?><br>Тел. <?=$item->phone?><br>Часы работы: <?=$item->schedule?>'},
                            <? endforeach?>
                        ]

                        var markers = [];
                        var latlng = [];
                        locations.map(function(location, i) {
                            var infowindow = new google.maps.InfoWindow({
                                content: location.content
                            });
                            var marker = new google.maps.Marker({
                                position: location.coords,
                                map: map,
                                title: location.address,
                                icon: '/images/places/marker.png'
                            });
                            google.maps.event.addListener(marker, 'click', function() {
                                infowindow.open(map,marker);
                            });
                            markers.push(marker);

                            latlng.push(new google.maps.LatLng(location.coords.lat, location.coords.lng));
                        });
                        var latlngbounds = new google.maps.LatLngBounds();
                        for (var i = 0; i < latlng.length; i++) {
                            latlngbounds.extend(latlng[i]);
                        }
                        map.fitBounds(latlngbounds);


                        <?/*var markerCluster = new MarkerClusterer(map, markers,
                            {
                                imagePath: '/images/places/cluster',
                                width: 45,
                                height: 45
                        });*/?>
                    }
                </script>
            </div>
        </div>
        <div class="row items">
            <? foreach($items as $item):?>
                <div class="col-xs-12 col-lg-6">
                    <a class="item-href">
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-5">
                                    <div class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                                        <div class="overlay"></div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-8 col-md-9 col-lg-7">
                                    <div class="desc" itemscope itemtype="http://schema.org/Organization">
                                        <div class="name" itemprop="name"><?=$item->name?></div>
                                        <ul class="tags">
                                            <li><i class="fa fa-phone" aria-hidden="true"></i> <span itemprop="telephone"><?=$item->phone?></span></li><br>
                                            <? if($item->email != null): ?>
                                                <li class="email"><i class="fa fa-at" aria-hidden="true"></i> <span class="copy-text tooltip" data-clipboard-text="<?=$item->email?>"><span itemprop="email"><?=$item->email?></span><span class="tooltiptext">Скопировать</span></span></li><br>
                                            <? endif ?>
                                            <li><i class="fa fa-calendar-o" aria-hidden="true"></i> <span itemprop="openingHours"><?=$item->schedule?></span></li>
                                        </ul>
                                        <!-- <div class="email"><?=$item->email?></div> -->
                                        <div class="address" itemprop="address"><?=$item->address?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            <? endforeach?>
        </div>

        <div class="row center-xs">
            <? if($items and count($items)==6):?>
                <a class="btn-lazy-load" href="#">Показать еще</a>
                <a class="btn-lazy-load all" href="#">Показать все</a>
            <? endif?>
        </div>
    </div>
</div>
<script>
    page = 3;
    city = <?= $city ? htmlspecialchars($city) : "0"?>;
    country = <?= $country ? htmlspecialchars($country) : "0"?>;
</script>

<?/*<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>*/?>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsbYX8zR9lfe7Sp24wFNSBhK-x3AIx2i4&callback=initMap">
</script>