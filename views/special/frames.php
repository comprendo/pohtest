<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Эксклюзив';

$this->registerScssFile("/css/special/index.scss");

?>

<div class="page static-page frames-page">
    <div class="container">
        <div class="row center-xs">
            <h2>Каркасы</h2>
            <p>На протяжении многих лет мастера фабрики POHJANMAAN не изменяют ручным традициям труда. Все начинается с каркаса. Ведь это «душа» изделия. Затем каркас «обрастает» наполнителями, постепенно появляются очертания изделия. Последний штрих – обивочные материалы. И вот готовая модель предстает перед нами в своем завершенном гармоничном виде.</p>
            <div class="article-image" style="background-image: url('/images/special/1.jpg')"></div>
            <p>В производстве каркаса применяется массив хвойных пород (сосны, реже ели). Материал заготавливается исключительно на севере, где прочность дерева достигает максимальных значений благодаря более плотному расположению годовых колец. Перед использованием древесина бережно просушивается для получения необходимого процента влажности (5 %).</p>
            <div class="article-image" style="background-image: url('/images/special/2.jpg')"></div>
            <p>Ключевая особенность каркасов POHJANMAAN – уникальное шипорезное соединение. Части каркаса соединяются между собой по принципу шестеренок, что обеспечивает более высокую прочность и позволяет каркасу выдерживать бОльшие нагрузки. Модель, в основе которое настолько прочное соединение, долгие годы будет радовать вас своим уютом. </p>
            <div class="article-image" style="background-image: url('/images/special/3.jpg')"></div>
        </div>
    </div>
</div>