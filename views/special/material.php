<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use app\assets\OwlAsset;

$this->title = $model->name.' '.$model->code;

$this->registerScssFile("/css/special/index.scss");

?>
<div class="material-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                <div class="row material-row">
                    <div class="col-xs-6 col-sm-6">
                        <div class="material-photos">
                            <div class="item" style="background-image: url('<?=$model->image?>')">

                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6">
                        <div class="material-info">
                            <h2><?=$model->code?></h2>
                            <div class="code"><?=$model->name?></div>
                            <hr/>
                            <p><?=$model->shortdesc?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>