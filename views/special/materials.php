<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Matproducers;

$this->title = 'Эксклюзив';

$this->registerScssFile("/css/special/index.scss");
$this->registerJsFile("/js/special/materials.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->registerJsFile("/js/special/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$matproducers = Matproducers::find();
$matproducers->orderBy(['sort' => SORT_ASC]);
?>

<div class="page materials-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="row filter-row">
                    <div class="col-xs filter">
                        <select name="producers" id="producer_select" class="_select2">
                            <option <?=($producer)?"":"selected" ?> value="0">Все</option>
                            <? foreach($matproducers->all() as $item):?>
                                <option <?=($producer and $producer==$item->id)?"selected":"" ?> value="<?=$item->id?>"><?=$item->name?></option>
                            <? endforeach?>
                        </select>
                    </div>
                    <div id="tabs">

                        <span id="all" class="tab-switch"></span>
                        <a href="#all" class="tab-link">Все</a>
                        <div class="tab-content">
                            <div class="row items-0">
                                <? foreach($materials as $material):?>
                                    <div class="col-xs-4 col-sm-4 col-md-3">
                                        <div class="material" data-image="<?=$material->image?>" style="background-image: url('<?=$material->image?>')">
                                            <div class="overlay">
                                                <div class="code"><?=$material->code?></div>
                                                <div class="name"><?=$material->name?></div>
                                                <div class="shortdesc"><?=$material->shortdesc?></div>
                                                <div class="longdesc hidden"><?=$material->shortdesc?></div>
                                                <div class="buttons">
                                                    <a class="btn-material zoom" href="#all"></a>
                                                    <a class="btn-material info" href="/special/material?id=<?=$material->id?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="0" href="#all">Показать еще</a>
                            </div>
                        </div>

                        <span id="leather" class="tab-switch"></span>
                        <a href="#leather" class="tab-link">Кожа</a>
                        <div class="tab-content">
                            <div class="row items-2">
                                <? foreach($leather as $material):?>
                                    <div class="col-xs-4 col-sm-4 col-md-3">
                                        <div class="material" data-image="<?=$material->image?>" style="background-image: url('<?=$material->image?>')">
                                            <div class="overlay">
                                                <div class="code"><?=$material->code?></div>
                                                <div class="name"><?=$material->name?></div>
                                                <div class="shortdesc"><?=$material->shortdesc?></div>
                                                <div class="longdesc hidden"><?=$material->shortdesc?></div>
                                                <div class="buttons">
                                                    <a class="btn-material zoom" href="#leather"></a>
                                                    <a class="btn-material info" href="/special/material?id=<?=$material->id?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="2" href="#leather">Показать еще</a>
                            </div>
                        </div>

                        <span id="cloth" class="tab-switch"></span>
                        <a href="#cloth" class="tab-link">Ткани</a>
                        <div class="tab-content">
                            <div class="row items-1">
                                <? foreach($cloth as $material):?>
                                    <div class="col-xs-4 col-sm-4 col-md-3">
                                        <div class="material" data-image="<?=$material->image?>" style="background-image: url('<?=$material->image?>')">
                                            <div class="overlay">
                                                <div class="code"><?=$material->code?></div>
                                                <div class="name"><?=$material->name?></div>
                                                <div class="shortdesc"><?=$material->shortdesc?></div>
                                                <div class="longdesc hidden"><?=$material->shortdesc?></div>
                                                <div class="buttons">
                                                    <a class="btn-material zoom" href="#cloth"></a>
                                                    <a class="btn-material info" href="/special/material?id=<?=$material->id?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="1" href="#cloth">Показать еще</a>
                            </div>
                        </div>

                        <span id="wood" class="tab-switch"></span>
                        <a href="#wood" class="tab-link">Ножки</a>
                        <div class="tab-content">
                            <div class="row items-3">
                                <? foreach($wood as $material):?>
                                    <div class="col-xs-4 col-sm-4 col-md-3">
                                        <div class="material" data-image="<?=$material->image?>" style="background-image: url('<?=$material->image?>')">
                                            <div class="overlay">
                                                <div class="code"><?=$material->code?></div>
                                                <div class="name"><?=$material->name?></div>
                                                <div class="shortdesc"><?=$material->shortdesc?></div>
                                                <div class="longdesc hidden"><?=$material->shortdesc?></div>
                                                <div class="buttons">
                                                    <a class="btn-material zoom" href="#wood"></a>
                                                    <a class="btn-material info" href="/special/material?id=<?=$material->id?>"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="3" href="#wood">Показать еще</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-4 first-xs last-md">
                <div class="sidebar">
                    <div class="description">
                        <h3>Материалы</h3>
                        <p>Компания уверена в долголетии своих моделей: мебель производится на фабрике от и до и материалам уделяется особое внимание.</p>
                        <p>Один из важных технологических процессов, который выполняется специалистами фабрики, — сушка древесины: правильный процент влажности гарантирует, что каркас не усядет и не прогнётся.</p>
                        <p>Все обивочные материалы проходят тестирование на предмет износоустойчивости, пиллингуемости, степени устойчивости окраски, огнестойкости.</p>
                        <p>Наполнители экологичны и гипоаллергенны.</p>
                    </div>
                    <div class="description">
                        <div class="side-desc all-desc hidden">
                            <h3>Ассортимент</h3>
                            <p>Коллекция тканей, представленных в  салонах Pohjanmaan,  действительно разнообразна. Вы можете заказать диваны и кресла Pohjanmaan как в обивке из ткани финского производства, так и в обивке из ткани известных импортеров. Большой выбор цветов и фактур поможет реализовать любые решения.</p>
                        </div>
                        <div class="side-desc cloth-desc hidden">
                            <h3>Ткани</h3>
                            <p>Коллекция наших тканей достаточно разнообразна. Вы можете заказать диваны и кресла Pohjanmaan как в обивке из ткани финского производства, так и в обивке из ткани известных импортеров. Простота ухода, прочность и практичность обивочного материала сильно различаются у разных типов ткани.</p>
                            <p>Несмотря на то, что современные ткани имеют защитную обработку, ткани, в состав которых входят натуральные волокна, такие как хлопок и лен, дают усадку, на поверхности этих тканей образуются складки и со временем цвет этих тканей линяет.</p>
                            <p>Почти все обивочные ткани надо регулярно пылесосить, используя мягкую насадку для текстиля. Пятно можно быстро удалить при помощи влажной салфетки.</p>
                        </div>
                        <div class="side-desc leather-desc hidden">
                            <h3>Кожа</h3>
                            <p>Коллекция кож POHJANMAAN поражает разнообразием цветов и фактур. Неизменным остается высокое качество материала, его натуральность. Коллекция пигментированной кожи придется по вкусу приверженцам практичных решений. Пигментированные кожи подвергаются более интенсивной обработке, поэтому кожа равномерно окрашена и меньше подвержена износу и загрязнению. Кроме того, именно эта коллекция кожи, благодаря большому выбору цветов, поможет воплотить в жизнь самые смелые дизайнерские идеи. Анилиновые и полуанилинове кожи максимально естественно отряжают  фактуру материала. Изделия, выполненные в такой обивке, приобретут особый шарм благородства и элегантности.</p>
                        </div>
                        <div class="side-desc wood-desc hidden">
                            <h3>Ножки</h3>
                            <p>Ножки служат эстетическим украшением интерьера и устойчивой опорой для мебели. Правильно подобранные ножки не только украшают мебель, но и интерьер в целом.</p>
                            <p>В нашем ассортименте большое количество разнообразных опор для мебели, которые отличаются по дизайну, конструкции и изготавливаемому материалу.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    pages = [2,2,2,2];
    producer = <?= $producer ? htmlspecialchars($producer) : "0"?>;
</script>

<div class="material-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs-3">
                <div class="thumb"></div>
            </div>
            <div class="col-xs-4">
                <div class="code"></div>
                <div class="name"></div>
                <div class="longdesc"></div>
            </div>
        </div>
    </div>
</div>