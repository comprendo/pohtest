<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use app\assets\OwlAsset;

$this->title = $model->name;

$this->registerScssFile("/css/interior/index.scss");

$this->registerJsFile("//static.addtoany.com/menu/page.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>
<div class="design-page">
    <div class="container">
        <div class="row design-row">
            <div class="col-xs-12 col-sm-8">
                <div class="design-photos">
                    <? foreach($model->getPhotos() as $photo):?>
                        <div class="item" style="background-image: url('<?=$photo->image?>')">

                        </div>
                    <? endforeach?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="designer">
                    <div class="avatar" style="background-image: url('<?=$designer->image?>')"></div>
                    <h2 class="name"><?=$designer->name?></h2>
                    <div class="job"><?=$designer->job?></div>
                    <?=($designer->iswinner)?'<div class="winner"><i class="fa fa-trophy" aria-hidden="true"></i> Победитель</div>':''?>
                    <hr/>
                    <p><?=$designer->description?></p>
                    <ul class="attributes">
                        <? foreach(json_decode($designer->attributes) as $key => $value):?>
                        <li><b><?=$key?>:</b> <?=$value?></li>
                        <? endforeach?>
                    </ul>
                </div>

                <div class="design-info">
                    <h2><?=$model->name?></h2>
                    <hr/>
                    <p><?=$model->description?></p>
                    <ul class="attributes">
                        <? foreach(json_decode($model->attributes) as $key => $value):?>
                            <li><b><?=$key?>:</b> <?=$value?></li>
                        <? endforeach?>
                    </ul>
                    <div class="aa-social m-t-25">
                        <!-- AddToAny BEGIN -->
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                            <a class="a2a_button_vk"></a>
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_viber"></a>
                            <a class="a2a_button_odnoklassniki"></a>
                            <a class="a2a_button_whatsapp"></a>
                        </div>
                        <!-- AddToAny END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>