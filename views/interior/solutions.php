<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Дизайнерам';

$this->registerScssFile("/css/interior/index.scss");


//$this->registerJsFile("/js/interior/lazyload.js", [
//    'depends' => \yii\web\JqueryAsset::className()
//]);

?>
<div class="interiors-grid">
    <div class="container">
        <div class="row items">
            <? foreach($items as $item):?>
                <? $designer = $item->getDesigner() ?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <a class="item-href" href="/interior/design/<?=$item->alias?>">
                        <?=($item->iswinner2)?'<div class="winner-badge"><i class="fa fa-trophy" aria-hidden="true"></i></div>':''?>
                        <?=($item->isnew2)?'<div class="winner-badge2"><br /><span style="color: #fff;">NEW</span></div>':''?>
                        <dev class="item">
                            <div class="thumb" style="background-image: url('<?=$item->getPhotos()[rand(0,count($item->getPhotos())-1)]->image?>')">
                                <div class="overlay"></div>
                            </div>
                            <div class="infobar">
                                <div class="designer">
                                    <div class="avatar" style="background-image: url('<?=$designer->image?>')"></div>
                                    <div class="name"><?=$designer->name?></div>
                                </div>
                               <!-- <div class="images-counter">
                                    <span><?=count($item->getPhotos())?></span>
                                </div> -->
                            </div>
                            <div class="desc">
                                <div class="name"><?=$item->name?></div>
                                <div class="shortdesc"><?=$item->shortdesc?></div>
                            </div>
                        </dev>
                    </a>
                </div>
            <? endforeach?>
        </div>

        <!--<div class="row center-xs">
            <a class="btn-lazy-load" href="#">Показать еще</a>
        </div>-->
    </div>
</div>
<!--<script>
    page = 3;
</script>-->