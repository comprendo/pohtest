<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;

$this->title = 'Интерьерные решения';

$this->registerScssFile("/css/interior/index.scss");
$this->registerJsFile("/js/interior/designers.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>

<? if($success):?>
    <div class="preloader-feedback"></div>
<? endif?>
<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <h2>Дизайнерам</h2>
            <p>Компания POHJANMAAN приглашает к сотрудничеству дизайнеров, архитекторов и декораторов. Мы рады работать с дизайнерами, такое сотрудничество всегда увлекательно, ведь оно рождает новые порой нестандартные проекты. Нам невероятно интересно вместе с дизайнерами открывать новые возможности мебели POHJANMAAN и наблюдать, как привычные скадинавские модели обретают новый, свежий образ.</p>
            <p>Мы высоко ценим идеи каждого дизайнера, сотрудничающего с POHJANMAAN, каждый проект важен для нас. Специально для вас мы подготовили выгодные условия работы. Мы уверены, что продукция POHJANMAAN - отличный инструмент для реализации идей.</p>
        </div>
        <div class="row center-xs">
            <div class="col-xs-12">
                <a class="btn-gold btn-run-timeline m-b-20" href="#">Смотреть предложение <i class="fa fa-play-circle" aria-hidden="true"></i></a>
                <a class="btn-gold outlined btn-order m-b-20" href="#">Связаться с нами</a>
            </div>
        </div>
    </div>
</div>

<?=$this->render('timeline', [
    'slides' => $slides,
    //'intro' => $intro,
    //'outro' => $outro,
])?>


<div class="order-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs">
                <div class="order-container">
                    <script>
                        success = <?=$success?1:0?>;
                    </script>
                    <? if($success):?>
                        <h3>Заявка принята!</h3>
                        <p>Спасибо за Ваше обращение! Это автоматический ответ, чтобы Вы знали, что мы получили ваше письмо. Мы ответим Вам в скором времени.</p>
                        <p>С уважением, команда Pohjanmaaan!</p>
                    <? else:?>
                        <h3>Оставить заявку</h3>
                        <?php $form = ActiveForm::begin([
                            'id' => 'feedback-form',
                            'options' => [
                                //'class' => 'p-20 p-t-0'
                            ],
                            'fieldConfig' => [
                                'template' => '{input}{error}',
                                'errorOptions' => [
                                    'class' => 'help-block help-block-error m-t-0 m-b-0'
                                ]
                            ],
                        ]); ?>
                        <?= $form->field($order, 'type')->hiddenInput(['value' => 0]); ?>
                        <?= $form->field($order, 'name')->textInput(['placeholder' => "Ваше Имя" ]) ?>
                        <?= $form->field($order, 'email')->textInput(['placeholder' => "Ваш Email", 'title' => "Обязательное поле"]) ?>
                        <?= $form->field($order, 'phone')->textInput(['placeholder' => "Ваш Телефон", 'class' => 'phone_rus']) ?>

                        <?= $form->field($order, 'place')->hiddenInput(['value' => 0]); ?>
                        <?= $form->field($order, 'city')->dropDownList(Cities::getArray('id','name')) ?>
                        <?= $form->field($order, 'referrer')->hiddenInput(['value' => $_SERVER['REQUEST_URI']]); ?>

                        <?= $form->field($order, 'message')->textarea(['rows' => 6, 'placeholder' => "Ваше Сообщение"]) ?>
                        <div class="check-wrap">
                            <input type="checkbox" required name="dpa"> <label class="inline-label" for="dpa">Ознакомился(-ась) и согласен(-на) с <a href="/dpa" target="_blank">соглашением на обработку персональных данных</a>.</label>
                        </div>
                        <?= $form->field($order, 'reCaptcha')->widget(
                            \app\components\recaptcha\ReCaptcha::className(),
                            [
                                'siteKey' => '6LfF0CYUAAAAAMnB8YxYjl3nqUdBHvvJW8WowX9C',
                            ]
                        ) ?>
                        <?= Html::submitButton("Отправить", ['class' => '', 'name' => 'send-button']) ?>
                        <?php ActiveForm::end(); ?>
                    <? endif?>
                </div>
            </div>
        </div>
    </div>
</div>