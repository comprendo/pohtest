<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Файлы';

$this->registerScssFile("/css/interior/index.scss");


$this->registerJsFile("/js/interior/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

?>
<div class="interiors-grid">
    <div class="container">
        <div class="row items">
            <? foreach($items as $item):?>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <a class="item-href" target="_blank" href="<?=$item->file?>">
                        <dev class="item item-storage">
                            <div class="thumb" <?=($item->cover)?'style="background-image: url('.$item->cover.')"':($item->isImage()?'style="background-image: url('.$item->file.')"':'')?>>
                                <? if(!$item->isImage()):?>
                                    <div class="ext"><i class="fa <?=$item->getIcon()?>" aria-hidden="true"></i></div>
                                <? endif?>
                                <div class="overlay"></div>
                            </div>
                            <div class="desc">
                                <div class="shortdesc"><?=$item->name?></div>
                            </div>
                        </dev>
                    </a>
                </div>
            <? endforeach?>
        </div>

    </div>
</div>
<script>
    page = 3;
</script>