<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;

use app\assets\CaptchaAsset;
CaptchaAsset::register($this);

$this->title = '3d модели';

$this->registerScssFile("/css/feedback/index.scss");
?>
<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-10 p-left">
                <p>Компания Pohjanmaan приглашает к сотрудничеству дизайнеров, архитекторов и декораторов. Мы рады работать с дизайнерами, такое сотрудничество всегда увлекательно, ведь оно рождает новые порой нестандартные проекты. Нам невероятно интересно вместе с дизайнерами открывать новые возможности мебели Pohjanmaan и наблюдать, как привычные скандинавские модели обретают новый, свежий образ.</p>
                <p>Мы высоко ценим идеи каждого дизайнера, сотрудничающего с Pohjanmaan, каждый проект важен для нас. Специально для вас мы подготовили выгодные условия работы:</p>
                <br>
                <h3>ПРЕИМУЩЕСТВА РАБОТЫ С НАМИ</h3>
                <p><b class="b-gold">ГОТОВЫЕ 3D МОДЕЛИ</b> - экономия вашего времени и возможность в сроки реализовать проект.</p>
                <p><b class="b-gold">ПОВЫШЕННЫЕ ТРЕБОВАНИЯ К ЭРГОНОМИКЕ</b> - мягкая мебель и стулья Pohjanmaan очень удобные, и это не пустые слова, т.к. фабрика уделяет эргономике мебели особое внимание, а это залог того, что ваши клиенты каждый день будут наслаждаться комфортом!</p>
                <p><b class="b-gold">100% ГАРАНТИЯ НА ИЗГОТОВЛЕНИЕ ЗАКАЗА</b> - 15 вариантов механизмов трансформации, более 100 моделей мягкой мебели и более 50-ти моделей обеденных групп, около 1000 оттенков и фактур тканей и более 50 оттенков натуральной кожи, а также возможность изготовить мебель в ткани заказчика.</p>
                <p><b class="b-gold">КОМПЛЕКСНОСТЬ ВАШЕГО ПРЕДЛОЖЕНИЯ</b> - от мебели до предметов интерьера и текстиля, выдержанных в едином стиле, что позволяет сделать ваш проект максимально лаконичным и сэкономить время на подбор предметов интерьера.</p>
                <p><b class="b-gold">ЕЖЕГОДНЫЙ КОНКУРС ДИЗАЙН-ПРОЕКТОВ</b> - это Ваш шанс бесплатно попасть на ежегодную выставку в Финляндию и посетить завод Pohjanmaan!</p>
                <br>
                <?/*
                <a class="btn-gold btn-run-timeline btn-3d-cat m-b-20" href="https://www.dropbox.com/sh/za14go1ccmdtv68/AAANfiyytFtMOEUC7Nbs7mDja?dl=0" target="_blank" style="float: left; margin-left: 0;">Каталог 3d-моделей (Часть 1) <i class="fa fa-cube" aria-hidden="true"></i></a>
                <a class="btn-gold btn-run-timeline btn-3d-cat m-b-20" href="https://www.dropbox.com/sh/el6f07metpxruzz/AACDbGo346vuVrHZxZvnHcLpa?dl=0" target="_blank" style="float: left; margin-left: 10px;">Каталог 3d-моделей (Часть 2) <i class="fa fa-cube" aria-hidden="true"></i></a>
                */?>

            </div>
        </div>
        <div class="row center-xs">
                <? if($success):?>
                <div class="col-xs-10 col-md-10">
                    <div class="row center-xs">
                        <div class="col-xs-8">
                            <hr/>
                            <h2>Ваша заявка принята!</h2>
                        </div>
                    </div>
                    <div class="row center-xs">
                        <div class="col-xs-8">
                            <p>Спасибо за интерес к нашей продукции! Это автоматический ответ, чтобы Вы знали, что мы получили ваше письмо. Мы отправим Вам материалы в ближайшее время.</p>
                            <p>С уважением, команда Pohjanmaaan!</p>
                        </div>
                    </div>
                </div>
                <? else:?>
                <div class="col-xs-10 col-md-5">
                    <br>
                    <hr>
                    <p>Для получения каталога, пожалуйста, заполните заявку:</p>
                    <div class="tab">
                        <?php $form = ActiveForm::begin([
                            'id' => 'feedback-form',
                            'options' => [
                                //'class' => 'p-20 p-t-0'
                            ],
                            'fieldConfig' => [
                                'template' => '{input}{error}',
                                'errorOptions' => [
                                    'class' => 'help-block help-block-error m-t-0 m-b-0'
                                ]
                            ],
                        ]); ?>
                        <?= $form->field($model, 'name')->textInput(['placeholder' => "Ваше Имя" ]) ?>
                        <?= $form->field($model, 'email')->textInput(['placeholder' => "Ваш Email", 'title' => "Обязательное поле"]) ?>
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => "Ваш Телефон", 'class' => 'phone_rus']) ?>
                        <?= $form->field($model, 'city')->dropDownList(Cities::getArray('id','name')) ?>

                        <?= $form->field($model, 'type')->hiddenInput(['value' => 2]); ?>
                        <?= $form->field($model, 'referrer')->hiddenInput(['value' => $_SERVER['REQUEST_URI']]); ?>

                        <?= $form->field($model, 'message')->hiddenInput(['value' => '-']); ?>
                        <div class="check-wrap">
                            <input type="checkbox" required name="dpa"> <label class="inline-label" for="dpa">Ознакомился(-ась) и согласен(-на) с <a href="/dpa" target="_blank">соглашением на обработку персональных данных</a>.</label>
                        </div>
                        <div class="form-group">
                            <div id="pohjacaptcha">
                                <div class="pbtn"></div>
                                <div class="popup">
                                    <div>Пожалуйста, нажмите на часть картинки изображенную справа</div>
                                    <div id="PuzzleCaptcha"></div>
                                </div>
                                <span class="notarobot"><span>Я не робот</span></span>
                            </div>
                        </div>
                        <?= Html::submitButton("Отправить", ['class' => 'btnSubmit', 'name' => 'send-button', 'disabled'=>'true']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <? endif;?>
        </div>
    </div>
</div>


