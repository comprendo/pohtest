<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $item):?>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="item" id="item-<?= $item->alias ?>">
            <a href="/catalog/model/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                <div class="overlay middle-xs center-xs">
                    <?/*<a href="/catalog/model/<?=$item->alias?>">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>*/?>
                </div>
            </a>
            <div class="desc">
                <div class="name"><?=$item->model?></div>
                <div class="subline"><?=$item->name?></div>
            </div>
        </div>
    </div>
<? endforeach?>