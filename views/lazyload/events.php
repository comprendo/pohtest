<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $item):?>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="item">
            <a href="/events/article/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->image?>')">
                <div class="date-badge">
                    <div class="day"><?=$item::d($item->date, 'dd')?></div>
                    <div class="month"><?=$item::d($item->date, 'MMMM')?>'<?=substr($item::d($item->date, 'y'), -2)?></div></div>
                <div class="overlay"></div>
                <? if($item->enddate):?>
                    <div class="timeleft">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>  <?=$item->getDaysLeft()?>
                    </div>
                <? endif?>
            </a>
            <div class="desc">
                <div class="name"><?=$item->name?></div>
                <ul class="tags">
                    <li><i class="fa fa-folder-o" aria-hidden="true"></i> <a href="/events?category=<?=$item->category?>"><?=$item->getEventCategory()?></a></li>
                    <li><i class="fa fa-eye" aria-hidden="true"></i> <?=$item->views?></li>
                </ul>
                <div class="shortdesc"><?=$item->shortdesc?></div>
                <a class="btn-more" href="/events/article/<?=$item->alias?>">Читать далее</a>
            </div>
        </div>
    </div>
<? endforeach?>