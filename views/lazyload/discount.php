<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $item):?>
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="item">
            <a href="/discount/model/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                <?=($item->featured)?'<div class="featured-badge"><i class="fa fa-star" aria-hidden="true"></i></div>':''?>
                <!-- <div class="availability-badge"><?=($item->issold == 1)?'Продано':'В наличии'?></div> -->
                <div class="overlay middle-xs center-xs">
                    <?/*<a href="/discount/model/<?=$item->alias?>">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>*/?>
                </div>
            </a>
            <div class="desc">
                <div class="name"><?=$item->model?><?=($item->issold == 0)? ' <div class="price-new">' . number_format($item->pricenew, 0, '', ' ') . ' Р</div>' : '' ?></div>
                <div class="subline"><?=$item->name?><?=($item->issold == 0)? ' <div class="price-old">' . number_format($item->priceold, 0, '', ' ') . ' Р</div>' : '' ?></div>
                <div class="availability">Доступность: <?=($item->issold == 1)?'продано':'в наличии'?></div>
            </div>
        </div>
    </div>
<? endforeach?>