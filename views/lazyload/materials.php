<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $material):?>
    <div class="col-xs-4 col-sm-4 col-md-3">
        <div class="material" data-image="<?=$material->image?>" style="background-image: url('<?=$material->image?>')">
            <div class="overlay">
                <div class="code"><?=$material->code?></div>
                <div class="name"><?=$material->name?></div>
                <div class="shortdesc"><?=$material->shortdesc?></div>
                <div class="longdesc hidden"><?=$material->shortdesc?></div>
                <div class="buttons">
                    <a class="btn-material zoom" href="#all"></a>
                    <a class="btn-material info" href="/special/material?id=<?=$material->id?>"></a>
                </div>
            </div>
        </div>
    </div>
<? endforeach?>