<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $item):?>
    <div class="col-xs-12 col-lg-6">
        <a class="item-href">
            <div class="item">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-5">
                        <div class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                            <div class="overlay"></div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-8 col-md-9 col-lg-7">
                        <div class="desc">
                            <div class="name"><?=$item->name?></div>
                            <ul class="tags">
                                <li><i class="fa fa-phone" aria-hidden="true"></i> <?=$item->phone?></li><br>
                                <? if($item->email != null): ?>
                                    <li class="email"><i class="fa fa-at" aria-hidden="true"></i> <span class="copy-text tooltip" data-clipboard-text="<?=$item->email?>"><?=$item->email?><span class="tooltiptext">Скопировать</span></span></li><br>
                                <? endif ?>
                                <li><i class="fa fa-calendar-o" aria-hidden="true"></i> <?=$item->schedule?></li>
                            </ul>
                            <div class="address"><?=$item->address?></div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
<? endforeach?>