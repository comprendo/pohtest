<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $mechanism):?>
    <div class="col-xs-6 col-sm-4">
        <div class="mechanism" <?=($mechanism->video)?'data-video="'.$mechanism->getVideoId().'"':''?>>
            <div class="thumb" style="background-image: url('<?=$mechanism->getPhotos()[0]->image?>')">
                <?=($mechanism->video)?'<div class="video-badge"><i class="fa fa-play-circle" aria-hidden="true"></i></div>':''?>
                <div class="overlay"></div>
            </div>
            <div class="desc">
                <div class="name"><?=$mechanism->name?></div>
                <div class="subline"><?=$mechanism->class?></div>
            </div>
            <div class="fulldesc hidden">
                <?=$mechanism->description?>
            </div>
        </div>
    </div>
<? endforeach?>