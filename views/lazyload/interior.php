<?php
/**
 * @var $this app\components\View
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<? foreach($items as $item):?>
    <? $designer = $item->getDesigner() ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <a class="item-href" href="/interior/design/<?=$item->alias?>">
            <?=($designer->iswinner)?'<div class="winner-badge"><i class="fa fa-trophy" aria-hidden="true"></i></div>':''?>
            <dev class="item">
                <div class="thumb" style="background-image: url('<?=$item->getPhotos()[rand(0,count($item->getPhotos())-1)]->image?>')">
                    <div class="overlay"></div>
                </div>
                <div class="infobar">
                    <div class="designer">
                        <div class="avatar" style="background-image: url('<?=$designer->image?>')"></div>
                        <div class="name"><?=$designer->name?></div>
                    </div>
                    <!-- <div class="images-counter">
                        <span><?=count($item->getPhotos())?></span>
                    </div> -->
                </div>
                <div class="desc">
                    <div class="name"><?=$item->name?></div>
                    <div class="shortdesc"><?=$item->shortdesc?></div>
                </div>
            </dev>
        </a>
    </div>
<? endforeach?>