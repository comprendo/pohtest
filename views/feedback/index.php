<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;

use app\assets\CaptchaAsset;
CaptchaAsset::register($this);

$this->title = 'Обратная связь';

$this->registerScssFile("/css/feedback/index.scss");
$this->registerJsFile("/js/feedback/index.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>

<? if($success):?>
    <div class="preloader-feedback"></div>
<? endif?>
<div class="page static-page">
    <div class="container">
        <? if($success):?>
        <div class="row center-xs">
            <div class="col-xs-8">
                <h2>Благодарим за обращение!</h2>
                <hr/>
            </div>
        </div>
        <div class="row center-xs">
            <div class="col-xs-8">
                <p>Спасибо за Ваше обращение! Это автоматический ответ, чтобы Вы знали, что мы получили ваше письмо. Мы ответим Вам в скором времени.</p>
                <p>С уважением, команда Pohjanmaaan!</p>
            </div>
        </div>
        <? else:?>
        <div class="row center-xs">
            <div class="col-xs-8">
                <h2>Заполните форму</h2>
                <hr/>
            </div>
        </div>
        <div class="row center-xs">
            <div class="col-xs-10 col-md-4">
                <div class="tab-switch switch-1 active">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="switch-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 489.711 489.711" style="enable-background:new 0 0 489.711 489.711;" xml:space="preserve" width="512px" height="512px">
<g>
    <g>
        <path d="M112.156,97.111c72.3-65.4,180.5-66.4,253.8-6.7l-58.1,2.2c-7.5,0.3-13.3,6.5-13,14c0.3,7.3,6.3,13,13.5,13    c0.2,0,0.3,0,0.5,0l89.2-3.3c7.3-0.3,13-6.2,13-13.5v-1c0-0.2,0-0.3,0-0.5v-0.1l0,0l-3.3-88.2c-0.3-7.5-6.6-13.3-14-13    c-7.5,0.3-13.3,6.5-13,14l2.1,55.3c-36.3-29.7-81-46.9-128.8-49.3c-59.2-3-116.1,17.3-160,57.1c-60.4,54.7-86,137.9-66.8,217.1    c1.5,6.2,7,10.3,13.1,10.3c1.1,0,2.1-0.1,3.2-0.4c7.2-1.8,11.7-9.1,9.9-16.3C36.656,218.211,59.056,145.111,112.156,97.111z" fill="#d5d5d5"/>
        <path d="M462.456,195.511c-1.8-7.2-9.1-11.7-16.3-9.9c-7.2,1.8-11.7,9.1-9.9,16.3c16.9,69.6-5.6,142.7-58.7,190.7    c-37.3,33.7-84.1,50.3-130.7,50.3c-44.5,0-88.9-15.1-124.7-44.9l58.8-5.3c7.4-0.7,12.9-7.2,12.2-14.7s-7.2-12.9-14.7-12.2l-88.9,8    c-7.4,0.7-12.9,7.2-12.2,14.7l8,88.9c0.6,7,6.5,12.3,13.4,12.3c0.4,0,0.8,0,1.2-0.1c7.4-0.7,12.9-7.2,12.2-14.7l-4.8-54.1    c36.3,29.4,80.8,46.5,128.3,48.9c3.8,0.2,7.6,0.3,11.3,0.3c55.1,0,107.5-20.2,148.7-57.4    C456.056,357.911,481.656,274.811,462.456,195.511z" fill="#d5d5d5"/>
    </g>
</svg>
                            </i>
                        </div>
                        <div class="col-xs-9">
                            <div class="name">Обратная связь</div>
                            <div class="desc">Напишите нам сообщение и мы свяжемся с Вами в скором времени.</div>
                        </div>
                    </div>

                </div>
                <div class="tab-switch switch-2">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="switch-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.6 477.6" style="enable-background:new 0 0 477.6 477.6;" xml:space="preserve" width="512px" height="512px">
<g>
    <g>
        <path d="M407.583,70c-45.1-45.1-105-70-168.8-70s-123.7,24.9-168.8,70c-87,87-93.3,226-15.8,320.2c-10.7,21.9-23.3,36.5-37.6,43.5    c-8.7,4.3-13.6,13.7-12.2,23.3c1.5,9.7,8.9,17.2,18.6,18.7c5.3,0.8,11,1.3,16.9,1.3l0,0c29.3,0,60.1-10.1,85.8-27.8    c34.6,18.6,73.5,28.4,113.1,28.4c63.8,0,123.7-24.8,168.8-69.9s69.9-105.1,69.9-168.8S452.683,115.1,407.583,70z M388.483,388.5    c-40,40-93.2,62-149.7,62c-37.8,0-74.9-10.1-107.2-29.1c-2.1-1.2-4.5-1.9-6.8-1.9c-2.9,0-5.9,1-8.3,2.8    c-30.6,23.7-61.4,27.2-74.9,27.5c16.1-12,29.6-30.6,40.9-56.5c2.1-4.8,1.2-10.4-2.3-14.4c-74-83.6-70.1-211,8.9-290    c40-40,93.2-62,149.7-62s109.7,22,149.7,62C471.083,171.6,471.083,306,388.483,388.5z" fill="#d5d5d5"/>
        <path d="M338.783,160h-200c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h200c7.5,0,13.5-6,13.5-13.5S346.183,160,338.783,160z" fill="#d5d5d5"/>
        <path d="M338.783,225.3h-200c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h200c7.5,0,13.5-6,13.5-13.5S346.183,225.3,338.783,225.3z" fill="#d5d5d5"/>
        <path d="M338.783,290.6h-200c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h200c7.5,0,13.5-6,13.5-13.5S346.183,290.6,338.783,290.6z" fill="#d5d5d5"/>
    </g>
</g>
</svg>
                            </i>
                        </div>
                        <div class="col-xs-9">
                            <div class="name">Задать вопрос</div>
                            <div class="desc">Мы ответим на любой интересующий Вас вопрос.</div>
                        </div>
                    </div>

                </div>
                <div class="tab-switch switch-3">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="switch-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 483.3 483.3" style="enable-background:new 0 0 483.3 483.3;" xml:space="preserve" width="512px" height="512px">
<g>
    <g>
        <path d="M424.3,57.75H59.1c-32.6,0-59.1,26.5-59.1,59.1v249.6c0,32.6,26.5,59.1,59.1,59.1h365.1c32.6,0,59.1-26.5,59.1-59.1    v-249.5C483.4,84.35,456.9,57.75,424.3,57.75z M456.4,366.45c0,17.7-14.4,32.1-32.1,32.1H59.1c-17.7,0-32.1-14.4-32.1-32.1v-249.5    c0-17.7,14.4-32.1,32.1-32.1h365.1c17.7,0,32.1,14.4,32.1,32.1v249.5H456.4z" fill="#d5d5d5"/>
        <path d="M304.8,238.55l118.2-106c5.5-5,6-13.5,1-19.1c-5-5.5-13.5-6-19.1-1l-163,146.3l-31.8-28.4c-0.1-0.1-0.2-0.2-0.2-0.3    c-0.7-0.7-1.4-1.3-2.2-1.9L78.3,112.35c-5.6-5-14.1-4.5-19.1,1.1c-5,5.6-4.5,14.1,1.1,19.1l119.6,106.9L60.8,350.95    c-5.4,5.1-5.7,13.6-0.6,19.1c2.7,2.8,6.3,4.3,9.9,4.3c3.3,0,6.6-1.2,9.2-3.6l120.9-113.1l32.8,29.3c2.6,2.3,5.8,3.4,9,3.4    c3.2,0,6.5-1.2,9-3.5l33.7-30.2l120.2,114.2c2.6,2.5,6,3.7,9.3,3.7c3.6,0,7.1-1.4,9.8-4.2c5.1-5.4,4.9-14-0.5-19.1L304.8,238.55z" fill="#d5d5d5"/>
    </g>
</g>
</svg>
                            </i>
                        </div>
                        <div class="col-xs-9">
                            <div class="name">Оставить заявку</div>
                            <div class="desc">Для оформления заявки необходимо предварительно заполнить данную форму.</div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-10 col-md-4">
                <div class="tab tab-1">
                    <?php $form = ActiveForm::begin([
                        'id' => 'feedback-form',
                        'options' => [
                            //'class' => 'p-20 p-t-0'
                        ],
                        'fieldConfig' => [
                            'template' => '{input}{error}',
                            'errorOptions' => [
                                'class' => 'help-block help-block-error m-t-0 m-b-0'
                            ]
                        ],
                    ]); ?>
                    <?= $form->field($model, 'name')->textInput(['placeholder' => "Ваше Имя" ]) ?>
                    <?= $form->field($model, 'email')->textInput(['placeholder' => "Ваш Email", 'title' => "Обязательное поле"]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => "Ваш Телефон", 'class' => 'phone_rus']) ?>
                    <?= $form->field($model, 'city')->dropDownList(Cities::getArray('id','name')) ?>

                    <?= $form->field($model, 'type')->hiddenInput(['value' => 0]); ?>
                    <?= $form->field($model, 'referrer')->hiddenInput(['value' => $_SERVER['REQUEST_URI']]); ?>

                    <? if($catalog):?>
                        <?= $form->field($model, 'catalog')->hiddenInput(['value' => intval($catalog)]); ?>
                    <? endif?>
                    <? if($discount):?>
                        <?= $form->field($model, 'discount')->hiddenInput(['value' => intval($discount)]); ?>
                    <? endif?>

                    <?= $form->field($model, 'message')->textarea(['rows' => 6, 'placeholder' => "Ваше Сообщение"]) ?>
                    <div class="check-wrap">
                        <input type="checkbox" required name="dpa"> <label class="inline-label" for="dpa">Ознакомился(-ась) и согласен(-на) с <a href="/dpa" target="_blank">соглашением на обработку персональных данных</a>.</label>
                    </div>
                    <?/*= $form->field($model, 'reCaptcha')->widget(
                        \app\components\recaptcha\ReCaptcha::className(),
                        ['siteKey' => '6LfF0CYUAAAAAMnB8YxYjl3nqUdBHvvJW8WowX9C']
                    ) */?>

                    <div class="form-group">
                        <div id="pohjacaptcha">
                            <div class="pbtn"></div>
                            <div class="popup">
                                <div>Пожалуйста, нажмите на часть картинки изображенную справа</div>
                                <div id="PuzzleCaptcha"></div>
                            </div>
                            <span class="notarobot"><span>Я не робот</span></span>
                        </div>
                    </div>

                    <?= Html::submitButton("Отправить", ['class' => 'btnSubmit', 'name' => 'send-button', 'disabled'=>'true']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <? endif?>
    </div>
</div>