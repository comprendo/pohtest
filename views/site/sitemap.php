<?php

/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;

$this->title = 'Карта сайта';

?>

<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-10 sitemap">
                <div><a href="https://pohjanmaan.ru/">Главная</a></div>
                <div>
                    <a href="https://pohjanmaan.ru/catalog/">Коллекции</a>
                    <div>
                        Мягкая мебель
                        <div><a href="https://pohjanmaan.ru/catalog/cushioned">Диваны с механизмом</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category7">Диваны прямые</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category8">Диваны угловые</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category21">Кресла</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category22">Реклайнеры</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category23">Пуфы</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/armchairs-with-mechanism">Кресла с механизмом</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/rocking-chairs">Кресла-качалки</a></div>
                    </div>
                    <div>
                        Кровати и матрасы
                        <div><a href="https://pohjanmaan.ru/catalog/category10">Кровати</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category11">Матрасы</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category12">Наматрасники</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category13">Изголовья</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/bedside-tables">Прикроватные тумбы</a></div>
                    </div>
                    <div>
                        Столы и стулья
                        <div><a href="https://pohjanmaan.ru/catalog/category14">Обеденный столы</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category24">Барные столы</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category25">Журнальные столы</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/category15">Стулья</a></div>
                        <div><a href="https://pohjanmaan.ru/catalog/bar-chairs">Барные стулья</a></div>
                    </div>
                    <div><a href="https://pohjanmaan.ru/catalog/cabinet">Корпусная мебель</a></div>
                    <div>
                        Аксессуары
                        <div><a href="http://poka.loc/catalog/category16">Светильники</a></div>
                        <div><a href="http://poka.loc/catalog/category18">Ковры</a></div>
                        <div><a href="http://poka.loc/catalog/category17">Картины</a></div>
                        <div><a href="http://poka.loc/catalog/posuda">Посуда</a></div>
                        <div><a href="http://poka.loc/catalog/category9">Подсвечники</a></div>
                        <div><a href="http://poka.loc/catalog/category27">Текстиль</a></div>
                        <div><a href="http://poka.loc/catalog/category28">Другое</a></div>
                    </div>
                    <div><a href="http://poka.loc/catalog/acoustic">Звукопоглощение</a></div>
                </div>
                <div><a href="https://pohjanmaan.ru/discount/">Дисконт</a></div>
                <div>
                    <a href="https://pohjanmaan.ru/events/">Актуальное</a>
                    <div><a href="https://pohjanmaan.ru/events/?category=3">Акции</a></div>
                    <div><a href="https://pohjanmaan.ru/events/?category=2">Новости</a></div>
                    <div><a href="https://pohjanmaan.ru/events/?category=1">Интересное</a></div>
                </div>
                <div>
                    Эксклюзив
                    <div><a href="https://pohjanmaan.ru/special/materials#all">Материалы</a></div>
                    <div><a href="https://pohjanmaan.ru/special/mechanisms#all">Механизмы</a></div>
                    <div><a href="https://pohjanmaan.ru/special/frames">Каркасы</a></div>
                </div>
                <div>
                    О компании
                    <div><a href="https://pohjanmaan.ru/about/history">История</a></div>
                    <div><a href="https://pohjanmaan.ru/about/responsibility">Ответственность</a></div>
                </div>
                <div><a href="https://pohjanmaan.ru/places/">Салоны</a></div>
                <div>
                    Дизайнерам
                    <div><a href="https://pohjanmaan.ru/interior/solutions">Интерьеры</a></div>
                    <div><a href="https://pohjanmaan.ru/interior/offer">Сотрудничество</a></div>
                    <div><a href="https://pohjanmaan.ru/interior/contest">Конкурс</a></div>
                    <div><a href="https://pohjanmaan.ru/interior/3dmodels">3D модели</a></div>
                    <div><a href="https://pohjanmaan.ru/interior/storage">Каталоги</a></div>
                </div>
                <div><a href="https://pohjanmaan.ru/feedback/">Обратная связь</a></div>
            </div>
        </div>
    </div>
</div>