<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->context->layout = "//err";
?>
<div class="site-error">
    <div class="err-header">
        <div id="err-slideshow"></div>
    </div>
    <a class="err-logo" href="/"><img src="/images/site/pohjanmaan_logo400.png" alt="На главную страницу" /></a>
    <div class="container err-block">
        <h1 class="m-t-100 m-b-20"><?= Html::encode($exception->statusCode) ?></h1>

        <h2><?= nl2br(Html::encode($message)) ?></h2>

        <p class="err-message">Возможно Вы ошиблись в адресе или страница была перемещена. Вы можете вернуться на главную или воспользоваться меню.</p>

        <a class="err-link" href="/">На главную</a>
    </div>
</div>