<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;


$this->registerCssFile("js/puzzleCaptcha/puzzleCAPTCHA.css");
$this->registerJsFile("js/puzzleCaptcha/puzzleCAPTCHA.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->title = 'puzzlecaptcha';

?>

<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-10 dpa">
                <form action="">
                    <div style="display: none;">
                        <input type="text">
                        <input type="text">
                        <label>Validation value:</label>
                        <input type="text" name="" class="validationValue">
                    </div>
                    <div id="PuzzleCaptcha"></div>
                    <button type="submit" class="btnSubmit" disabled="true">submit</button>
                </form>
            </div>
        </div>
    </div>
</div>