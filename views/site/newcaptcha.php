<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Cities;
use app\assets\CaptchaAsset;


CaptchaAsset::register($this);

$this->title = 'puzzlecaptcha';

?>

<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <div class="col-xs-10 dpa">
                <form action="">
                    <div class="form-group">
                        <div id="pohjacaptcha">
                            <div class="pbtn"></div>
                            <div class="popup">
                                <div>Пожалуйста, нажмите на часть картинки изображенную справа</div>
                                <div id="PuzzleCaptcha"></div>
                            </div>
                            <span class="notarobot"><span>Я не робот</span></span>
                        </div>
                    </div>

                    <button type="submit" class="btnSubmit" disabled="true">submit</button>
                </form>
            </div>
        </div>
    </div>
</div>