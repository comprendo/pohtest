<?php

/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use app\assets\FontAwesomeAsset;
use app\assets\OwlAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\VegasAsset;

AppAsset::register($this);
VegasAsset::register($this);

$this->title = 'Pohjanmaan';

$this->registerScssFile("/css/site/index.scss");
$this->registerJsFile("/js/site/index.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

?>
<div class="wrap">
    <div class="mobile-nav">
        <div class="burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="row center-xs middle-xs nav">
            <div class="col-xs-12">
                <a href="/catalog/">коллекции</a>
            </div>
            <div class="col-xs-12">
                <a href="/feedback/">обратная связь</a>
            </div>
            <div class="col-xs-12">
                <a href="/discount/">дисконт</a>
            </div>
            <div class="col-xs-12">
                <a href="/events/?category=3">акции</a>
            </div>
            <div class="col-xs-12">
                <a href="/special/">эксклюзив</a>
            </div>
            <div class="col-xs-12">
                <a href="/about/">о компании</a>
            </div>
            <div class="col-xs-12">
                <a href="/places/">салоны</a>
            </div>
            <div class="col-xs-12">
                <a href="/interior/">дизайнерам</a>
            </div>
            <div class="col-xs-12">
                <a href="https://constructor.pohjanmaan.ru/models/3d" target="_blank">конструктор</a>
            </div>
        </div>
    </div>
    <div id="index-bg">
        <div id="slideshow">
            <?/*<div id="muteYouTubeVideoPlayer"></div>
            <script async src="https://www.youtube.com/iframe_api"></script>
            <script>
                function onYouTubeIframeAPIReady() {
                    var player;
                    player = new YT.Player('muteYouTubeVideoPlayer', {
                        videoId: 'pT1mQ-xEAnU', // YouTube Video ID
                        width: '100%',               // Player width (in px)
                        height: '100%',              // Player height (in px)
                        playerVars: {
                            autoplay: 1,        // Auto-play the video on load
                            controls: 0,        // Show pause/play buttons in player
                            showinfo: 0,        // Hide the video title
                            modestbranding: 1,  // Hide the Youtube Logo
                            loop: 1,            // Run the video in a loop
                            fs: 0,              // Hide the full screen button
                            cc_load_policy: 0, // Hide closed captions
                            iv_load_policy: 3,  // Hide the Video Annotations
                            autohide: 0,        // Hide video controls when playing
                            playlist: 'pT1mQ-xEAnU'         // Hide video controls when playing
                        },
                        events: {
                            onReady: function(e) {
                                e.target.mute();
                            }
                        }
                    });
                }
            </script>*/?>
        </div>
    </div>
    <div id="index-page">
        <div class="container">
            <div class="row center-xs middle-xs content">
                <div class="col-xs-12">
                    <img class="logo" src="/images/site/pohjanmaan_logo400.png" alt="pohjanmaan - настоящая финская мебель" />
                </div>
                <div class="col-xs-10">
                    <div class="row center-xs">
                        <div class="col-xs-4 col-md eff5">
                            <a class="m-c m-c-1" href="/catalog/">коллекции</a>
                        </div>
                        <div class="col-xs-4 col-md eff5">
                            <a class="m-c m-c-2" href="/interior/solutions/">Дизайнерам</a>
                        </div>
                        <div class="col-xs-4 col-md eff5">
                            <a class="m-c m-c-5" href="/special/">Эксклюзив</a>
                        </div>
                        <div class="col-xs-4 col-md eff5">
                            <a class="m-c m-c-3" href="/discount/">Discount</a>
                        </div>
                        <div class="col-xs-4 col-md eff5">
                            <a class="m-c m-c-4" href="/places/">Салоны</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="dwexpo">
            <a href="https://spbdesignweek.ru/pohjanmaan" target="_blank">
                <img src="/images/site/dwexpo2020_6.svg" alt="Design Week Expo 2020">
            </a>
        </div> -->
    </div>
</div>