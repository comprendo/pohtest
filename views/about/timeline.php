<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\ScrollbarAsset;

ScrollbarAsset::register($this);

$this->registerScssFile("/css/about/timeline.scss");
$this->registerJsFile("/js/about/timeline.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$slide_current = 0;
$slide_count = count($slides);

?>

<div class="timeline">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="intro">
        <div class="content">
            <div class="row middle-xs center-xs fullheight">
                <div class="col-xs-12">
                    <h3><?=$intro->title?></h3>
                    <p><?=$intro->description?></p>
                    <div class="mouse"></div>
                </div>
            </div>
            <div class="whiteline start"></div>
        </div>
    </div>

    <? foreach($slides as $slide):?>
    <? $slide_current++; ?>
    <div class="step" style="background-image: url('<?=$slide->image?>')">
        <?=($slide_current==1)?'<div class="gradient-top"></div>':''?>
        <?=($slide_current==$slide_count)?'<div class="gradient-bottom"></div>':''?>
        <div class="content">
            <div class="row middle-xs center-xs fullheight">
                <div class="col-xs-8">
                    <div class="card">
                        <h4><?=$slide->title?></h4>
                        <p><?=$slide->description?></p>
                    </div>
                </div>

            </div>
        </div>
        <div class="whiteline step"></div>
    </div>
    <? endforeach?>

    <div class="outro">
        <div class="content">
            <div class="row middle-xs center-xs fullheight">
                <div class="col-xs-12">
                    <h3><?=$outro->title?></h3>
                    <p><?=$outro->description?></p>
                    <a href="/catalog/" class="btn-timeline">В каталог</a>
                </div>
            </div>
            <div class="whiteline end"></div>
        </div>
    </div>
</div>
