<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'О компании';

?>

<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <h2>Ответственность</h2>
            <p>Pohjanmaan — это финская мебель, производство которой, прежде всего, строится на уважении к природе. Цель фабрики состоит в том, чтобы свести к минимуму возможные экологические последствия. Мебель изготавливается с максимальным учетом требований к защите окружающей среды. Более 80 % отходов производства проходят вторичную утилизацию, что доказывает социальную ответственность производителя. Все материалы, используемые в производстве продукции Pohjanmaan, — от наполнителя до обивки — экологичны и гипоаллергенны.</p>
        </div>
        <div class="row center-xs">
            <div class="col-xs-12">
                <a class="btn-gold outlined" href="/about/materials/">Материалы</a>
                <a class="btn-gold outlined" href="/about/mechanisms/">Механизмы</a>
            </div>
        </div>
    </div>
</div>