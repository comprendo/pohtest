<?php

/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'О компании';

$this->registerJsFile("/js/about/history.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>

<div class="page static-page">
    <div class="container">
        <div class="row center-xs">
            <h2>История компании</h2>
            <p>Чай должен быть ароматным, книга - интересной, а диван - настолько уютным, чтобы отдых в его объятиях приносил удовольствие. Именно такую мебель уже более 55 лет изготавливают на финской фабрике Pohjanmaan. Сегодня компания является крупнейшим финским производителем мебели, при этом по-прежнему отдает приоритет ручной работе. Вот почему каждый диван, кресло или столик Pohjanmaan наделяются не только современными эргономичными свойствами, но и особым характером.</p>
        </div>
        <div class="row center-xs">
            <div class="col-xs-12">
                <a class="btn-gold btn-run-timeline" href="#">Начать просмотр <i class="fa fa-play-circle" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>

<?= $this->render('timeline', [
    'slides' => $slides,
    'intro' => $intro,
    'outro' => $outro,
]) ?>