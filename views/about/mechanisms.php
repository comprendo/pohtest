<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'О компании';

$this->registerScssFile("/css/about/index.scss");
$this->registerJsFile("/js/about/mechanisms.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->registerJsFile("/js/about/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>

<div class="page mechanisms-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="row">
                    <div id="tabs">

                        <span id="all" class="tab-switch"></span>
                        <a href="#all" class="tab-link">Все</a>
                        <div class="tab-content">
                            <div class="row items-0">
                                <? foreach($mechanisms as $mechanism):?>
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="mechanism" <?=($mechanism->video)?'data-video="'.$mechanism->getVideoId().'"':''?>>
                                            <div class="thumb" style="background-image: url('<?=$mechanism->getPhotos()[0]->image?>')">
                                                <?=($mechanism->video)?'<div class="video-badge"><i class="fa fa-play-circle" aria-hidden="true"></i></div>':''?>
                                                <div class="overlay"></div>
                                            </div>
                                            <div class="desc">
                                                <div class="name"><?=$mechanism->name?></div>
                                                <div class="subline"><?=$mechanism->class?></div>
                                            </div>
                                            <div class="fulldesc hidden">
                                                <?=$mechanism->description?>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="0" href="#all">Показать еще</a>
                            </div>
                        </div>

                        <span id="transformations" class="tab-switch"></span>
                        <a href="#transformations" class="tab-link">Трансформация</a>
                        <div class="tab-content">
                            <div class="row items-1">
                                <? foreach($transformations as $mechanism):?>
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="mechanism" <?=($mechanism->video)?'data-video="'.$mechanism->getVideoId().'"':''?>>
                                            <div class="thumb" style="background-image: url('<?=$mechanism->getPhotos()[0]->image?>')">
                                                <?=($mechanism->video)?'<div class="video-badge"><i class="fa fa-play-circle" aria-hidden="true"></i></div>':''?>
                                                <div class="overlay"></div>
                                            </div>
                                            <div class="desc">
                                                <div class="name"><?=$mechanism->name?></div>
                                                <div class="subline"><?=$mechanism->class?></div>
                                            </div>
                                            <div class="fulldesc hidden">
                                                <?=$mechanism->description?>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="1" href="#transformations">Показать еще</a>
                            </div>
                        </div>

                        <span id="recliners" class="tab-switch"></span>
                        <a href="#recliners" class="tab-link">Реклайнеры</a>
                        <div class="tab-content">
                            <div class="row items-2">
                                <? foreach($recliners as $mechanism):?>
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="mechanism" <?=($mechanism->video)?'data-video="'.$mechanism->getVideoId().'"':''?>>
                                            <div class="thumb" style="background-image: url('<?=$mechanism->getPhotos()[0]->image?>')">
                                                <?=($mechanism->video)?'<div class="video-badge"><i class="fa fa-play-circle" aria-hidden="true"></i></div>':''?>
                                                <div class="overlay"></div>
                                            </div>
                                            <div class="desc">
                                                <div class="name"><?=$mechanism->name?></div>
                                                <div class="subline"><?=$mechanism->class?></div>
                                            </div>
                                            <div class="fulldesc hidden">
                                                <?=$mechanism->description?>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="2" href="#recliners">Показать еще</a>
                            </div>
                        </div>

                        <span id="additional" class="tab-switch"></span>
                        <a href="#additional" class="tab-link">Дополнительные опции</a>
                        <div class="tab-content">
                            <div class="row items-0">
                                <? foreach($additional as $mechanism):?>
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="mechanism" <?=($mechanism->video)?'data-video="'.$mechanism->getVideoId().'"':''?>>
                                            <div class="thumb" style="background-image: url('<?=$mechanism->getPhotos()[0]->image?>')">
                                                <?=($mechanism->video)?'<div class="video-badge"><i class="fa fa-play-circle" aria-hidden="true"></i></div>':''?>
                                                <div class="overlay"></div>
                                            </div>
                                            <div class="desc">
                                                <div class="name"><?=$mechanism->name?></div>
                                                <div class="subline"><?=$mechanism->class?></div>
                                            </div>
                                            <div class="fulldesc hidden">
                                                <?=$mechanism->description?>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach?>
                            </div>
                            <div class="row center-xs">
                                <a class="btn-lazy-load" data-type="3" href="#additional">Показать еще</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 first-xs last-md">
                <div class="sidebar">
                    <div class="description">
                        <div class="side-desc all-desc hidden">
                            <h3>Механизмы</h3>
                            <p>Заботясь о Вашем сне, компания Pohjanmaan представляет широкий спектр механизмов трансформации, которые позволяют превратить Ваш диван в уютное и практичное спальное место!</p>
                            <p>Спальный механизм необходимо выбирать в зависимости от предполагаемой частоты использования. Гостевые механизмы трансформации предназначены для периодического использования. Классические механизмы подходят для ежедневного использования.</p>
                            <p>Механизмы трансформации Pohjanmaan создают комфорт и уют для Вас и ваших гостей!</p>

                        </div>
                        <div class="side-desc transformations-desc hidden">
                            <h3>Трансформация</h3>
                            <p>Заботясь о Вашем сне, компания Pohjanmaan представляет широкий спектр механизмов трансформации, которые позволяют превратить Ваш диван в уютное и практичное спальное место!</p>
                            <p>Спальный механизм необходимо выбирать в зависимости от предполагаемой частоты использования. Гостевые механизмы трансформации предназначены для периодического использования. Классические механизмы подходят для ежедневного использования.</p>
                            <p>Механизмы трансформации Pohjanmaan создают комфорт и уют для Вас и ваших гостей!</p>
                        </div>
                        <div class="side-desc recliners-desc hidden">
                            <h3>Реклайнеры</h3>
                            <p>В переводе с английского слово "recliner" - это кресло с откидной спинкой. Однако это не единственное преимущество моделей, укомплектованных таким механизмом. Невероятная комфортность диванов и кресел реклайнеров достигается за счет откидывающейся спинки, выдвижной подушки для ног.</p>
                        </div>
                        <div class="side-desc additional-desc hidden">
                            <h3>Дополнительные опции</h3>
                            <p>Заботясь о Вашем сне, компания Pohjanmaan представляет широкий спектр механизмов трансформации, которые позволяют превратить Ваш диван в уютное и практичное спальное место!</p>
                            <p>Спальный механизм необходимо выбирать в зависимости от предполагаемой частоты использования. Гостевые механизмы трансформации предназначены для периодического использования. Классические механизмы подходят для ежедневного использования.</p>
                            <p>Механизмы трансформации Pohjanmaan создают комфорт и уют для Вас и ваших гостей!</p>
                        </div>
                    </div>
                    <div class="description">
                        <div class="item-desc hidden">
                            <h3></h3>
                            <h4></h4>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    pages = [2,2,2]
</script>

<div class="mechanism-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs">
                <div class="video-container"></div>
            </div>
        </div>
    </div>
</div>