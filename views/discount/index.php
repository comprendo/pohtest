<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Cities;

$this->title = 'Дисконт';

$this->registerScssFile("/css/catalog/index.scss");

$this->registerJsFile("/js/discount/index.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/discount/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

/*$cities = Cities::getArray('id','name');
asort($cities);

// костыль для сортировки городов
$keys = [1,3]; // 1 - питер, 2 - москва.
foreach ($keys as $key){

    if(isset($cities[$key])) {
        $value = $cities[$key];
        unset($cities[$key]);
        $cities = array($key=>$value) + $cities;
    }
}*/

// теперь без костылей, с человеческой сортировкой
$cities = Cities::find();
$cities->orderBy(['sort' => SORT_ASC]);

?>

<? if($category):?>
    <a class="btn-back-fixed" href="/discount/<?=($_SERVER['QUERY_STRING']?"?".$_SERVER['QUERY_STRING']:"")?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
<? endif?>
<div class="catalog-grid discount-grid">
    <div class="container">
        <div class="row filter-row middle-xs">
            <div class="col-xs-6">
                <h2><?=$catname?></h2>
            </div>
            <div class="col-xs filter">
                <select name="city" id="city_select" class="_select2">
                    <option <?=($city)?"":"selected" ?> value="0">Все города</option>
                    <? foreach($cities->all() as $item):?>
                        <option <?=($city and $city==$item->id)?"selected":"" ?> value="<?=$item->id?>"><?=$item->name?></option>
                    <? endforeach?>
                </select>
            </div>
        </div>

        <div class="row items">
            <? if(!$items):?>
                <p class="not-found">По Вашему запросу ничего не найдено.</p>
            <? endif?>
            <? foreach($items as $item):?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="item">
                        <a href="/discount/model/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                            <?=($item->featured)?'<div class="featured-badge"><i class="fa fa-star" aria-hidden="true"></i></div>':''?>
                            <!-- <div class="availability-badge"><?=($item->issold == 1)?'Продано':'В наличии'?></div> -->
                            <div class="overlay middle-xs center-xs">
                                <?/*<a href="/discount/model/<?=$item->alias?>">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>*/?>
                            </div>
                        </a>
                        <div class="desc">
                            <div class="name"><?=$item->model?><?=($item->issold == 0)? ' <div class="price-new">' . number_format($item->pricenew, 0, '', ' ') . ' Р</div>' : '' ?></div>
                            <div class="subline"><?=$item->name?><?=($item->issold == 0)? ' <div class="price-old">' . number_format($item->priceold, 0, '', ' ') . ' Р</div>' : '' ?></div>
                            <div class="availability">Доступность: <?=($item->issold == 1)?'продано':'в наличии'?></div>
                        </div>
                    </div>
                </div>
            <? endforeach?>
        </div>

        <div class="row center-xs">
            <? if($items and count($items)==9):?>
                <a class="btn-lazy-load" href="#">Показать еще</a>
                <a class="btn-lazy-load all" href="#">Показать все</a>
            <? endif?>
        </div>
    </div>
</div>
<script>
    page = 1;
    category = <?= $category ? htmlspecialchars($category) : "0"?>;
    city = <?= $city ? htmlspecialchars($city) : "0"?>;
    q = "<?= $q ? htmlspecialchars($q) : "0"?>";
</script>