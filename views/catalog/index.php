<?php
/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use yii\helpers\Url;

$sorts = [
    0 => "По популярности",
    1 => "По алфавиту",
    2 => "Новинки",
    3 => "По умолчанию",
    4 => "По категориям",
];

$displays = [
    0 => "Мягкая мебель",
    1 => "Все товары",
];

$this->title = 'Коллекции';

$this->registerScssFile("/css/catalog/index.scss");

$this->registerJsFile("/js/catalog/index.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/catalog/lazyload.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>
<? if($category):?>
    <a class="btn-back-fixed" href="/catalog/<?=($_SERVER['QUERY_STRING']?"?".$_SERVER['QUERY_STRING']:"")?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
<? endif?>
<div class="catalog-grid">
    <div class="container">
        <div class="row filter-row middle-xs">
            <div class="col-xs-6">
                <h2><?=$catname?></h2>
            </div>
            <? if(!$category): ?>
            <div class="col-xs filter">
                <select name="display" id="display_select" class="_select2">
                    <? foreach($displays as $key => $value):?>
                        <option <?=($display and $display==$key)?"selected":"" ?> value="<?=$key?>"><?=$value?></option>
                    <? endforeach?>
                </select>
            </div>
            <? endif; ?>
            <div class="col-xs filter">
                <select name="sort" id="sort_select" class="_select2">
                    <? foreach($sorts as $key => $value):?>
                        <option <?=($sort and $sort==$key)?"selected":"" ?> value="<?=$key?>"><?=$value?></option>
                    <? endforeach?>
                </select>
            </div>
        </div>

        <div class="row items">
            <? if(!$items):?>
                <p class="not-found">По Вашему запросу ничего не найдено.</p>
            <? endif?>
            <? foreach($items as $item):?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="item" id="item-<?= $item->alias ?>">
                        <a href="/catalog/model/<?=$item->alias?>" class="thumb" style="background-image: url('<?=$item->getPhotos()[0]->image?>')">
                            <div class="overlay middle-xs center-xs">
                                <?/*<a href="/catalog/model/<?=$item->alias?>">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>*/?>
                            </div>
                        </a>
                        <div class="desc">
                            <div class="name"><?=$item->model?></div>
                            <div class="subline"><?=$item->name?></div>
                        </div>
                    </div>
                </div>
            <? endforeach?>
        </div>

        <div class="row center-xs">
            <? if($items and count($items) < $num_rows):?>
                <a class="btn-lazy-load" href="#">Показать еще</a>
                <a class="btn-lazy-load all" data-pages="<?= ceil($num_rows / 9); ?>" href="#">Показать все</a>
            <? endif?>
        </div>
    </div>
</div>
<script>
    page = <?= $page ? htmlspecialchars($page) : "1"?>;
    category = <?= $category ? htmlspecialchars($category) : "0"?>;
    display = <?= $display ? htmlspecialchars($display) : "0"?>;
    sort = <?= $sort ? htmlspecialchars($sort) : "0"?>;
    q = "<?= $q ? htmlspecialchars($q) : "0"?>";
</script>