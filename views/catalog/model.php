<?php

/**
 * @var $this app\components\View
 * @var $albums \yii\db\ActiveQuery;
 * @var $reviews \yii\db\ActiveQuery;
 * @var $sales \yii\db\ActiveQuery;
 */

use yii\helpers\Html;
use app\assets\OwlAsset;
use yii\widgets\ActiveForm;
use app\models\Cities;

use app\assets\CaptchaAsset;

CaptchaAsset::register($this);

OwlAsset::register($this);

$this->title = $model->name . " <span class='hairline'>" . $model->model . "</span>";

$this->registerScssFile("/css/catalog/index.scss");

$this->registerJsFile("/js/catalog/model.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->registerJsFile("//static.addtoany.com/menu/page.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>
<? if($success):?>
<div class="preloader-feedback"></div>
<? endif?>
<div class="product-card">
    <div class="container">
        <div class="row product-row">
            <div class="col-xs-12 col-sm-6">
                <div class="product-slider">
                    <? foreach($model->getPhotos() as $photo):?>
                    <div class="item" style="background-image: url('<?= $photo->image ?>')">
                        <div class="zoom" data-image="<?= $photo->image ?>"><i class="fa fa-search" aria-hidden="true"></i></div>
                        <a class="zoom-mobile" href="<?= $photo->image ?>" target="_blank"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div>
                    <? endforeach?>
                </div>
                <!-- <div class="prices-group">
                    <div class="price-block">
                        <? if($model->chairprice > 0):?>
                        <p>Кресло от <b><?= number_format($model->chairprice, 0, '.', ' ') ?> ₽</b></p>
                        <? endif;?>
                        <? if($model->sofaprice > 0):?>
                        <p>Диван от <b><?= number_format($model->sofaprice, 0, '.', ' ') ?> ₽</b></p>
                        <? endif;?>
                        <? if($model->cornersofaprice > 0):?>
                        <p>Угловой диван от <b><?= number_format($model->cornersofaprice, 0, '.', ' ') ?> ₽</b></p>
                        <? endif;?>
                    </div>
                </div> -->
            </div>
            <div class="col-xs-12 col-sm-6">
                <h2><?= $model->name . " <span class='hairline'>" . $model->model . "</span>" ?></h2>
                <? if($model->isnew):?>
                <div class="badge">Новинка</div>
                <? endif?>
                <hr />
                <p><?= $model->description ?></p>
                <div class="aa-social m-t-35">
                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                        <a class="a2a_button_vk"></a>
                        <a class="a2a_button_facebook"></a>
                        <a class="a2a_button_twitter"></a>
                        <a class="a2a_button_viber"></a>
                        <a class="a2a_button_odnoklassniki"></a>
                        <a class="a2a_button_whatsapp"></a>
                    </div>
                    <!-- AddToAny END -->
                </div>
                <ul class="attributes">
                    <li><b>Коллекция:</b> <?= $model->model ?></li>
                    <? if($model->materials):?>
                    <li><b>Материал:</b>
                        <? foreach($model->getMaterialTypes() as $key => $value) echo "<span class='material-label'>".$value."</span>"?>
                    </li>
                    <? endif?>
                </ul>
                <? if($model->materials):?>
                <div class="materials">
                    <? foreach($model->getMaterialGroups() as $group):?>
                    <div class="material-group">
                        <? foreach($group as $material):?>
                        <a class="material-link" data-image="<?= $material->image ?>" style="background-image: url('<?= $material->image ?>')" href="#">
                            <div class="hidden">
                                <div class="code"><?= $material->code ?></div>
                                <div class="name"><?= $material->name ?></div>
                                <div class="shortdesc"><?= $material->shortdesc ?></div>
                                <div class="longdesc"><?= $material->shortdesc ?></div>
                            </div>
                        </a>
                        <? endforeach?>
                    </div>
                    <? endforeach?>
                    <div class="material-group">
                        <a class="material-more" href="/about/materials" target="_blank" title="Все материалы">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <? endif?>
                <hr />
                <? if($mechanisms = $model->getMechanisms()):?>
                <? foreach($mechanisms as $mechanism):?>
                <? if($mechanism->video):?>
                <a class="mechanism-link" data-video="<?= $mechanism->getVideoId() ?>" href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i> <?= $mechanism->name ?></a>
                <? endif?>
                <? endforeach?>

                <hr />
                <? endif?>
                <div>
                    <? if($model->pdf):?>
                    <a class="pdf-link" target="_blank" href="<?= $model->pdf ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Характеристики модели</a>
                    <? endif?>
                    <? if($model->fullcatalog):?>
                    <a class="pdf-link" target="_blank" href="<?= $model->fullcatalog ?>"><i class="fa fa-folder-o" aria-hidden="true"></i> Полный каталог</a>
                    <? endif?>
                </div>

                <a class="btn-gold-outlined btn-order" href="#">Оставить заявку</a>
                <a class="btn-gold-outlined" target="_blank" href="/feedback/?catalog=<?= $model->id ?>">Задать вопрос</a>
            </div>
        </div>
    </div>
</div>

<div class="catalog-grid">
    <div class="container">
        <div class="row filter-row middle-xs">
            <div class="col-xs-12">
                <h3>Вас может заинтересовать</h3>
            </div>
        </div>

        <div class="row items">
            <? foreach($items as $item):?>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="item">
                    <a href="/catalog/model/<?= $item->alias ?>" class="thumb" style="background-image: url('<?= $item->getPhotos()[0]->image ?>')">
                        <div class="overlay middle-xs center-xs">
                            <?/*<a href="/catalog/model/<?= $item->alias ?>">Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </a>*/?>
                </div>
                </a>
                <div class="desc">
                    <div class="name"><?= $item->name ?></div>
                    <div class="subline"><?= $item->model ?></div>
                </div>
            </div>
        </div>
        <? endforeach?>
    </div>
</div>
</div>

<div class="photo-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs">
                <div class="photo-container"></div>
            </div>
        </div>
    </div>
</div>

<div class="material-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs-3">
                <div class="thumb"></div>
            </div>
            <div class="col-xs-4">
                <div class="code"></div>
                <div class="name"></div>
                <div class="longdesc"></div>
            </div>
        </div>
    </div>
</div>

<div class="mechanism-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs">
                <div class="video-container"></div>
            </div>
        </div>
    </div>
</div>

<div class="order-modal">
    <div class="btn-close">
        <span></span>
        <span></span>
    </div>

    <div class="container">
        <div class="row middle-xs center-xs fullheight">
            <div class="col-xs">
                <div class="order-container">
                    <div class="row between-xs">
                        <div class="col-xs-12 col-md-6 hidden-xs">
                            <div class="product_modal">
                                <img src="<?= $model->getPhotos()[0]->image ?>" alt="<?= $model->name . " " . $model->model ?>" />
                                <div class="heading_modal"><?= $model->name . " " . $model->model ?></div>
                                <div class="desc_modal"><?= $model->description ?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <script>
                                success = <?= $success ? 1 : 0 ?>;
                            </script>
                            <? if($success):?>
                            <h3>Заявка принята!</h3>
                            <p>Спасибо за Ваше обращение! Это автоматический ответ, чтобы Вы знали, что мы получили ваше письмо. Мы ответим Вам в скором времени.</p>
                            <p>С уважением, команда Pohjanmaaan!</p>
                            <? else:?>
                            <h3>Оставить заявку</h3>
                            <?php $form = ActiveForm::begin([
                                'id' => 'feedback-form',
                                'options' => [
                                    //'class' => 'p-20 p-t-0'
                                ],
                                'fieldConfig' => [
                                    'template' => '{input}{error}',
                                    'errorOptions' => [
                                        'class' => 'help-block help-block-error m-t-0 m-b-0'
                                    ]
                                ],
                            ]); ?>
                            <?= $form->field($order, 'type')->hiddenInput(['value' => 1]); ?>
                            <?= $form->field($order, 'name')->textInput(['placeholder' => "Ваше Имя"]) ?>
                            <?= $form->field($order, 'email')->textInput(['placeholder' => "Ваш Email", 'title' => "Обязательное поле"]) ?>
                            <?= $form->field($order, 'phone')->textInput(['placeholder' => "Ваш Телефон", 'class' => 'phone_rus']) ?>

                            <?= $form->field($order, 'place')->hiddenInput(['value' => 0]); ?>
                            <?= $form->field($order, 'city')->dropDownList(Cities::getArray('id', 'name')) ?>
                            <?= $form->field($order, 'catalog')->hiddenInput(['value' => $model->id]); ?>
                            <?= $form->field($order, 'referrer')->hiddenInput(['value' => $_SERVER['REQUEST_URI']]); ?>

                            <?= $form->field($order, 'message')->textarea(['rows' => 6, 'placeholder' => "Ваше Сообщение"]) ?>
                            <div class="check-wrap">
                                <input type="checkbox" required name="dpa"> <label class="inline-label" for="dpa">Ознакомился(-ась) и согласен(-на) с <a href="/dpa" target="_blank">соглашением на обработку персональных данных</a>.</label>
                            </div>
                            <div class="form-group">
                                <div id="pohjacaptcha">
                                    <div class="pbtn"></div>
                                    <div class="popup">
                                        <div>Пожалуйста, нажмите на часть картинки изображенную справа</div>
                                        <div id="PuzzleCaptcha"></div>
                                    </div>
                                    <span class="notarobot"><span>Я не робот</span></span>
                                </div>
                            </div>
                            <?= Html::submitButton("Отправить", ['class' => 'btnSubmit', 'name' => 'send-button', 'disabled' => 'true']) ?>
                            <?php ActiveForm::end(); ?>
                            <? endif?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>