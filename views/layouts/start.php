<?php


/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\FontAwesomeAsset;
use app\models\Settings;

AppAsset::register($this);

$this->registerJsFile("/js/main.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta id="viewport" name="viewport" content="width=500, initial-scale=0.6">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-32x32.png" sizes="32x32" />
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/images/cropped-favicon_pohjanmaan.ru_-180x180.png" />
    <meta name="msapplication-TileImage" content="/images/cropped-favicon_pohjanmaan.ru_-270x270.png" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <meta name="Description" CONTENT="Финская мебель в Санкт-Петербурге, Москве, Екатеринбурге, Новосибирске, Красноярске, Нижнем Новгороде, Казани и других городах России">
    <meta name="Keywords" CONTENT="Pohjanmaan, финская мебель, мебель в санкт-петербурге">
</head>

<body>
    <div class="preloader"></div>

    <!-- <?php
            //if(!isset($_COOKIE['message_close']) || $_COOKIE['message_close'] != 1):
            ?>
<div class="modal-news">
    <div class="modal-inner">
        <div class="modal-header">
            <span class="modal-close">&times;</span>
            <h1>Мы работаем онлайн</h1>
        </div>
        <div class="modal-image">
            <img src="/images/site/work_online.jpg" alt="Мы работаем онлайн" style="max-width: 100%;">
        </div>
        <div class="modal-body">
            <p>В соответствии с указом правительства мы вынуждены приостановить работу наших салонов.</p>
            <p>Конечно, мы продолжаем оставаться на связи.</p>
            <p>По всем вопросам вы можете обращаться напрямую к руководителю нашей розничной сети, телефон +79219678735 или по форме обратной связи. Ваше обращение будет перенаправлено менеджеру для дальнейшей работы.</p>
            <p>Берегите себя и будьте здоровы!</p>
        </div>
    </div>
</div>
<?php //endif; 
?> -->

    <?php $this->beginBody(); ?>

    <?= $content ?>

    <?php $this->endBody() ?>

    <!-- <script type="text/javascript">
    $('.modal-news .modal-header .modal-close').click(function() {
        $('.modal-news').fadeOut(300);

        $.ajax({
            type: 'GET',
            url: '/messageclose'
        });
    });
</script> -->

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(15014665, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/15014665" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</body>

</html>
<?php $this->endPage() ?>