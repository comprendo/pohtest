<?php


/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\VegasAsset;
use app\assets\Select2Asset;
//use app\assets\ScrollbarAsset;
use app\assets\FontAwesomeAsset;
use app\models\Settings;
use app\models\Categories;
use app\models\Heros;
use app\models\Seotags;
use yii\helpers\Url;

AppAsset::register($this);
VegasAsset::register($this);
Select2Asset::register($this);
FontAwesomeAsset::register($this);
//ScrollbarAsset::register($this);

$this->registerScssFile("/css/catalog/media.scss");

$this->registerJsFile("/js/jquery.autocomplete.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/inner.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$hero['bg'] = "/images/catalog/hero.jpg";
$hero['description'] = "Cras dictum nisl quis sollicitudin iaculis. Fusce efficitur arcu, id aliquam purus interdum. Etiam nec suscipit odio.";
if (isset(Yii::$app->controller->herobg)) {
    $query = Heros::findOne(Yii::$app->controller->herobg);
    $hero['bg'] = $query->image;
    $hero['description'] = $query->description;
}
if (isset(Yii::$app->controller->seotag)) {
    $seotag = Seotags::findOne(Yii::$app->controller->seotag);
} else {
    $seotag = Seotags::findOne(1);
}

// Browser User Agent
// $user_agent = '';
// if ( stristr($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) $user_agent = 'firefox.css';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta id="viewport" name="viewport" content="width=500, initial-scale=0.6">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-32x32.png" sizes="32x32" />
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/images/cropped-favicon_pohjanmaan.ru_-180x180.png" />
    <meta name="msapplication-TileImage" content="/images/cropped-favicon_pohjanmaan.ru_-270x270.png" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(strip_tags($this->title)) ?> | Финская мебель Pohjanmaan</title>
    <?php $this->head() ?>
    <?php if ($user_agent) : ?>
        <link rel="stylesheet" href="/css/<?= $user_agent ?>" />
    <?php endif; ?>
    <meta name="Description" CONTENT="<?= $seotag->description ?>">
    <meta name="Keywords" CONTENT="<?= $seotag->keywords ?>">
</head>

<body>
    <div class="preloader"></div>
    <div class="mobile-nav inner">
        <div class="burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="row center-xs middle-xs nav">
            <div class="col-xs-12">
                <a href="/catalog/">Коллекции</a>
            </div>
            <div class="col-xs-12">
                <a href="/feedback/">обратная связь</a>
            </div>
            <div class="col-xs-12">
                <a href="/discount/<?= ($_SERVER['QUERY_STRING'] ? "?" . $_SERVER['QUERY_STRING'] : "") ?>">дисконт</a>
            </div>
            <div class="col-xs-12">
                <a href="/events/?category=3">акции</a>
            </div>
            <div class="col-xs-12">
                <a href="/special/">эксклюзив</a>
            </div>
            <div class="col-xs-12">
                <a href="/about/">о компании</a>
            </div>
            <div class="col-xs-12">
                <a href="/places/<?= ($_SERVER['QUERY_STRING'] ? "?" . $_SERVER['QUERY_STRING'] : "") ?>">салоны</a>
            </div>
            <div class="col-xs-12">
                <a href="/interior/">дизайнерам</a>
            </div>
            <div class="col-xs-12">
                <a href="https://constructor.pohjanmaan.ru/models/3d" target="_blank">конструктор</a>
            </div>
        </div>
    </div>
    <div class="top-menu">
        <div class="container">
            <div class="row middle-xs">
                <div class="col-xs-2">
                    <a href="/"><img src="/images/pohjanmaan_logo.png" alt="На главную страницу" /></a>
                </div>
                <div class="col-xs">
                    <nav>
                        <a class="<?= Yii::$app->controller->id == 'catalog' ? 'active' : '' ?>" href="/catalog/">Коллекции</a>
                        <?/*<a class="<?= Yii::$app->controller->id == 'feedback' ? 'active' : '' ?>" href="/feedback/">Обратная связь</a>*/?>
                        <a class="<?= Yii::$app->controller->id == 'discount' ? 'active' : '' ?>" href="/discount/<?/*=($_SERVER['QUERY_STRING']?" ?".$_SERVER['QUERY_STRING']:"")*/?>">Дисконт </a> <a class="<?= Yii::$app->controller->id == 'events' ? 'active' : '' ?>" href="/events/?category=3">Акции</a>
                        <a class="<?= Yii::$app->controller->id == 'special' ? 'active' : '' ?>" href="/special/">Эксклюзив</a>
                        <a class="<?= Yii::$app->controller->id == 'about' ? 'active' : '' ?>" href="/about/">О компании</a>
                        <a class="<?= Yii::$app->controller->id == 'places' ? 'active' : '' ?>" href="/places/<?= ($_SERVER['QUERY_STRING'] ? "?" . $_SERVER['QUERY_STRING'] : "") ?>">Салоны</a>
                        <a class="<?= Yii::$app->controller->id == 'interior' ? 'active' : '' ?>" href="/interior/">Дизайнерам</a>
                        <a class="active" href="https://constructor.pohjanmaan.ru/models/3d" target="_blank">Конструктор</a>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="hero">
        <script>
            herobg = "<?= $hero['bg'] ?>";
        </script>
        <div class="container">
            <div class="row row-hero middle-xs">
                <div class="col-xs">
                    <h1><?= $this->title ?></h1>
                    <?= isset(Yii::$app->controller->herocustom) ? Yii::$app->controller->herocustom : "<p>" . $hero['description'] . "</p>" ?>
                </div>
            </div>
        </div>
    </div>
    <div class="submenu">
        <ul class="categories">
            <? if(Yii::$app->controller->catparent):?>
            <? foreach(Categories::find()->where(['parent' => Yii::$app->controller->catparent])->orderBy(['sort' => SORT_ASC])->all() as $category):?>
            <li class="sub-item">
                <? if($children = $category->getChildren()):?>
                <?/* Это если у нас есть подменю */?>
                <a href="#" class="sub-cat" style="background-image: url('<?= $category->image ?>')"><?= $category->name ?></a>
                <div class="dropdown row center-xs middle-xs">
                    <div class="btn-close">
                        <span></span>
                        <span></span>
                    </div>
                    <? foreach($children as $child):?>
                    <div class="col-xs-12 col-md">
                        <a href="<?= Url::toRoute(['/' . Yii::$app->controller->id . '/' . $child->alias]) ?>"><?= $child->name ?></a>
                    </div>
                    <? endforeach?>
                </div>
                <? else:?>
                <?/* А это если у нас нет подменю */?>
                <a href="<?= "/" . Yii::$app->controller->id . "/" . $category->alias ?>" style="background-image: url('<?= $category->image ?>')"><?= $category->name ?></a>
                <? endif?>
            </li>
            <? endforeach?>
            <? endif?>
        </ul>
    </div>
    <?php $this->beginBody(); ?>

    <?= $content ?>

    <a class="btn-scrolltop" href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <img class="logo-footer" src="/images/pohjanmaan_logo_white.png" alt="Pohjanmaan" />
                    <p>Представительство в России<br><a class="mail-link" href="mailto:russia@pohjanmaan.ru">russia@pohjanmaan.ru</a></p>
                    <p>Pohjanmaan Kaluste Oy<br>Kalustekatu 7<br>61300 Kurikka, Finland<br><a class="mail-link" href="mailto:russia@pohjanmaan.fi">russia@pohjanmaan.fi</a><br>т. +358 (0) 201-450-500</p>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Навигация</h4>
                    <nav>
                        <a class="<?= Yii::$app->controller->id == 'catalog' ? 'active' : '' ?>" href="/catalog/">Коллекции</a>
                        <a class="<?= Yii::$app->controller->id == 'feedback' ? 'active' : '' ?>" href="/feedback/">Обратная связь</a>
                        <a class="<?= Yii::$app->controller->id == 'discount' ? 'active' : '' ?>" href="/discount/">Дисконт</a>
                        <a class="<?= Yii::$app->controller->id == 'events' ? 'active' : '' ?>" href="/events/?category=3">Акции</a>
                        <a class="<?= Yii::$app->controller->id == 'special' ? 'active' : '' ?>" href="/special/">Эксклюзив</a>
                        <a class="<?= Yii::$app->controller->id == 'about' ? 'active' : '' ?>" href="/about/">О компании</a>
                        <a class="<?= Yii::$app->controller->id == 'places' ? 'active' : '' ?>" href="/places/">Салоны</a>
                        <a class="<?= Yii::$app->controller->id == 'interior' ? 'active' : '' ?>" href="/interior/">Интерьерные решения</a>
                    </nav>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Продукция</h4>
                    <nav>
                        <a href="/catalog/cushioned">Мягкая мебель</a>
                        <a href="/catalog/category14">Столовые группы</a>
                        <a href="/catalog/category10">Спальные группы</a>
                        <a href="/catalog/category13">Текстиль</a>
                        <a href="/catalog/posuda">Посуда</a>
                    </nav>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Социальные сети</h4>
                    <p>В соответствии с современными тенденциями мы готовы быть в контакте с Вами в любой удобной социальной сети.
                    </p>
                    <nav class="nav-soc">
                        <a target="_blank" class="btn-soc facebook" href="https://www.facebook.com/pohjanmaan.ru"></a>
                        <a target="_blank" class="btn-soc instagram" href="http://www.instagram.com/pohjanmaan.ru/"></a>
                        <a target="_blank" class="btn-soc ok" href="https://ok.ru/profile/584040026282"></a>
                        <a target="_blank" class="btn-soc vk" href="https://vk.com/pohjanmaan"></a>
                        <a target="_blank" class="btn-soc twitter" href="#"></a>
                        <a target="_blank" class="btn-soc youtube" href="https://www.youtube.com/channel/UC_F4PgSCpSXTs0fekSHMXdw"></a>
                    </nav>

                    <a target="_blank" href="http://dealers.pohjanmaan.ru">Вход для дилеров</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <p class="copyright">&copy; Pohjanmaan, 2017 - <?= date('Y'); ?></p>
                    <p class="sitemap-link"><a href="/sitemap">Карта сайта</a></p>
                </div>
            </div>
        </div>
    </footer>
    <?php $this->endBody() ?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(m, e, t, r, i, k, a) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(15014665, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/15014665" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</body>

</html>
<?php $this->endPage() ?>