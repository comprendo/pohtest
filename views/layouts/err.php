<?php


/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\assets\VegasAsset;
use app\assets\Select2Asset;
//use app\assets\ScrollbarAsset;
use app\assets\FontAwesomeAsset;
use app\models\Settings;
use app\models\Categories;
use app\models\Heros;
use app\models\Seotags;
use yii\helpers\Url;

AppAsset::register($this);
VegasAsset::register($this);
Select2Asset::register($this);
FontAwesomeAsset::register($this);
//ScrollbarAsset::register($this);

$this->registerScssFile("/css/catalog/media.scss");

$this->registerJsFile("/js/jquery.autocomplete.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile("/js/inner.js", [
    'depends' => \yii\web\JqueryAsset::className()
]);

$hero['bg'] = "/images/catalog/hero.jpg";
$hero['description'] = "Cras dictum nisl quis sollicitudin iaculis. Fusce efficitur arcu, id aliquam purus interdum. Etiam nec suscipit odio.";
if (isset(Yii::$app->controller->herobg)) {
    $query = Heros::findOne(Yii::$app->controller->herobg);
    $hero['bg'] = $query->image;
    $hero['description'] = $query->description;
}
if (isset(Yii::$app->controller->seotag)) {
    $seotag = Seotags::findOne(Yii::$app->controller->seotag);
} else {
    $seotag = Seotags::findOne(1);
}

// Browser User Agent
// $user_agent = '';
// if ( stristr($_SERVER['HTTP_USER_AGENT'], 'Firefox') ) $user_agent = 'firefox.css';
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta id="viewport" name="viewport" content="width=500, initial-scale=0.6">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-32x32.png" sizes="32x32" />
    <link rel="icon" href="/images/cropped-favicon_pohjanmaan.ru_-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="/images/cropped-favicon_pohjanmaan.ru_-180x180.png" />
    <meta name="msapplication-TileImage" content="/images/cropped-favicon_pohjanmaan.ru_-270x270.png" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(strip_tags($this->title)) ?> | Финская мебель Pohjanmaan</title>
    <?php $this->head() ?>
    <?php if ($user_agent) : ?>
        <link rel="stylesheet" href="/css/<?= $user_agent ?>" />
    <?php endif; ?>
    <meta name="Description" CONTENT="<?= $seotag->description ?>">
    <meta name="Keywords" CONTENT="<?= $seotag->keywords ?>">
</head>

<body>
    <?php $this->beginBody(); ?>

    <?= $content ?>

    <a class="btn-scrolltop" href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <img class="logo-footer" src="/images/pohjanmaan_logo_white.png" alt="Pohjanmaan" />
                    <p>Представительство в России<br><a class="mail-link" href="mailto:russia@pohjanmaan.ru">russia@pohjanmaan.ru</a></p>
                    <p>Pohjanmaan Kaluste Oy<br>Kalustekatu 7<br>61300 Kurikka, Finland<br><a class="mail-link" href="mailto:russia@pohjanmaan.fi">russia@pohjanmaan.fi</a><br>т. +358 (0) 201-450-500</p>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Навигация</h4>
                    <nav>
                        <a class="<?= Yii::$app->controller->id == 'catalog' ? 'active' : '' ?>" href="/catalog/">Коллекции</a>
                        <a class="<?= Yii::$app->controller->id == 'feedback' ? 'active' : '' ?>" href="/feedback/">Обратная связь</a>
                        <a class="<?= Yii::$app->controller->id == 'discount' ? 'active' : '' ?>" href="/discount/">Дисконт</a>
                        <a class="<?= Yii::$app->controller->id == 'events' ? 'active' : '' ?>" href="/events/?category=3">Акции</a>
                        <a class="<?= Yii::$app->controller->id == 'special' ? 'active' : '' ?>" href="/special/">Эксклюзив</a>
                        <a class="<?= Yii::$app->controller->id == 'about' ? 'active' : '' ?>" href="/about/">О компании</a>
                        <a class="<?= Yii::$app->controller->id == 'places' ? 'active' : '' ?>" href="/places/">Салоны</a>
                        <a class="<?= Yii::$app->controller->id == 'interior' ? 'active' : '' ?>" href="/interior/">Интерьерные решения</a>
                    </nav>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Продукция</h4>
                    <nav>
                        <a href="/catalog/cushioned">Мягкая мебель</a>
                        <a href="/catalog/category14">Столовые группы</a>
                        <a href="/catalog/category10">Спальные группы</a>
                        <a href="/catalog/category13">Текстиль</a>
                        <a href="/catalog/posuda">Посуда</a>
                    </nav>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h4>Социальные сети</h4>
                    <p>В соответствии с современными тенденциями мы готовы быть в контакте с Вами в любой удобной социальной сети.
                    </p>
                    <nav class="nav-soc">
                        <a target="_blank" class="btn-soc facebook" href="https://www.facebook.com/pohjanmaan.ru"></a>
                        <a target="_blank" class="btn-soc instagram" href="http://www.instagram.com/pohjanmaan.ru/"></a>
                        <a target="_blank" class="btn-soc ok" href="https://ok.ru/profile/584040026282"></a>
                        <a target="_blank" class="btn-soc vk" href="https://vk.com/pohjanmaan"></a>
                        <a target="_blank" class="btn-soc twitter" href="#"></a>
                        <a target="_blank" class="btn-soc youtube" href="https://www.youtube.com/channel/UC_F4PgSCpSXTs0fekSHMXdw"></a>
                    </nav>

                    <a target="_blank" href="http://dealers.pohjanmaan.ru">Вход для дилеров</a>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <p class="copyright">&copy; Pohjanmaan, 2017 - <?= date('Y'); ?></p>
                    <p class="sitemap-link"><a href="/sitemap">Карта сайта</a></p>
                </div>
            </div>
        </div>
    </footer>
    <?php $this->endBody() ?>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function(d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter15014665 = new Ya.Metrika({
                        id: 15014665,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        ut: "noindex"
                    });
                } catch (e) {}
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function() {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/15014665?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</body>

</html>
<?php $this->endPage() ?>