<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "cities".
 *
 * @property integer $id
 * @property integer $country
 * @property string $name
 * @property integer $sort
 */
class Cities extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Города";

    // public $countries = [
    //     0 => "Россия",
    //     1 => "Finland",
    // ];

    /**
    * Получить страну
    */
    public function getCountry() {
        return $this->hasOne(Countries::className(), ['id' => 'country'])->one();
    }

    /**
    * Получить список всех стран
    */
    public function getCountries() {
        $result = [];
        $countries = Countries::find()->select(['id', 'name'])->asArray()->orderBy('sort')->all();
        for($i = 0; $i < count($countries); $i++) {
            $result[$countries[$i]['id']] = $countries[$i]['name'];
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cities';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }

    /**
     */
    // public function getCountry() {
    //     return $this->countries[$this->country];
    // }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['country', 'name'], 'required'],
            [['country', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'country' => 'Страна',
            'name' => 'Название',
            'sort' => 'Сортировка',
        ];
    }
}
