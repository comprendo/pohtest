<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "feedback".
 *
 * @property integer $id
 * @property integer $type
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $city
 * @property integer $place
 * @property string $message
 * @property integer $catalog
 * @property integer $discount
 * @property string $referrer
 * @property integer $processed
 */
class Feedback extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Заявки";

    public $feedbackTypes = [
        0 => "Обратная связь",
        1 => "Заявка",
        2 => "Заявка на каталог",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'feedback';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }


    /**
     */
    public function getFeedbackType() {
        return $this->feedbackTypes[$this->type];
    }

    /**
     * @return \app\models\Cities
     */
    public function getCity() {
        return $this->hasMany(Cities::className(), ['id' => 'city'])->one();
    }
    /**
     * @return \app\models\Places
     */
    public function getPlace() {
        return $this->hasMany(Places::className(), ['id' => 'place'])->one();
    }
    /**
     * @return \app\models\Catalog
     */
    public function getCatalog() {
        return $this->hasMany(Catalog::className(), ['id' => 'catalog'])->one();
    }
    /**
     * @return \app\models\Discount
     */
    public function getDiscount() {
        return $this->hasMany(Discount::className(), ['id' => 'discount'])->one();
    }

    public function getNext() {
        $next = $this->find()->where(['>', 'id', $this->id])->one();
        return $next;
    }

    public function getPrev() {
        $prev = $this->find()->where(['<', 'id', $this->id])->orderBy('id desc')->one();
        return $prev;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'name', 'email', 'city', 'message'], 'required'],
            [['id', 'type', 'city', 'place', 'catalog', 'discount', 'processed'], 'integer'],
            [['message'], 'string'],
            [['name', 'phone', 'email', 'referrer'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'type' => 'Тип заявки',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'city' => 'Город',
            'place' => 'Салон',
            'message' => 'Сообщение',
            'catalog' => 'Каталог',
            'discount' => 'Дисконт',
            'referrer' => 'Откуда отправлено',
            'processed' => 'Обработана',
        ];
    }


    /**
     *
     */
    public function sendEmail() {
        $admin = Users::findOne(1);

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        // $headers .= 'To: '.$admin->email . "\r\n";
        // $headers .= 'Cc: '.$admin->email . "\r\n";
        // $headers .= 'Bcc: '.$admin->email . "\r\n";

        $content = "Заявка: <br>";
        //$content .= "ID: ".$model->id."\r\n";
        $content .= "Дата поступления: ".$this->getCreateDate("d MMMM y H:mm")."<br>";
        $content .= "Тип заявки: ".$this->getFeedbackType()."<br>";
        $content .= "Имя: ".htmlspecialchars($this->name)."<br>";
        $content .= "Телефон: ".($this->phone?htmlspecialchars($this->phone):"Не указано")."<br>";
        $content .= "Email: ".($this->email?htmlspecialchars($this->email):"Не указано")."<br>";
        $content .= "Город: ".($this->city?$this->getCity()->name:"Не указано")."<br>";
        $content .= "Салон: ".($this->place?$this->getPlace()->name:"Не указано")."<br>";
        $content .= "Сообщение: ".htmlspecialchars($this->message)."<br>";

        if($this->catalog){
            $catalog = $this->getCatalog();

            $content .= "<br>Модель каталога: <br>";
            //$content .= "ID: ".$catalog->id."\r\n";
            $content .= "Название: ".$catalog->name."<br>";
            $content .= "Модель: ".$catalog->model."<br>";
            $content .= "Описание: ".$catalog->description."<br>";
        }

        if($this->discount){
            $discount = $this->getDiscount();

            $content .= "<br>Модель дисконта: <br>";
            //$content .= "ID: ".$discount->id."\r\n";
            $content .= "Название: ".$discount->name."<br>";
            $content .= "Модель: ".$discount->model."<br>";
            $content .= "Доступность: ".(($discount->issold == 0)?"<span style='color: green; font-weight: bold;'>В наличии</span>":"<span style='color: red; font-weight: bold;'>Продано</span>")."<br>";
            $content .= "Новая цена: ".$discount->pricenew."<br>";
            $content .= "Старая цена: ".$discount->priceold."<br>";
            $content .= "Город: ".($discount->city?$discount->getCity()->name:"Не указано")."<br>";
            $content .= "Салон: ".($discount->place?$discount->getPlace()->name:"Не указано")."<br>";
            $content .= "Описание: ".$discount->description."<br>";
        }

        mail($admin->email, "=?utf-8?B?".base64_encode("Заявка с сайта [Pohjanmaan]")."?=", $content, $headers);
        mail('copy_zkz@pohjanmaan.ru', "=?utf-8?B?".base64_encode("Заявка с сайта [Pohjanmaan]")."?=", $content, $headers);

        if($this->type==2){
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= 'To: '.$this->email . "\r\n";
            $headers .= 'From: noreply@pohjanmaan.ru' . "\r\n";

            $content = "Здравствуйте!<br>";
            $content .= "Вы оставили заявку на получение каталога и мы отправляем вам ссылки на скачивание.<br>";
            $content .= "Если Вы не оставляли заявку, пожалуйста, проигнорируйте это письмо.<br><br>";

            $content .= "Каталог 3d-моделей: <a href='https://oootekhno.bitrix24.ru/~fqrJf' target='_blank' style='color:#d2a637; font-weight:bold;'>скачать</a><br>";
            
            $content .= "<br>По вопросам участия в ежегодном конкурсе визуализаций и фотографий реализованных проектов PohjanmaanAwards 2021, можно обратиться по адресу: <a href='mailto:event@pohjanmaan.ru' style='color:#d2a637; font-weight:bold;'>event@pohjanmaan.ru</a>";

            $content .= "<br>Это автоматическое письмо, пожалуйста, не отвечайте на него. Связаться с нами Вы можете через контакты, указанные на сайте.<br>";

            mail($this->email, "=?utf-8?B?".base64_encode("[Pohjanmaan] Каталог 3d-моделей")."?=", $content, $headers);
        }
    }
}
