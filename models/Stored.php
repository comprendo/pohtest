<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "stored".
 *
 * @property integer $id
 * @property integer $user
 * @property integer $type
 * @property string $name
 * @property string $file
 * @property string $cover
 */
class Stored extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Файлы";

    public $types = [
        0 => "Приватный",
        1 => "Внутренний",
        2 => "Публичный",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'stored';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'file' => [],
                     'cover' => ['type' => 'image']
                ]
            ]
       ]);
    }

    public function getType() {
        return $this->types[$this->type];
    }

    public function getExtension() {
        $path_parts = pathinfo($this->file);
        return strtolower($path_parts['extension']);
    }

    public function isImage() {
        $image_extensions = ['jpg','jpeg','png'];
        return in_array(strtolower($this->getExtension()), $image_extensions)?true:false;
    }

    public function getIcon() {

        switch ($this->getExtension()) {
            case 'doc':
                return 'fa-file-word-o';
                break;
            case 'docx':
                return 'fa-file-word-o';
                break;
            case 'pdf':
                return 'fa-file-pdf-o';
                break;
            case 'xls':
                return 'fa-file-excel-o';
                break;
            default:
                return 'fa-file-o';
        }

    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user', 'name'], 'required'],
            [['user', 'type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['file'], 'file' , 'extensions' => ['jpg','jpeg','png','pdf','doc','docx','xls']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user' => 'Пользователь',
            'type' => 'Тип файла',
            'name' => 'Название файла',
            'file' => 'Файл',
            'cover' => 'Обложка',
        ];
    }
}
