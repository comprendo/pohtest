<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "countries".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 */
class Countries extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Страны";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'countries';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'sort'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Страна',
            'sort' => 'Сортировка',
        ];
    }
}
