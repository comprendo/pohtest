<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "places".
 *
 * @property integer $id
 * @property integer $sort
 * @property integer $country
 * @property integer $city
 * @property string $name
 * @property string $address
 * @property string $phone
 * @property string $schedule
 * @property string $images
 * @property string $coords
 */
class Places extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Салоны";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'places';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }

    /**
     * @return \app\models\Cities
     */
    public function getCity() {
        return $this->hasMany(Cities::className(), ['id' => 'city'])->one();
    }

    /**
    * Получить страну
    */
    public function getCountry() {
        return $this->hasOne(Countries::className(), ['id' => 'country'])->one();
    }

    public function getPhotos() {
        $photoKeys = explode(",",$this->images);

        $query = Gallery::find();
        $query->andFilterWhere(['in', 'id', $photoKeys]);

        return $query->all();
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['country', 'city', 'name', 'address', 'phone', 'email', 'schedule', 'images', 'sort'], 'required'],
            [['country', 'city', 'sort'], 'integer'],
            [['name', 'address', 'phone', 'email', 'schedule', 'images', 'coords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'sort' => 'Сортировка',
            'country' => 'Страна',
            'city' => 'Город',
            'name' => 'Название',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'schedule' => 'Часы работы',
            'images' => 'Фотографии',
            'coords' => 'Координаты',
        ];
    }
}
