<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "materials".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $type
 * @property integer $matproducer
 * @property string $image
 * @property string $shortdesc
 * @property string $longdesc
 */
class Materials extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Материалы";

    public $materialTypes = [
        1 => "Ткань",
        2 => "Кожа",
        3 => "Дерево",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'materials';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }


    /**
     */
    public function getMaterialType() {
        return $this->materialTypes[$this->type];
    }
    /**
     * @return \app\models\Matproducers
     */
    public function getMatproducer() {
        return $this->hasOne(Matproducers::className(), ['id' => 'matproducer'])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['code', 'name', 'type', 'shortdesc', 'longdesc', 'matproducer'], 'required'],
            [['type', 'matproducer'], 'integer'],
            [['longdesc'], 'string'],
            [['code', 'name', 'shortdesc'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'name' => 'Название',
            'type' => 'Тип',
            'matproducer' => 'Производитель',
            'image' => 'Изображение',
            'shortdesc' => 'Краткое описание',
            'longdesc' => 'Полное описание',
        ];
    }
}
