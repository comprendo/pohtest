<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "matproducers".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 */
class Matproducers extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Производители";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'matproducers';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'sort' => 'Сортировка',
        ];
    }
}
