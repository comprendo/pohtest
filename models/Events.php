<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "events".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $category
 * @property string $date
 * @property string $enddate
 * @property string $image
 * @property string $shortdesc
 * @property string $text
 * @property integer $views
 */
class Events extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Актуальное";

    public $eventCategories = [
        1 => "Интересное",
        2 => "Новости",
        3 => "Акции",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'events';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }


    /**
     */
    public function getEventCategory() {
        return $this->eventCategories[$this->category];
    }

    /**
     */
    public function getDaysLeft() {
        if (!$this->enddate) return "";

        $future = strtotime($this->enddate);
        $today = time();

        $timeleft = $future-$today;
        $daysleft = round((($timeleft/24)/60)/60);

        return ($daysleft>0)?"Осталось дней: ".$daysleft:"Окончено";
    }

    public function generateAlias(){
        $src = $this->name;

        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
        ];

        //transliterate
        $string = str_replace($cyr, $lat, $src);

        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'category', 'date', 'text', 'shortdesc', 'alias'], 'required'],
            [['category', 'views'], 'integer'],
            [['date', 'enddate'], 'safe'],
            [['text', 'shortdesc'], 'string'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'category' => 'Категория',
            'date' => 'Дата',
            'enddate' => 'Окончание акции (опционально)',
            'image' => 'Изображение',
            'text' => 'Текст статьи',
            'shortdesc' => 'Краткое описание',
            'views' => 'Просмотры',
        ];
    }
}
