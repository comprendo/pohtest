<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "discount".
 *
 * @property integer $id
 * @property integer $user
 * @property string $name
 * @property string $model
 * @property string $modelru
 * @property string $itemcode
 * @property string $alias
 * @property string $description
 * @property string $materials
 * @property string $mechanisms
 * @property string $pdf
 * @property string $photos
 * @property integer $featured
 * @property integer $category
 * @property integer $views
 * @property integer $pricenew
 * @property integer $priceold
 * @property integer $active
 * @property integer $issold
 * @property integer $place
 * @property integer $city
 * @property integer $seotag
 */
class Discount extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Дисконт";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'discount';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'pdf' => []
                ]
            ]
       ]);
    }


    /**
     * @return \app\models\Seotags
     */
    public function getSeotag() {
        return $this->hasOne(Seotags::className(), ['id' => 'seotag'])->one();
    }


    /**
     * @return \app\models\Categories
     */
    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category'])->one();
    }

    public function getPhotos() {
        $photoKeys = explode(",",$this->photos);

        $query = Gallery::find();
        $query->andFilterWhere(['in', 'id', $photoKeys]);

        return $query->all();
    }

    public function getMaterials() {
        $materialKeys = explode(",",$this->materials);

        $query = Materials::find();
        $query->andFilterWhere(['in', 'id', $materialKeys]);

        return $query->all();
    }

    public function getMechanisms() {
        $mechKeys = explode(",",$this->mechanisms);

        $query = Mechanisms::find();
        $query->andFilterWhere(['in', 'id', $mechKeys]);

        return $query->all();
    }

    public function getMaterialGroups(){
        $groups = [];

        foreach($this->getMaterials() as $material)
        {
            $groups[$material->type][] = $material;
        }

        return $groups;
    }

    public function getMaterialTypes() {
        $materialTypes = [];

        foreach ($this->getMaterials() as $material){
            if(!in_array($material->type, $materialTypes)){
                $materialTypes[$material->type] = $material->getMaterialType();
            }
        }

        return $materialTypes;
    }

    /**
     * @return \app\models\Places
     */
    public function getPlace() {
        return $this->hasMany(Places::className(), ['id' => 'place'])->one();
    }
    /**
     * @return \app\models\Cities
     */
    public function getCity() {
        return $this->hasMany(Cities::className(), ['id' => 'city'])->one();
    }
    /**
     * @return \app\models\Users
     */
    public function getUser() {
        return $this->hasMany(Users::className(), ['id' => 'user'])->one();
    }


    public function generateAlias(){
        $src = $this->name." ".$this->model;

        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
        ];

        //transliterate
        $string = str_replace($cyr, $lat, $src);

        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'model', 'modelru', 'alias', 'description', 'photos', 'featured', 'pricenew', 'priceold', 'place', 'city', 'user'], 'required'],
            [['description'], 'string'],
            [['featured', 'views', 'category', 'pricenew', 'priceold', 'place', 'city', 'user', 'active', 'issold', 'seotag'], 'integer'],
            [['name', 'model', 'modelru', 'alias', 'materials', 'mechanisms', 'photos', 'itemcode'], 'string', 'max' => 255],
            [['pdf'], 'file' , 'extensions' => ['pdf']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user' => 'Пользователь',
            'name' => 'Название',
            'model' => 'Модель',
            'modelru' => 'Модель Рус',
            'itemcode' => 'Код товара',
            'alias' => 'Alias',
            'description' => 'Описание',
            'materials' => 'Материалы',
            'mechanisms' => 'Механизмы',
            'pdf' => 'Файл характеристик (*.pdf)',
            'photos' => 'Фотографии',
            'featured' => 'Избранное',
            'category' => 'Категория',
            'views' => 'Просмотры',
            'pricenew' => 'Новая цена',
            'priceold' => 'Старая цена',
            'active' => 'Одобрено',
            'place' => 'Салон',
            'city' => 'Город',
            'issold' => 'Продано',
            'seotag' => 'Набор мета-тегов',
        ];
    }
}
