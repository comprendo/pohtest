<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Materials;

/**
 * CatalogSearch представляет собой модель для поиска в `app\models\Catalog`.
 */
class MaterialsSearch extends Materials {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id'], 'integer'],
            [['code'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // передача scenarios() из родительского класса
        return Model::scenarios();
    }

    /**
     * Создает data provider и строку запроса к нему
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Materials::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // Раскоментируйте эту строчку, если хотите не получить ни одной строчки в случае неудачи
            // $query->where('0=1');
            return $dataProvider;
        }

        // условия для сетки
        $query->andFilterWhere([
            'id' => $this->id,
            //'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
