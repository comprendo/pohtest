<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Discount;

/**
 * DiscountSearch представляет собой модель для поиска в `app\models\Discount`.
 */
class DiscountSearch extends Discount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city', 'issold'], 'integer'],
            [['itemcode', 'name', 'model', 'alias'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // передача scenarios() из родительского класса
        return Model::scenarios();
    }

    /**
     * Создает data provider и строку запроса к нему
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // Раскоментируйте эту строчку, если хотите не получить ни одной строчки в случае неудачи
            // $query->where('0=1');
            return $dataProvider;
        }

        // условия для сетки
        $query->andFilterWhere([
            'id' => $this->id,
            'city' => $this->city,
            //'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'issold', $this->issold])
            ->andFilterWhere(['like', 'itemcode', $this->itemcode])
            ->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
