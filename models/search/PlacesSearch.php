<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Places;

/**
 * DiscountSearch представляет собой модель для поиска в `app\models\Discount`.
 */
class PlacesSearch extends Places {
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'city'], 'integer'],
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // передача scenarios() из родительского класса
        return Model::scenarios();
    }

    /**
     * Создает data provider и строку запроса к нему
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Places::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // Раскоментируйте эту строчку, если хотите не получить ни одной строчки в случае неудачи
            // $query->where('0=1');
            return $dataProvider;
        }

        // условия для сетки
        $query->andFilterWhere([
            'id' => $this->id,
            'city' => $this->city,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
