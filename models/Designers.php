<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "designers".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $job
 * @property integer $iswinner
 * @property string $description
 * @property string $attributes
 */
class Designers extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Дизайнеры";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'designers';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'job', 'description', 'attributes'], 'required'],
            [['description', 'attributes'], 'string'],
            [['iswinner'], 'integer'],
            [['name', 'job'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'image' => 'Фотография',
            'job' => 'Должность',
            'iswinner' => 'Победитель',
            'description' => 'Описание',
            'attributes' => 'Атрибуты',
        ];
    }
}
