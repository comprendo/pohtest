<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "catalog".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property integer $modelsort
 * @property string $model
 * @property string $modelru
 * @property string $description
 * @property integer $chairprice
 * @property integer $sofaprice
 * @property integer $cornersofaprice
 * @property string $materials
 * @property string $mechanisms
 * @property string $pdf
 * @property string $fullcatalog
 * @property string $photos
 * @property integer $isnew
 * @property string $category
 * @property integer $views
 * @property integer $seotag
 */
class Catalog extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Коллекции";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'catalog';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'pdf' => []
                ]
            ]
       ]);
    }

    /**
     * @return \app\models\Seotags
     */
    public function getSeotag() {
        return $this->hasOne(Seotags::className(), ['id' => 'seotag'])->one();
    }


    /*    public function getCategory() {
            return $this->hasOne(Categories::className(), ['id' => 'category'])->one();
        }*/
    /**
     * @return \app\models\Categories
     */
    public function getCategories() {
        $catKeys = explode(",",$this->category);

        $query = Categories::find();
        $query->andFilterWhere(['in', 'id', $catKeys]);

        return $query->all();
    }
    public function getCategoryNames() {
        $names = [];

        foreach ($this->getCategories() as $category){
            $names[$category->id] = $category->name;
        }

        return implode(', ', $names);
    }

    public function getPhotos() {
        $photoKeys = explode(",",$this->photos);

        $query = Gallery::find();
        $query->andFilterWhere(['in', 'id', $photoKeys]);

        return $query->all();
    }

    public function getMaterials() {
        $materialKeys = explode(",",$this->materials);

        $query = Materials::find();
        $query->andFilterWhere(['in', 'id', $materialKeys]);

        return $query->all();
    }

    public function getMechanisms() {
        $mechKeys = explode(",",$this->mechanisms);

        $query = Mechanisms::find();
        $query->andFilterWhere(['in', 'id', $mechKeys]);

        return $query->all();
    }

    public function getMaterialGroups(){
        $groups = [];

        foreach($this->getMaterials() as $material)
        {
            $groups[$material->type][] = $material;
        }

        return $groups;
    }

    public function getMaterialTypes() {
        $materialTypes = [];

        foreach ($this->getMaterials() as $material){
            if(!in_array($material->type, $materialTypes)){
                $materialTypes[$material->type] = $material->getMaterialType();
            }
        }

        return $materialTypes;
    }

    public function generateAlias(){
        $src = $this->name." ".$this->model;

        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
        ];

        //transliterate
        $string = str_replace($cyr, $lat, $src);

        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'model', 'modelru', 'alias', 'modelsort', 'description', 'chairprice', 'sofaprice', 'cornersofaprice', 'photos', 'isnew'], 'required'],
            [['description'], 'string'],
            [['modelsort', 'chairprice', 'sofaprice', 'cornersofaprice', 'isnew', 'views', 'seotag'], 'integer'],
            [['name', 'model', 'modelru', 'materials', 'mechanisms', 'photos', 'alias', 'fullcatalog', 'category'], 'string', 'max' => 255],
            [['pdf'], 'file' , 'extensions' => ['pdf']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'modelsort' => 'Сортировка',
            'model' => 'Модель',
            'modelru' => 'Модель Рус',
            'description' => 'Описание',
            'chairprice' => 'Цена на кресло от',
            'sofaprice' => 'Цена на диван от',
            'cornersofaprice' => 'Цена на угловой диван от',
            'materials' => 'Материалы',
            'mechanisms' => 'Механизмы',
            'pdf' => 'Файл характеристик (*.pdf) | Опционально',
            'fullcatalog' => 'Ссылка на полный каталог  | Опционально',
            'photos' => 'Фотографии',
            'isnew' => 'Новинка',
            'category' => 'Категория',
            'views' => 'Просмотры',
            'seotag' => 'Набор мета-тегов',
        ];
    }
}
