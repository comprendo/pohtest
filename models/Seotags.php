<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "seotags".
 *
 * @property integer $id
 * @property string $name
 * @property string $keywords
 * @property string $description
 */
class Seotags extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Набор";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'seotags';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'keywords', 'description'], 'required'],
            [['keywords', 'description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название набора',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
        ];
    }
}
