<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "mechanisms".
 *
 * @property integer $id
 * @property string $name
 * @property string $class
 * @property integer $type
 * @property string $images
 * @property string $description
 * @property string $video
 */
class Mechanisms extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Механизмы";

    public $mechanismTypes = [
        1 => "Трансформация",
        2 => "Реклайнер",
        3 => "Дополнительные опции",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'mechanisms';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }


    public function getPhotos() {
        $photoKeys = explode(",",$this->images);

        $query = Gallery::find();
        $query->andFilterWhere(['in', 'id', $photoKeys]);

        return $query->all();
    }

    /**
     */
    public function getMechanismType() {
        return $this->mechanismTypes[$this->type];
    }

    /**
     */
    public function getVideoId() {
        parse_str( parse_url( $this->video, PHP_URL_QUERY ), $params );
        return $params['v'] ? $params['v'] : false;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'class', 'type', 'images', 'description'], 'required'],
            [['type'], 'integer'],
            [['description'], 'string'],
            [['name', 'class', 'images', 'video'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'class' => 'Класс',
            'type' => 'Тип',
            'images' => 'Изображения',
            'description' => 'Описание',
            'video' => 'Видео',
        ];
    }
}
