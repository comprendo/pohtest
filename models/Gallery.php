<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "gallery".
 *
 * @property integer $id
 * @property integer $user
 * @property string $name
 * @property string $image
 */
class Gallery extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Изображения";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gallery';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }
    /**
     * @return \app\models\Users
     */
    public function getUser() {
        return $this->hasMany(Users::className(), ['id' => 'user'])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['user'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user' => 'Пользователь',
            'name' => 'Название',
            'image' => 'Изображение',
        ];
    }
}
