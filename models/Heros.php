<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "heros".
 *
 * @property integer $id
 * @property string $section
 * @property string $image
 * @property string $description
 */
class Heros extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Фоны";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'heros';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['section'], 'required'],
            [['section', 'description'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'section' => 'Раздел',
            'description' => 'Описание',
            'image' => 'Изображение',
        ];
    }
}
