<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "interiors".
 *
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $shortdesc
 * @property string $description
 * @property string $attributes
 * @property string $images
 * @property integer $designer
 */
class Interiors extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Интерьеры";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'interiors';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }

    /**
     * @return \app\models\Designers
     */
    public function getDesigner() {
        return $this->hasOne(Designers::className(), ['id' => 'designer'])->one();
    }

    /**
     *
     */
    public function getPhotos() {
        $photoKeys = explode(",",$this->images);

        $query = Gallery::find();
        $query->andFilterWhere(['in', 'id', $photoKeys]);

        return $query->all();
    }

    public function generateAlias(){
        $src = $this->name;

        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'
        ];

        //transliterate
        $string = str_replace($cyr, $lat, $src);

        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);

        return $string;
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'shortdesc', 'description', 'attributes', 'images', 'designer', 'alias'], 'required'],
            [['shortdesc', 'description', 'attributes'], 'string'],
            [['designer', 'iswinner2', 'isnew2'], 'integer'],
            [['name', 'images', 'alias'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'iswinner2' => 'Победитель',
            'isnew2' => 'Новый',            
            'alias' => 'Alias',
            'shortdesc' => 'Краткое описание',
            'description' => 'Полное описание',
            'attributes' => 'Атрибуты',
            'images' => 'Изображения',
            'designer' => 'Дизайнер',
        ];
    }
}
