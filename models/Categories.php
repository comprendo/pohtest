<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "categories".
 *
 * @property integer $id
 * @property integer $parent
 * @property string $name
 * @property string $alias
 * @property string $image
 * @property integer $sort
 */
class Categories extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Категории";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'categories';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }



    /**
     * @return \app\models\Categories
     */
    public function getParent() {
        return $this->hasOne(Categories::className(), ['id' => 'parent'])->one();
    }

    /**
     * @return \app\models\Categories
     */
    public function getChildren() {
        return $this->hasMany(Categories::className(), ['parent' => 'id'])->orderBy(['sort' => SORT_ASC])->all();
    }



    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['parent', 'sort'], 'integer'],
            [['name', 'alias'], 'required'],
            [['name', 'alias'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'parent' => 'Родительская категория',
            'name' => 'Название',
            'alias' => 'Alias',
            'image' => 'Изображение',
            'sort' => 'Сортировка',
        ];
    }
}
