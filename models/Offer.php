<?php

namespace app\models;

use Yii;
use app\components\Model;

/**
 * Модель для таблицы "offer".
 *
 * @property integer $id
 * @property string $name
 * @property integer $city
 * @property string $email
 * @property string $phone
 * @property integer $haveworked
 * @property integer $isstudio
 * @property integer $processed
 */
class Offer extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Офферы";

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'offer';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
       ]);
    }

    /**
     * @return \app\models\Cities
     */
    public function getCity() {
        return $this->hasMany(Cities::className(), ['id' => 'city'])->one();
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'city', 'email', 'phone', 'haveworked', 'isstudio'], 'required'],
            [['city', 'haveworked', 'isstudio', 'processed'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'city' => 'Город',
            'email' => 'Email',
            'phone' => 'Телефон',
            'haveworked' => 'Работал ли ранее',
            'isstudio' => 'Агенство',
            'processed' => 'Обработана',
        ];
    }

    /**
     *
     */
    public function sendEmail() {
        $admin = Users::findOne(1);

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        // $headers .= 'To: '.$admin->email . "\r\n";
        // $headers .= 'Cc: '.$admin->email . "\r\n";
        // $headers .= 'Bcc: '.$admin->email . "\r\n";

        $content = "Заявка[Дизайнер]: <br>";
        //$content .= "ID: ".$model->id."\r\n";
        $content .= "Дата поступления: ".$this->getCreateDate("d MMMM y H:mm")."<br>";
        $content .= "Имя: ".htmlspecialchars($this->name)."<br>";
        $content .= "Телефон: ".($this->phone?htmlspecialchars($this->phone):"Не указано")."<br>";
        $content .= "Email: ".($this->email?htmlspecialchars($this->email):"Не указано")."<br>";
        $content .= "Город: ".($this->city?$this->getCity()->name:"Не указано")."<br>";
        $content .= "Работал(а) с мебелью Pohjanmaan?: ".($this->haveworked?'Да':"Нет")."<br>";
        $content .= "Представляет ли агенство?: ".($this->isstudio?'Да':"Нет")."<br>";

        mail($admin->email, "=?utf-8?B?".base64_encode("Заявка с сайта [Pohjanmaan]")."?=", $content, $headers);
        mail('copy_zkz@pohjanmaan.ru', "=?utf-8?B?" . base64_encode("Заявка с сайта [Pohjanmaan]") . "?=", $content, $headers);

        ////// письмо юзеру

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'To: '.$this->email . "\r\n";
        $headers .= 'From: noreply@pohjanmaan.ru' . "\r\n";

        $content = "Здравствуйте!<br>";
        $content .= "Вы оставили заявку на сайте Pohjanmaan.<br>";
        $content .= "Пройдя по ссылке Вы можете скачать каталог и предложение по сотрудничеству.<br><br>";

        $content .= "<a href='https://yadi.sk/d/JrBlYSvb3XsoRN' target='_blank' style='color:#d2a637; font-weight:bold;'>https://yadi.sk/d/JrBlYSvb3XsoRN</a><br>";


        $content .= "<br>Если Вы не оставляли заявку, пожалуйста, проигнорируйте это письмо.<br>";
        $content .= "Это автоматическое письмо, пожалуйста, не отвечайте на него. Связаться с нами можно через контакты, указанные на сайте.<br><br>";
        $content .= "Желаем Вам хорошего дня и вдохновения!.<br><br>";

        $content .= "Команда Pohjanmaan<br>";
        $content .= "Office: +7 (812) 334-90-25, доб.*2062<br>";
        $content .= "Mobile: +7 (911) 994-61-74<br>";

        mail($this->email, "=?utf-8?B?".base64_encode("[Pohjanmaan] Сотрудничество")."?=", $content, $headers);


    }
}
