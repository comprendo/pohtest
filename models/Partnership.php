<?php

namespace app\models;

use Yii;
use app\components\Model;
use app\components\behaviors\FileBehavior;
/**
 * Модель для таблицы "partnership".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $priority
 * @property integer $type
 * @property string $image
 */
class Partnership extends Model {

    /**
    * Название модели
    */
    public static $modelName = "Слайды";


    public $slideTypes = [
        0 => "Слайд",
        //1 => "Интро",
        //2 => "Конец",
    ];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'partnership';
    }

    /**
    * @inheritdoc
    */
    public function behaviors() {
        return array_merge(parent::behaviors(), [
           'file' => [
                'class' => FileBehavior::className(),
                'attributes' => [
                     'image' => ['type' => 'image']
                ]
            ]
       ]);
    }

    /**
     */
    public function getSlideType() {
        return $this->slideTypes[$this->type];
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description', 'priority', 'type'], 'required'],
            [['description'], 'string'],
            [['priority', 'type'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'image' , 'extensions' => ['jpg','jpeg','png']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'priority' => 'Порядок',
            'type' => 'Тип',
            'image' => 'Изображение',
        ];
    }
}
