<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model {
    public $name;
    public $phone;
    public $body;
    public $service;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            // name, email, subject and body are required
            [['name', 'phone'], 'required'],
            // email has to be a valid email address
            [['name', 'phone', 'body'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels() {
        return [
            'name' => 'Имя',
            'phone' => 'Телефон',
            'body' => 'Сообщение',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string  $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email) {
        if ($this->validate()) {
            return Yii::$app->mailer->compose("message", [
                'name' => $this->name,
                'phone' => $this->phone,
                'body' => $this->body
            ])
                ->setTo($email)
                ->setFrom(["webmaster@engine.tinyturtle.ru" => "Администратор"])
                ->setSubject("Новое письмо с сайта")
                ->send();
        }
        return false;
    }
}
