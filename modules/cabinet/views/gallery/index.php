<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="gallery-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => function($model) {
                                return Html::img($model->image, ['width'=>'120']);
                            }
                    ],
                    'name',
                    //'image:image',
                    [
                        'class' => 'app\components\make\grid\ActionColumn',
                        'template' => '{view} {update}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
