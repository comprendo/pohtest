<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Categories;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="catalog-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    //'user',
                    'name',
                    'model',
                    [
                        'attribute' => 'category',
                        'filter' => Categories::getArray('id', 'name'),
                        'value' => function($model) {
                                return ($model->getCategory())?($model->getCategory()->name):"-";
                            }
                    ],
                    //'description:ntext',
                    //'materials',
                    //'photos',
                    //'views',
                    [
                        'attribute' => 'place',
                        'value' => function($model) {
                                return ($model->getPlace())?$model->getPlace()->name:"-";
                            }
                    ],
                    'pricenew',
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => function($model) {
                                return ($model->active)?"Одобрено":"<span class='pending'>Ждет подтверждения</span>";
                            }
                    ],
                    //'priceold',
             // 'image:image',
             // 'photos:image',
                    [
                        'class' => 'app\components\make\grid\ActionColumn',
                        'template' => '{view} {update}',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<style>
    .pending{
        color: #c75757;
        font-weight: bold;
    }
</style>