<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */

$this->title = $model->name(["ЕД", "ИМ"]);
?>
<div class="catalog-view">

    <h2> <strong><?= $model->name." ".$model->model?></strong></h2>

    <div class="panel">
        <div class="panel-body">
            <div class="pull-right t-right">
                Дата создания: <strong><?=  $model->getCreateDate("d MMMM y H:mm");?></strong> <br>
            </div>

            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?/*= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент',
                'method' => 'post',
                'pjax' => '0',
                ],
            ]) */?>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => "table table-striped"],
                'attributes' => [
                    //'id',
                    //'user',
                    'name',
                    'model',
                    'modelru',
                    'pricenew',
                    'priceold',
                    [
                        'attribute' => 'category',
                        'format' => 'raw',
                        'value' => ($model->getCategory())?($model->getCategory()->name):"-"
                    ],
                    'description:ntext',
                    'materials',
                    [
                        'attribute' => 'pdf',
                        'format' => 'raw',
                        'value' => Html::a("Скачать документ", [$model->pdf], ['class'=>'no-pjax', 'target'=>'_blank'])
                    ],
                    'photos',
                    //'featured',
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => ($model->city)?$model->getCity()->name:'-'
                    ],
                    [
                        'attribute' => 'place',
                        'format' => 'raw',
                        'value' => ($model->place)?$model->getPlace()->name:'-'
                    ],

                    [
                        'attribute' => 'active',
                        'value' => function($model) {
                                return ($model->active)?"Одобрено":"Ждет подтверждения";
                            }
                    ],
                    //'views',
                ],
            ]) ?>
        </div>
    </div>
</div>
