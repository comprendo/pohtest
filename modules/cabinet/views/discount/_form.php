<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use app\models\Cities;
use app\models\Users;
use app\models\Gallery;
use app\models\Materials;
use app\models\Mechanisms;
use app\models\Places;

$rootCategory = Categories::findOne(28);
$categoryList = [];
foreach($rootCategory->getChildren() as $subCategory) {
    $categoryList[$subCategory->id] = $subCategory->name;

    foreach($subCategory->getChildren() as $targetCategory) {
        $categoryList[$targetCategory->id] = $targetCategory->name;
    }
}

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */
/* @var $form yii\widgets\ActiveForm */

$user = Users::findOne(Yii::$app->user->id);

?>

<div class="catalog-form">

    <? if($model->active==0):?>
        <?php if (count($model->errors)): ?>
            <div class="alert alert-danger">
                <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
            </div>
        <? endif;?>
        <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
        <?//= $form->field($model, 'user')->textInput(['maxlength' => true]) ?>
        <?//= $form->field($model, 'city')->dropDownList( Cities::getArray('id','name') ) ?>
        <?= $form->field($model, 'place')->dropDownList( Places::getArray('id','name', ['city' => $user->city]) ) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'modelru')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'pricenew')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'priceold')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'category')->dropDownList($categoryList) ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'materials', ['template' => '{input}'])->hiddenInput() ?>
        <div class="form-group">
            <label class="control-label" for="materials_tech">Материалы</label>
            <select name="materials_tech" id="materials_tech" class="form-control" multiple="multiple">
                <? foreach(Materials::find()->all() as $material):?>
                    <option data-image="<?=$material->image?>" value="<?=$material->id?>"><?=$material->code?></option>
                <? endforeach?>
            </select>
            <div class="help-block"></div>
        </div>

        <?= $form->field($model, 'mechanisms', ['template' => '{input}'])->hiddenInput() ?>
        <div class="form-group">
            <label class="control-label" for="mechanisms_tech">Механизмы</label>
            <select name="mechanisms_tech" id="mechanisms_tech" class="form-control" multiple="multiple">
                <? foreach(Mechanisms::find()->all() as $mechanism):?>
                    <option data-image="<?=$mechanism->getPhotos()[0]->image?>" value="<?=$mechanism->id?>"><?=$mechanism->name?> <?=$mechanism->class?></option>
                <? endforeach?>
            </select>
            <div class="help-block"></div>
        </div>

        <?= $form->field($model, 'photos', ['template' => '{input}'])->hiddenInput() ?>
        <div class="form-group">
            <label class="control-label" for="photos_tech">Фотографии</label>
            <select name="photos_tech" id="photos_tech" class="form-control" multiple="multiple">
                <? foreach(Gallery::find()->where(["user" => Yii::$app->user->id])->all() as $photo):?>
                    <option data-image="<?=$photo->image?>" value="<?=$photo->id?>"><?=$photo->name?></option>
                <? endforeach?>
            </select>
            <div class="help-block"></div>
        </div>

        <?= $form->field($model, 'pdf')->fileInput() ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <? else:?>
        <div class="alert alert-danger">
            Редактирование модели после прохождения модерации не возможно. Для внесения изменений, пожалуйста свяжитесь с администратором.
        </div>
    <? endif?>

</div>

<? if($model->active==0):?>
<style>
    .matpic{
        width: 28px;
        height: 28px;
        border: 2px solid white;
    }
    .photopic{
        max-width: 120px;
        max-height: 120px;
        border: 2px solid white;
    }
</style>

<script>
    $(function() {
        //--------------------------------------------------------------------------

        var materialValues = $('input[name="Discount[materials]"]').val().split(',');
        $("#materials_tech").select2();
        $("#materials_tech").select2('val',materialValues);

        setTimeout(function(){
            $("#materials_tech").select2({
                formatResult: formatOption,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#materials_tech").on("change", function(e) {
            var materials = $(this).val();
            $('input[name="Discount[materials]"]').val(materials.toString());
        });


        //--------------------------------------------------------------------------

        var mechanismValues = $('input[name="Discount[mechanisms]"]').val().split(',');
        $("#mechanisms_tech").select2();
        $("#mechanisms_tech").select2('val',mechanismValues);

        setTimeout(function(){
            $("#mechanisms_tech").select2({
                formatResult: formatOption,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#mechanisms_tech").on("change", function(e) {
            var mechanisms = $(this).val();
            if(materials)
                $('input[name="Discount[materials]"]').val(materials.toString());
            else
                $('input[name="Discount[materials]"]').val('');
        });

        //---------------------------------------------------------------------------


        var photoValues = $('input[name="Discount[photos]"]').val().split(',');
        $("#photos_tech").select2();
        $("#photos_tech").select2('val',photoValues);

        setTimeout(function(){
            $("#photos_tech").select2({
                formatResult: formatThumb,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; },
                sortResults: function(results, container, query) {
                    return results.sort(function(a, b) {
                        if (parseInt(a.id) < parseInt(b.id)) {
                            return 1;
                        } else if (parseInt(a.id) > parseInt(b.id)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            });
        }, 500);

        $("#photos_tech").on("change", function(e) {
            var photos = $(this).val();
            $('input[name="Discount[photos]"]').val(photos.toString());
        });

    });

    function formatOption (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="matpic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };


    function formatThumb (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="photopic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };
</script>

<? endif?>