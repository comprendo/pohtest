<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\models\Gallery;
use app\models\Users;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AccessControl;

/**
 * GalleryController Реализовывает CRUD для модели Gallery.
 */
class GalleryController extends Controller
 {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [ 'allow' => true, 'roles' => ['admin', 'user'] ],
                ],
            ],
        ];
    }

    /**
     * Отображает все записи модели Gallery.
     * @return mixed
     */
    public function actionIndex() {
        $user = Users::findOne(Yii::$app->user->id);

        $query = Gallery::find();
        $query->andWhere(['user' => $user->id]);

        if(!Yii::$app->getRequest()->getQueryParam('sort'))
            $query->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' =>  Gallery::name(["МН","ИМ"])
        ]);
    }

    /**
     * Показывает одну запись модели Gallery.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Создает запись запись модели Gallery.
     * Если создание прошло успешно, будет совершен редирект на страницу просмотра.
     * @return mixed
     */
    public function actionCreate() {
        $user = Users::findOne(Yii::$app->user->id);

        $model = new Gallery();

        if ($model->load(Yii::$app->request->post())) {
            $model->user = $user->id;

            if($model->save()){
                return $this->runAction("view", ['id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет существующую запись модели Gallery.
     * Если обновление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
/*    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('view', [
            'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
    * Показывать измеения в модели Gallery.
    * @param integer $id
    * @return mixed
    */
/*    public function actionLog($id) {
        return $this->render('log', [
            'model' => $this->findModel($id),
            'log' => $this->findModel($id)->getLog(),
        ]);
    }*/

    /**
     * Удаляет запись из модели Gallery.
     * Если удаление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
/*    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->runAction($this->defaultAction);

    }*/

    /**
     * Находит запись в модели Gallery основыаясь на внешнем ключе.
     * Если запись не найдена, выбрасывает ошибку 404.
     * @param integer $id
     * @return Gallery найденная запись
     * @throws NotFoundHttpException если модель не была найдена
     */
    protected function findModel($id)
    {
        $user = Users::findOne(Yii::$app->user->id);
        if (($model = Gallery::findOne($id)) !== null) {
            if($model->user != $user->id)
                throw new NotFoundHttpException('Ничего не найдено.');

            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
