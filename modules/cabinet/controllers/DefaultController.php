<?php

namespace app\modules\cabinet\controllers;
use app\components\Controller;
use app\components\AccessControl;

class DefaultController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                           "admin",
                           "user"
                        ],
                    ],
                ],
            ],

        ];
    }

    public function actionIndex() {
        //return $this->render("index");
        return $this->redirect('/cabinet/discount');
    }
}