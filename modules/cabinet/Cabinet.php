<?php

namespace app\modules\cabinet;
use \yii\base\Module;
use \app\models;

class Cabinet extends Module {

    // Контроллеры, которые могут использоваться в админке
    public $controllers = [

        "default" => [
            "url" => ["/cabinet"],
            "title" => "Личный кабинет",
            "icon" => "icon-home",
        ],

        "discount" => [
            "url" => ["/cabinet/discount/"],
            "title" => "Дисконт",
            "icon" => "fa fa-dollar",
            /*"badge" => [
                "model" => 'app\models\Discount',
                "condition" => []
            ]*/
        ],

        "gallery" => [
            "url" => ["/cabinet/gallery/"],
            "title" => "Галерея",
            "icon" => "fa fa-image",
        ],
    ];

    /**
     * @var array
     *
     * Пункты меню для отображения в layout. Формат:
     *
     * "default" => [
     *      "items" => ["card", "users"], - перечисление контроллеров из $this->controllers
     *      "icon" => "glyphicon glyphicon-off", - класс иконки, который получит категория
     *      "title" => "Пользователи" - название категории
     * ],
     *
     * Можно просто указать "users" для отображения контроллера без категории
     */
    public $menu = [
        /*"default" => [
            "items" => ["users", "users"],
            "icon" => "glyphicon glyphicon-off",
            "title" => "Пользователи"
        ],*/
        //"default",
        "gallery",
        "discount",


    ];
}