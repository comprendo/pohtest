<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->id;
?>
<div class="users-view">

    <h2>Пользователь #<strong><?= Html::encode($this->title) ?></strong></h2>

    <div class="panel">
        <div class="panel-body">
            <div class="pull-right t-right">
                Дата создания: <strong><?=  $model->getCreateDate("d MMMM y H:mm");?></strong> <br>
                <a href="<?= Url::toRoute([$model->tableName().'/log', 'id' => $model->id])?>">История изменений</a>
            </div>

            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить этот элемент',
                    'method' => 'post',
                    'pjax' => '0',
                ],
            ]) ?>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => "table table-striped"],
                'attributes' => [
                    'id',
                    'login',
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'value' => $model->email?$model->email:"-"
                    ],
                    //'photo:image',
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => ($model->city)?Html::a($model->getCity()->name, ['/admin/cities/view', 'id' => $model->city]):'-'
                    ],
                    [
                        'attribute' => 'role',
                        'format' => 'raw',
                        'value' => $model->roles[$model->role]
                    ],
                    //'status',
                ],
            ]) ?>
        </div>
    </div>
</div>
