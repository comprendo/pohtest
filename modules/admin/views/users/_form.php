<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form"  >

    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form']]); ?>
    <?= $form->field($model, 'login')->textInput(['maxlength' => true, "autocomplete"=> "false"]) ?>
    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, "autocomplete" => "new-password", "value" => ""]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true, "autocomplete"=> "false"]) ?>
    <?//= $form->field($model, 'photo')->fileInput() ?>
    <?= $form->field($model, 'city')->dropDownList( Cities::getArray('id','name') ) ?>
    <?= $form->field($model, 'role')->dropDownList((Yii::$app->user->role=="admin")?[ 'admin' => 'Администратор', 'manager' => 'Менеджер', 'user' => 'Дистребьютер']:['user' => 'Дистребьютер'], ['prompt' => ''/*, 'disabled' => 'disabled'*/]) ?>
    <?//= $form->field($model, 'status')->checkbox($model->isNewRecord?['disabled' => 'disabled']:[]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
