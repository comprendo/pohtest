<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Events;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */

if(!$model->alias){

    $model->alias = $model->generateAlias();
    if (($item = Events::findOne(['alias' => $model->alias])) !== null) {
        $model->alias .= "-1";
    };
}
?>

<div class="events-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'category')->dropDownList($model->eventCategories) ?>
    <?= $form->field($model, 'date')->textInput(['class' => 'b-datepicker form-control']) ?>
    <?= $form->field($model, 'enddate')->textInput(['class' => 'b-datepicker form-control']) ?>
    <?= $form->field($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'shortdesc')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'text')->textarea(['class' => 'cke-editor']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <script>
        var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"'","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"'","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"'","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"'","б":"b","ю":"yu"};

        function transliterate(word){
            return word.split('').map(function (char) {
                return a[char] || char;
            }).join("");
        }
        function ToSeoUrl(url) {

            // make the url lowercase
            var encodedUrl = url.toString().toLowerCase();

            // replace & with and
            encodedUrl = encodedUrl.split(/\&+/).join("-and-")

            // remove invalid characters
            encodedUrl = encodedUrl.split(/[^a-z0-9]/).join("-");

            // remove duplicates
            encodedUrl = encodedUrl.split(/-+/).join("-");

            // trim leading & trailing characters
            encodedUrl = encodedUrl.trim('-');

            return encodedUrl;
        }

        $(document).ready(function() {
            $('input[name="Events[name]"]').keypress(function(){
                $('input[name="Events[alias]"]').val(ToSeoUrl(transliterate($(this).val())));
            });
            $('input[name="Events[name]"]').change(function(){
                $('input[name="Events[alias]"]').val(ToSeoUrl(transliterate($(this).val())));
            });

            $("#events-date").datepicker({
                dateFormat: "yy-mm-dd"
            })
            $("#events-enddate").datepicker({
                dateFormat: "yy-mm-dd"
            })
        })
    </script>

    <style>
        #ui-datepicker-div{
            z-index: 100 !important;
        }
    </style>

    <?php ActiveForm::end(); ?>

</div>