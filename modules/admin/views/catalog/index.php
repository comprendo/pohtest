<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Categories;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="catalog-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'name',
                    'model',
                    'alias',
                    [
                        'attribute' => 'category',
                        //'filter' => Categories::getArray('id', 'name'),
                        'value' => function($model) {
                                return ($model->getCategoryNames())?($model->getCategoryNames()):"-";
                            }
                    ],
                    //'description:ntext',
                    'materials',
                    'photos',
                    'views',
                    'modelsort',
             // 'image:image',
             // 'photos:image',
             // 'isnew',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
