<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use app\models\Materials;
use app\models\Mechanisms;
use app\models\Gallery;
use app\models\Catalog;
use app\models\Seotags;

$rootCategory = Categories::findOne(1);
$categoryList = [];
foreach($rootCategory->getChildren() as $subCategory) {
    if($subCategory->getChildren()){
        foreach($subCategory->getChildren() as $targetCategory) {
            $categoryList[$targetCategory->id] = $targetCategory->name;
        }
    } else {
        $categoryList[$subCategory->id] = $subCategory->name;
    }
}

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */
/* @var $form yii\widgets\ActiveForm */

if(!$model->alias){

    $model->alias = $model->generateAlias();
    if (($item = Catalog::findOne(['alias' => $model->alias])) !== null) {
        $model->alias .= "-1";
    };
}
?>

<div class="catalog-form">
    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'modelru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'modelsort')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'category')->dropDownList($categoryList) ?>
    <?= $form->field($model, 'category', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="categories_tech">Категории</label>
        <select name="categories_tech" id="categories_tech" class="form-control" multiple="multiple">
            <? foreach($categoryList as $key => $value):?>
                <option value="<?=$key?>"><?=$value?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>


    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'chairprice')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sofaprice')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'cornersofaprice')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'materials', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="materials_tech">Материалы</label>
        <select name="materials_tech" id="materials_tech" class="form-control" multiple="multiple">
            <? foreach(Materials::find()->all() as $material):?>
                <option data-image="<?=$material->image?>" value="<?=$material->id?>"><?=$material->code?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'mechanisms', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="mechanisms_tech">Механизмы</label>
        <select name="mechanisms_tech" id="mechanisms_tech" class="form-control" multiple="multiple">
            <? foreach(Mechanisms::find()->all() as $mechanism):?>
                <option data-image="<?=$mechanism->getPhotos()[0]->image?>" value="<?=$mechanism->id?>"><?=$mechanism->name?> <?=$mechanism->class?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'photos', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="photos_tech">Фотографии</label>
        <select name="photos_tech" id="photos_tech" class="form-control" multiple="multiple">
            <? foreach(Gallery::find()->all() as $photo):?>
                <option data-image="<?=$photo->image?>" value="<?=$photo->id?>"><?=$photo->name?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'pdf')->fileInput() ?>
    <?= $form->field($model, 'fullcatalog')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isnew')->checkbox() ?>

    <hr>
    <?= $form->field($model, 'seotag', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="seotag_tech">Набор мета-тегов</label>
        <select name="seotag_tech" id="seotag_tech" class="form-control">
            <? foreach(Seotags::find()->all() as $tag):?>
                <option value="<?=$tag->id?>"><?=$tag->name?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>
    <?= Html::a('Редактировать наборы', ['/admin/seotags/'], ['class' => 'btn btn-success no-pjax', 'target' => '_blank']) ?>
    <hr>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<style>
    .matpic{
        width: 28px;
        height: 28px;
        border: 2px solid white;
    }
    .photopic{
        max-width: 120px;
        max-height: 120px;
        border: 2px solid white;
    }
</style>


<script>

    var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"'","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"'","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"'","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"'","б":"b","ю":"yu"};

    function transliterate(word){
        return word.split('').map(function (char) {
            return a[char] || char;
        }).join("");
    }
    function ToSeoUrl(url) {

        // make the url lowercase
        var encodedUrl = url.toString().toLowerCase();

        // replace & with and
        encodedUrl = encodedUrl.split(/\&+/).join("-and-")

        // remove invalid characters
        encodedUrl = encodedUrl.split(/[^a-z0-9]/).join("-");

        // remove duplicates
        encodedUrl = encodedUrl.split(/-+/).join("-");

        // trim leading & trailing characters
        encodedUrl = encodedUrl.trim('-');

        return encodedUrl;
    }
    $(function() {

        var seotagVal = $('input[name="Catalog[seotag]"]').val().split(',');
        $("#seotag_tech").select2();
        $("#seotag_tech").select2('val',seotagVal);

        if(!$('input[name="Catalog[seotag]"]').val()){
            $("#seotag_tech").select2('val',1);
            $('input[name="Catalog[seotag]"]').val(1);
        }

        setTimeout(function(){
            $("#seotag_tech").select2({
                //formatResult: formatOption,
                //formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#seotag_tech").on("change", function(e) {
            var tag = $(this).val();
            if(tag)
                $('input[name="Catalog[seotag]"]').val(tag);
            else
                $('input[name="Catalog[seotag]"]').val('');
        });


        //--------------------------------------------------------------------------
        $('input[name="Catalog[name]"]').keypress(function(){
            var name = $('input[name="Catalog[name]"]').val();
            var model = $('input[name="Catalog[model]"]').val();
            $('input[name="Catalog[alias]"]').val(ToSeoUrl(transliterate(name+" "+model)));
        });
        $('input[name="Catalog[model]"]').keypress(function(){
            var name = $('input[name="Catalog[name]"]').val();
            var model = $('input[name="Catalog[model]"]').val();
            $('input[name="Catalog[alias]"]').val(ToSeoUrl(transliterate(name+" "+model)));
        });
        $('input[name="Catalog[name]"]').change(function(){
            var name = $('input[name="Catalog[name]"]').val();
            var model = $('input[name="Catalog[model]"]').val();
            $('input[name="Catalog[alias]"]').val(ToSeoUrl(transliterate(name+" "+model)));
        });
        $('input[name="Catalog[model]"]').change(function(){
            var name = $('input[name="Catalog[name]"]').val();
            var model = $('input[name="Catalog[model]"]').val();
            $('input[name="Catalog[alias]"]').val(ToSeoUrl(transliterate(name+" "+model)));
        });

        //--------------------------------------------------------------------------

        var catValues = $('input[name="Catalog[category]"]').val().split(',');
        $("#categories_tech").select2();
        $("#categories_tech").select2('val',catValues);

        setTimeout(function(){
            $("#categories_tech").select2({
                //formatResult: formatOption,
                //formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#categories_tech").on("change", function(e) {
            var categories = $(this).val();
            if(categories)
                $('input[name="Catalog[category]"]').val(categories.toString());
            else
                $('input[name="Catalog[category]"]').val('');
        });

        //--------------------------------------------------------------------------

        var materialValues = $('input[name="Catalog[materials]"]').val().split(',');
        $("#materials_tech").select2();
        $("#materials_tech").select2('val',materialValues);

        setTimeout(function(){
            $("#materials_tech").select2({
                formatResult: formatOption,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#materials_tech").on("change", function(e) {
            var materials = $(this).val();
            if(materials)
                $('input[name="Catalog[materials]"]').val(materials.toString());
            else
                $('input[name="Catalog[materials]"]').val('');
        });

        //--------------------------------------------------------------------------

        var mechanismValues = $('input[name="Catalog[mechanisms]"]').val().split(',');
        $("#mechanisms_tech").select2();
        $("#mechanisms_tech").select2('val',mechanismValues);

        setTimeout(function(){
            $("#mechanisms_tech").select2({
                formatResult: formatOption,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; }
            });
        }, 500);

        $("#mechanisms_tech").on("change", function(e) {
            var mechanisms = $(this).val();
            $('input[name="Catalog[mechanisms]"]').val(mechanisms.toString());
        });

        //---------------------------------------------------------------------------


        var photoValues = $('input[name="Catalog[photos]"]').val().split(',');
        $("#photos_tech").select2();
        $("#photos_tech").select2('val',photoValues);

        setTimeout(function(){
            $("#photos_tech").select2({
                formatResult: formatThumb,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; },
                sortResults: function(results, container, query) {
                    return results.sort(function(a, b) {
                        //console.log(results);
                        if (parseInt(a.id) < parseInt(b.id)) {
                            return 1;
                        } else if (parseInt(a.id) > parseInt(b.id)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            });
        }, 500);

        $("#photos_tech").on("change", function(e) {
            var photos = $(this).val();
            $('input[name="Catalog[photos]"]').val(photos.toString());
        });

    });

    function formatOption (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="matpic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };


    function formatThumb (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="photopic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };
</script>