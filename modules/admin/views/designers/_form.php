<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Designers */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="designers-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'iswinner')->checkbox() ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'image')->fileInput() ?>
    <?= $form->field($model, 'job')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attributes', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <div id="attributes">
        <label>Атрибуты</label>

        </div>
        <a class="btn btn-primary" id="add-attribute">Добавить атрибут</a>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="hidden templates">

    <!-- Атрибуты -->
    <div class="attribute-template">
        <div class="attribute panel panel-default">
            <div class="panel-heading">
                <div class="form-group">
                    <label>Атрибут:</label>
                    <input type="text" name="attribute-name" class="form-control attribute-name form-white">
                </div>
                <div class="form-group">
                    <label>Значение:</label>
                    <input type="text" name="attribute-value" class="form-control attribute-value form-white">
                </div>
                <a class="btn btn-danger remove-attribute">Удалить атрибут</a>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {

        $("#add-attribute").click(addAttribute);
        $("#save").click(save);
        $(this).on("click", ".attribute .remove-attribute", function() {
            $(this).closest(".attribute").remove();
        });

        $("form.pjax-form").on("beforeValidate", function() {
            $("#designers-attributes").val(JSON.stringify(save()));
        });

        if($("#designers-attributes").val()){
            var attributes = JSON.parse($("#designers-attributes").val());

            for (var i in attributes) {
                var attribute = addAttribute();
                $(attribute).find(".attribute-name").val(i);
                $(attribute).find(".attribute-value").val(attributes[i]);
            }
        }


    });

    function addAttribute() {
        var container = $("#attributes");
        var template = $(".templates .attribute-template").clone().html();
        $(template).appendTo(container);
        return $("#attributes .attribute:last");
    }

    function save() {
        var attributes = {};

        $("#attributes .attribute").each(function() {
            var name = $(this).find(".attribute-name").val();
            attributes[name] = $(this).find(".attribute-value").val();
        });

        return attributes;
    }

</script>