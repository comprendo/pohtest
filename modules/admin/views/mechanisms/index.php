<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="mechanisms-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'name',
                    'class',
                    [
                        'attribute' => 'type',
                        'value' => function($model) {
                                return $model->getMechanismType();
                            }
                    ],
                  //'images:image',
                  //'description:ntext',
                    [
                        'attribute' => 'video',
                        'format' => 'raw',
                        'value' => function($model) {
                                return ($model->video)?'<i class="fa fa-play-circle" aria-hidden="true"></i>':'-';
                            }
                    ],
             // 'video',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
