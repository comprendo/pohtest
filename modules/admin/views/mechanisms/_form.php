<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Gallery;

/* @var $this yii\web\View */
/* @var $model app\models\Mechanisms */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mechanisms-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'class')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->dropDownList($model->mechanismTypes) ?>

    <?= $form->field($model, 'images', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="photos_tech">Фотографии</label>
        <select name="photos_tech" id="photos_tech" class="form-control" multiple="multiple">
            <? foreach(Gallery::find()->all() as $photo):?>
                <option data-image="<?=$photo->image?>" value="<?=$photo->id?>"><?=$photo->name?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'video')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .matpic{
        width: 28px;
        height: 28px;
        border: 2px solid white;
    }
    .photopic{
        max-width: 120px;
        max-height: 120px;
        border: 2px solid white;
    }
</style>

<script>
    $(function() {

        var photoValues = $('input[name="Mechanisms[images]"]').val().split(',');
        $("#photos_tech").select2();
        $("#photos_tech").select2('val',photoValues);

        setTimeout(function(){
            $("#photos_tech").select2({
                formatResult: formatThumb,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; },
                sortResults: function(results, container, query) {
                    return results.sort(function(a, b) {
                        if (parseInt(a.id) < parseInt(b.id)) {
                            return 1;
                        } else if (parseInt(a.id) > parseInt(b.id)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            });
        }, 500);

        $("#photos_tech").on("change", function(e) {
            var photos = $(this).val();
            $('input[name="Mechanisms[images]"]').val(photos.toString());
        });

    });

    function formatOption (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="matpic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };


    function formatThumb (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="photopic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };
</script>