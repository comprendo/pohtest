<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */

$this->title = $model->name(["ЕД", "ИМ"]);
?>
<div class="catalog-view">

    <h2><?= Html::encode($this->title) ?> #<strong><?=$model->id?></strong></h2>

    <div class="panel">
        <div class="panel-body">
            <div class="pull-right t-right">
                Дата создания: <strong><?=  $model->getCreateDate("d MMMM y H:mm");?></strong> <br>
                <a href="<?= Url::toRoute(['log', 'id' => $model->id])?>">История изменений</a>
            </div>

            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент',
                'method' => 'post',
                'pjax' => '0',
                ],
            ]) ?>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => "table table-striped"],
                'attributes' => [
                    'id',
                    'itemcode',
                    [
                        'attribute' => 'user',
                        'format' => 'raw',
                        'value' => ($model->user)?Html::a($model->getUser()->login, ['/admin/users/view', 'id' => $model->user]):'-'
                    ],
                    'name',
                    'model',
                    'modelru',
                    'alias',
                    'pricenew',
                    'priceold',
                    [
                        'attribute' => 'category',
                        'format' => 'raw',
                        'value' => ($model->getCategory())?(Html::a($model->getCategory()->name, ['/admin/categories/view', 'id' => $model->category])):"-"
                    ],
                    'description:ntext',
                    'materials',
                    'mechanisms',
                    [
                        'attribute' => 'pdf',
                        'format' => 'raw',
                        'value' => Html::a("Скачать документ", [$model->pdf], ['class'=>'no-pjax', 'target'=>'_blank'])
                    ],
                    'photos',
                    'featured',
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => ($model->city)?Html::a($model->getCity()->name, ['/admin/cities/view', 'id' => $model->city]):'-'
                    ],
                    [
                        'attribute' => 'place',
                        'format' => 'raw',
                        'value' => ($model->place)?Html::a($model->getPlace()->name, ['/admin/places/view', 'id' => $model->place]):'-'
                    ],

                    [
                        'attribute' => 'active',
                        'value' => function($model) {
                            return ($model->active)?"Одобрено":"Ждет подтверждения";
                        }
                    ],
                    [
                        'attribute' => 'issold',
                        'value' => function($model) {
                            return ($model->issold)?"Продано":"Не продано";
                        }
                    ],
                    'views',
                    'seotag',
                ],
            ]) ?>
        </div>
    </div>
</div>
