<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Categories;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="catalog-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    'itemcode',
                    [
                        'attribute' => 'user',
                        'value' => function ($model) {
                            return ($model->getUser()) ? $model->getUser()->login : "-";
                        }
                    ],
                    'name',
                    'model',
                    //'alias',
                    [
                        'attribute' => 'category',
                        'filter' => Categories::getArray('id', 'name'),
                        'value' => function ($model) {
                            return ($model->getCategory()) ? ($model->getCategory()->name) : "-";
                        }
                    ],
                    //'description:ntext',
                    //'materials',
                    //'photos',
                    'views',
                    [
                        'attribute' => 'city',
                        'value' => function ($model) {
                            return ($model->getCity()) ? $model->getCity()->name : "-";
                        },
                        'filter' => Cities::getArray('id', 'name')
                    ],
                    [
                        'attribute' => 'active',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->active) ? "Одобрено" : "<span class='pending'>Ждет подтверждения</span>";
                        }
                    ],
                    [
                        'attribute' => 'issold',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return ($model->issold) ? "Продано" : "Не продано";
                        },
                        'filter' => array('0' => 'Не продано', '1' => 'Продано')
                    ],
                    // 'image:image',
                    // 'photos:image',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>


<style>
    .pending {
        color: #c75757;
        font-weight: bold;
    }
</style>