<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="offer-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'Получена',
                        'content' => function($model) {
                            return $model->getCreateDate("d MMMM y H:mm");
                        }
                    ],
                    'name',
                    [
                        'attribute' => 'city',
                        'value' => function($model) {
                            return ($model->getCity())?$model->getCity()->name:"-";
                        }
                    ],
                    [
                        'attribute' => 'email',
                        'value' => function($model) {
                            return ($model->email)?$model->email:"-";
                        }
                    ],
                    /*[
                        'attribute' => 'phone',
                        'value' => function($model) {
                            return ($model->phone)?$model->phone:"-";
                        }
                    ],*/
                    [
                        'attribute' => 'haveworked',
                        'format' => 'raw',
                        'value' => function($model) {
                            return ($model->haveworked)?"Работал":"Не работал";
                        }
                    ],
                    [
                        'attribute' => 'isstudio',
                        'format' => 'raw',
                        'value' => function($model) {
                            return ($model->isstudio)?"Агенство":"Частное лицо";
                        }
                    ],
                    [
                        'attribute' => 'processed',
                        'format' => 'raw',
                        'value' => function($model) {
                            return ($model->processed)?"Обработана":"<span class='newfeedback'>Новая</span>";
                        }
                    ],
             // 'haveworked',
             // 'isstudio',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>


<style>
    .newfeedback{
        color: #40b02a;
        font-weight: bold;
    }
</style>