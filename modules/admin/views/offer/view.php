<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Offer */

$this->title = $model->name(["ЕД", "ИМ"]);
?>
<div class="offer-view">

    <h2><?= Html::encode($this->title) ?> #<strong><?=$model->id?></strong></h2>

    <div class="panel">
        <div class="panel-body">
            <div class="pull-right t-right">
                Дата создания: <strong><?=  $model->getCreateDate("d MMMM y H:mm");?></strong> <br>
                <a href="<?= Url::toRoute(['log', 'id' => $model->id])?>">История изменений</a>
            </div>

            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент',
                'method' => 'post',
                'pjax' => '0',
                ],
            ]) ?>
            <?= $model->processed?"":Html::a('Принять', ['approve', 'id' => $model->id], ['class' => 'btn btn-dark']) ?>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => "table table-striped"],
                'attributes' => [
                    'id',
                    'name',
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => ($model->city)?Html::a($model->getCity()->name, ['/admin/cities/view', 'id' => $model->city]):'-'
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'value' => $model->email?$model->email:'-'
                    ],
                    [
                        'attribute' => 'phone',
                        'format' => 'raw',
                        'value' => $model->phone?$model->phone:'-'
                    ],
                    [
                        'attribute' => 'haveworked',
                        'format' => 'raw',
                        'value' => function($model) {
                            return ($model->haveworked)?"Работал":"Не работал";
                        }
                    ],
                    [
                        'attribute' => 'isstudio',
                        'format' => 'raw',
                        'value' => function($model) {
                            return ($model->isstudio)?"Агенство или студия":"Частное лицо";
                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
