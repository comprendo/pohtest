<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="gallery-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => function($model) {
                                return Html::img($model->image, ['width'=>'120']);
                            }
                    ],
                    'id',
                    /*[
                        'attribute' => 'user',
                        'value' => function($model) {
                                return ($model->getUser())?$model->getUser()->login:"-";
                            }
                    ],*/
                    'name',

                    //'image:image',
                    [
                        'class' => 'app\components\make\grid\ActionColumn',
                        'template' => '{view}{update}{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-trash t-3 p-r-0"></i>', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'data-pjax' => '0',
                                    'delete-url' => $url,
                                    'class' => 'btn btn-danger btn-sm ajaxDelete'
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<script>
    $(function() {
        $(document).on('click', '.ajaxDelete', function() {
            var deleteUrl = $(this).attr('delete-url');
            $.ajax({
                url: deleteUrl,
                type: "post",
            }).done(function() {
                location.reload();
            });
        });
    });

</script>
