<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="materials-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'image',
                        'format' => 'raw',
                        'value' => function($model) {
                                return Html::img($model->image, ['width'=>'32','height'=>'32']);
                            }
                    ],
                    'code',
                    'name',
                    [
                        'attribute' => 'type',
                        'value' => function($model) {
                                return $model->getMaterialType();
                            }
                    ],
                    [
                        'attribute' => 'matproducer',
                        'value' => function($model) {
                            return ($model->getMatproducer())?$model->getMatproducer()->name:"-";
                        }
                    ],
                    'shortdesc',
             // 'longdesc:ntext',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
