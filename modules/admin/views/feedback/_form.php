<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cities;
use app\models\Places;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */
/* @var $form yii\widgets\ActiveForm */
?>
<div style="text-align: right">
    <?= $model->getPrev() ? Html::a('Предыдущая заявка №'.$model->getPrev()->id, ['update?id='.$model->getPrev()->id], ['class' => 'btn btn-primary']) : "" ?>
    <?= $model->getNext() ? Html::a('Следующая заявка №'.$model->getNext()->id, ['update?id='.$model->getNext()->id], ['class' => 'btn btn-primary']) : "" ?>
</div>
<div class="feedback-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'processed')->checkbox() ?>
    <?= $form->field($model, 'type')->dropDownList($model->feedbackTypes) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city')->dropDownList( Cities::getArray('id','name') ) ?>
    <?= $form->field($model, 'place')->dropDownList( Places::getArray('id','name') ) ?>
    <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'referrer')->textInput(['maxlength' => true, 'disabled' => 'disabled']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>