<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = $model->name(["ЕД", "ИМ"]);
?>
<div class="feedback-view">

    <h2 style="margin-bottom: 20px;"><?= Html::encode($this->title) ?> #<strong><?=$model->id?></strong>
        <div style="display: inline-block; float: right; margin-right: -10px;">
            <?= $model->getPrev() ? Html::a('Предыдущая заявка №'.$model->getPrev()->id, ['view?id='.$model->getPrev()->id], ['class' => 'btn btn-primary']) : "" ?>
            <?= $model->getNext() ? Html::a('Следующая заявка №'.$model->getNext()->id, ['view?id='.$model->getNext()->id], ['class' => 'btn btn-primary']) : "" ?>
        </div>
    </h2>

    <div class="panel">
        <div class="panel-body">
            <div class="pull-right t-right">
                Дата создания: <strong><?=  $model->getCreateDate("d MMMM y H:mm");?></strong> <br>
                <a href="<?= Url::toRoute(['log', 'id' => $model->id])?>">История изменений</a>
            </div>

            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить этот элемент',
                    'method' => 'post',
                    'pjax' => '0',
                ],
            ]) ?>
            <?= Html::a('Скачать', ['save', 'id' => $model->id], ['class' => 'btn btn-success no-pjax', 'target' => "_blank"]) ?>
            <?= $model->processed?"":Html::a('Принять', ['approve', 'id' => $model->id], ['class' => 'btn btn-dark']) ?>

            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => "table table-striped"],
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'type',
                        'format' => 'raw',
                        'value' => $model->getFeedbackType()
                    ],
                    'name',
                    [
                        'attribute' => 'phone',
                        'format' => 'raw',
                        'value' => $model->phone?$model->phone:'-'
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'value' => $model->email?$model->email:'-'
                    ],
                    [
                        'attribute' => 'city',
                        'format' => 'raw',
                        'value' => ($model->city)?Html::a($model->getCity()->name, ['/admin/cities/view', 'id' => $model->city]):'-'
                    ],
                    [
                        'attribute' => 'place',
                        'format' => 'raw',
                        'value' => ($model->place)?Html::a($model->getPlace()->name, ['/admin/places/view', 'id' => $model->place]):'-'
                    ],
                    'message:ntext',
                    'referrer',
                ],
            ]) ?>
        </div>
    </div>

    <? if($model->catalog and $model->getCatalog()):?>
        <h3><b>Каталог #<?=$model->catalog?></b></h3>
        <div class="panel">
            <div class="panel-body">

                <?= DetailView::widget([
                    'model' => $model->getCatalog(),
                    'options' => ['class' => "table table-striped"],
                    'attributes' => [
                        'id',
                        'name',
                        'model',
                        'description:ntext',
                    ],
                ]) ?>
            </div>
        </div>
    <? endif?>
    <? if($model->discount and $model->getDiscount()):?>
        <h3><b>Дисконт #<?=$model->discount?></b></h3>
        <div class="panel">
            <div class="panel-body">

                <?= DetailView::widget([
                    'model' => $model->getDiscount(),
                    'options' => ['class' => "table table-striped"],
                    'attributes' => [
                        'id',
                        'name',
                        'model',
                        'pricenew',
                        'priceold',
                        'description:ntext',
                        [
                            'attribute' => 'city',
                            'format' => 'raw',
                            'value' => ($model->getDiscount()->city)?Html::a($model->getDiscount()->getCity()->name, ['/admin/cities/view', 'id' => $model->getDiscount()->city]):'-'
                        ],
                        [
                            'attribute' => 'place',
                            'format' => 'raw',
                            'value' => ($model->getDiscount()->place)?Html::a($model->getDiscount()->getPlace()->name, ['/admin/places/view', 'id' => $model->getDiscount()->place]):'-'
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    <? endif?>
</div>
