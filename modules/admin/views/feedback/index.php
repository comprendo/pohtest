<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="feedback-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'Получена',
                        'content' => function($model) {
                                return $model->getCreateDate("d MMMM y H:mm");
                            }
                    ],
                    /*[
                        'attribute' => 'type',
                        'value' => function($model) {
                                return $model->getFeedbackType();
                            }
                    ],*/
                    'name',
                    [
                        'attribute' => 'phone',
                        'value' => function($model) {
                                return ($model->phone)?$model->phone:"-";
                            }
                    ],
                    [
                        'attribute' => 'email',
                        'value' => function($model) {
                                return ($model->email)?$model->email:"-";
                            }
                    ],
                    [
                        'attribute' => 'city',
                        'value' => function($model) {
                                return ($model->getCity())?$model->getCity()->name:"-";
                            }
                    ],
                    /*'referrer',[
                        'attribute' => 'Модель',
                        'format' => 'raw',
                        'value' => function($model) {
                                if($model->catalog){
                                    return ($model->getCatalog())?Html::a($model->getCatalog()->name." ".$model->getCatalog()->model, ['/admin/catalog/view', 'id' => $model->catalog]):'<i class="fa fa-ban" aria-hidden="true"></i>';
                                }
                                if($model->discount){
                                    return ($model->getDiscount())?Html::a($model->getDiscount()->name." ".$model->getDiscount()->model, ['/admin/discount/view', 'id' => $model->discount]):'<i class="fa fa-ban" aria-hidden="true"></i>';
                                }
                                return '<i class="fa fa-ban" aria-hidden="true"></i>';
                            }
                    ],*/
                    [
                        'attribute' => 'processed',
                        'format' => 'raw',
                        'value' => function($model) {
                                return ($model->processed)?"Обработана":"<span class='newfeedback'>Новая</span>";
                            }
                    ],
             // 'city',
             // 'place',
             // 'message:ntext',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>


<style>
    .newfeedback{
        color: #40b02a;
        font-weight: bold;
    }
</style>