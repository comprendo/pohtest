<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Designers;
use app\models\Gallery;
use app\models\Interiors;

/* @var $this yii\web\View */
/* @var $model app\models\Interiors */
/* @var $form yii\widgets\ActiveForm */

if(!$model->alias){

    $model->alias = $model->generateAlias();
    if (($item = Interiors::findOne(['alias' => $model->alias])) !== null) {
        $model->alias .= "-1";
    };
}
?>

<div class="interiors-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($model, 'iswinner2')->checkbox() ?>
        <?= $form->field($model, 'isnew2')->checkbox() ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'images', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="photos_tech">Фотографии</label>
        <select name="photos_tech" id="photos_tech" class="form-control" multiple="multiple">
            <? foreach(Gallery::find()->all() as $photo):?>
                <option data-image="<?=$photo->image?>" value="<?=$photo->id?>"><?=$photo->name?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'shortdesc')->textarea(['rows' => 3]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'attributes', ['template' => '{input}'])->hiddenInput() ?>
    <div id="attributes">
        <label for="">Атрибуты</label>

    </div>
    <a class="btn btn-primary" id="add-attribute">Добавить атрибут</a>

    <?= $form->field($model, 'designer')->dropDownList(Designers::getArray('id','name')) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="hidden templates">

    <!-- Атрибуты -->
    <div class="attribute-template">
        <div class="attribute panel panel-default">
            <div class="panel-heading">
                <div class="form-group">
                    <label>Атрибут:</label>
                    <input type="text" name="attribute-name" class="form-control attribute-name form-white">
                </div>
                <div class="form-group">
                    <label>Значение:</label>
                    <input type="text" name="attribute-value" class="form-control attribute-value form-white">
                </div>
                <a class="btn btn-danger remove-attribute">Удалить атрибут</a>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">

    var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"'","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"'","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"'","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"'","б":"b","ю":"yu"};

    function transliterate(word){
        return word.split('').map(function (char) {
            return a[char] || char;
        }).join("");
    }
    function ToSeoUrl(url) {

        // make the url lowercase
        var encodedUrl = url.toString().toLowerCase();

        // replace & with and
        encodedUrl = encodedUrl.split(/\&+/).join("-and-")

        // remove invalid characters
        encodedUrl = encodedUrl.split(/[^a-z0-9]/).join("-");

        // remove duplicates
        encodedUrl = encodedUrl.split(/-+/).join("-");

        // trim leading & trailing characters
        encodedUrl = encodedUrl.trim('-');

        return encodedUrl;
    }

    $(document).ready(function() {

        $('input[name="Interiors[name]"]').keypress(function(){
            $('input[name="Interiors[alias]"]').val(ToSeoUrl(transliterate($(this).val())));
        });
        $('input[name="Interiors[name]"]').change(function(){
            $('input[name="Interiors[alias]"]').val(ToSeoUrl(transliterate($(this).val())));
        });

        $("#add-attribute").click(addAttribute);
        $("#save").click(save);
        $(this).on("click", ".attribute .remove-attribute", function() {
                $(this).closest(".attribute").remove();
            });

        $("form.pjax-form").on("beforeValidate", function() {
            $("#interiors-attributes").val(JSON.stringify(save()));
        });

        if($("#interiors-attributes").val()){
            var attributes = JSON.parse($("#interiors-attributes").val());

            for (var i in attributes) {
                var attribute = addAttribute();
                $(attribute).find(".attribute-name").val(i);
                $(attribute).find(".attribute-value").val(attributes[i]);
            }
        }


    });

    function addAttribute() {
        var container = $("#attributes");
        var template = $(".templates .attribute-template").clone().html();
        $(template).appendTo(container);
        return $("#attributes .attribute:last");
    }

    function save() {
        var attributes = {};

        $("#attributes .attribute").each(function() {
            var name = $(this).find(".attribute-name").val();
            attributes[name] = $(this).find(".attribute-value").val();
        });

        return attributes;
    }

</script>


<style>
    .matpic{
        width: 28px;
        height: 28px;
        border: 2px solid white;
    }
    .photopic{
        max-width: 120px;
        max-height: 120px;
        border: 2px solid white;
    }
</style>

<script>
    $(function() {

        var photoValues = $('input[name="Interiors[images]"]').val().split(',');
        $("#photos_tech").select2();
        $("#photos_tech").select2('val',photoValues);

        setTimeout(function(){
            $("#photos_tech").select2({
                formatResult: formatThumb,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; },
                sortResults: function(results, container, query) {
                    return results.sort(function(a, b) {
                        if (parseInt(a.id) < parseInt(b.id)) {
                            return 1;
                        } else if (parseInt(a.id) > parseInt(b.id)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            });
        }, 500);

        $("#photos_tech").on("change", function(e) {
            var photos = $(this).val();
            $('input[name="Interiors[images]"]').val(photos.toString());
        });

    });

    function formatOption (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="matpic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };


    function formatThumb (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="photopic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };
</script>