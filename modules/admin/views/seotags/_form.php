<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Seotags */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seotags-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <p>Введите название набора мета-тегов. В дальнейшем это название будет использоваться для удобства поиска и прикрепления данного набора к странице.</p>
    <hr>
    <?= $form->field($model, 'keywords')->textInput(['class' => 'select-tags']) ?>
    <p>Введите ключевые слова. Разделять запятой нет необходимости. После того, как слово введено нажимте ENTER и вводите следующее.</p>
    <hr>
    <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    <p>Введите описание страницы.</p><p class="desc_lbl">Использовано символов:  <span class="desc_counter">0</span> / 160</p>
    <hr>
    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .desc_lbl{
        display: none;
    }
    .desc_counter{
        margin-left: 5px;
    }
</style>

<script>


        $("#seotags-description").on("input propertychange", function(e) {
            $('.desc_lbl').show();
            var desc = $(this).val();

            $('.desc_counter').html(desc.length);
            if(desc.length>160){
                $(this).val(desc.substring(0, 159));
            }
        });
        $("#seotags-description").on("change", function(e) {
            $('.desc_lbl').show();
            var desc = $(this).val();

            $('.desc_counter').html(desc.length);
            if(desc.length>160){
                $(this).val(desc.substring(0, 159));
            }
        });



</script>