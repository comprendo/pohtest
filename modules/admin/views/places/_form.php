<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Cities;
use app\models\Gallery;

/* @var $this yii\web\View */
/* @var $model app\models\Places */
/* @var $form yii\widgets\ActiveForm */

if(!$model->coords)
    $model->coords = "59.94998504197073, 30.31648660888675";
$coords = explode(",",$model->coords);
?>

<div class="places-form">

    <?php if (count($model->errors)): ?>
        <div class="alert alert-danger">
            <?php foreach($model->errors as $attr) echo implode("<br>", $attr); ?>
        </div>
    <? endif;?>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'pjax-form', 'enctype' => 'multipart/form-data']]); ?>
    <?= $form->field($model, 'country')->dropDownList( Cities::getCountries() ) ?>
        <?= $form->field($model, 'city')->dropDownList( Cities::getArray('id','name') ) ?>
    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'schedule')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'images', ['template' => '{input}'])->hiddenInput() ?>
    <div class="form-group">
        <label class="control-label" for="photos_tech">Фотографии</label>
        <select name="photos_tech" id="photos_tech" class="form-control" multiple="multiple">
            <? foreach(Gallery::find()->all() as $photo):?>
                <option data-image="<?=$photo->image?>" value="<?=$photo->id?>"><?=$photo->name?></option>
            <? endforeach?>
        </select>
        <div class="help-block"></div>
    </div>

    <?= $form->field($model, 'coords')->textInput(['maxlength' => true/*, "disabled" => "disabled"*/]) ?>

    <div class="form-group">
        <div id="map"></div>
        <script>
            function initMap() {

                var styledMapType = new google.maps.StyledMapType(
                    [
                        {
                            "featureType": "administrative",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                }
                            ]
                        },
                        {
                            "featureType": "transit.line",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                },
                            ]
                        },
                        {
                            "featureType": "transit.station",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "hue": "#ff0000"
                                },
                                {
                                    "saturation": "0"
                                },
                                {
                                    "lightness": "0"
                                },
                                {
                                    "gamma": "0.00"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#fcfcfc"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#fcfcfc"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#dddddd"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#dddddd"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#eeeeee"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "simplified"
                                },
                                {
                                    "color": "#dddddd"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#434343"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "labels.text",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        }
                    ],
                    {name: 'Салоны'}
                );

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 13,
                    center: {lat: <?=$coords[0]?>, lng: <?=$coords[1]?>},
                    scrollwheel: false,
                    mapTypeControlOptions: {
                        mapTypeIds: ['styled_map']
                    }
                });
                map.mapTypes.set('styled_map', styledMapType);
                map.setMapTypeId('styled_map');

                var marker = new google.maps.Marker({
                        position: {lat: <?=$coords[0]?>, lng: <?=$coords[1]?>},
                        map: map,
                        icon: '/images/places/marker.png',
                        draggable: true
                    });

                google.maps.event.addListener(marker, "dragend", function (event) {
                    var latitude = event.latLng.lat();
                    var longitude = event.latLng.lng();
                    $('#places-coords').val( latitude + ', ' + longitude );
                });
            }
        </script>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsbYX8zR9lfe7Sp24wFNSBhK-x3AIx2i4&callback=initMap">
</script>


<style>
    .matpic{
        width: 28px;
        height: 28px;
        border: 2px solid white;
    }
    .photopic{
        max-width: 120px;
        max-height: 120px;
        border: 2px solid white;
    }
</style>

<script>
    $(function() {

        var photoValues = $('input[name="Places[images]"]').val().split(',');
        $("#photos_tech").select2();
        $("#photos_tech").select2('val',photoValues);

        setTimeout(function(){
            $("#photos_tech").select2({
                formatResult: formatThumb,
                formatSelection: formatOption,
                escapeMarkup: function(m) { return m; },
                sortResults: function(results, container, query) {
                    return results.sort(function(a, b) {
                        if (parseInt(a.id) < parseInt(b.id)) {
                            return 1;
                        } else if (parseInt(a.id) > parseInt(b.id)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                }
            });
        }, 500);

        $("#photos_tech").on("change", function(e) {
            var photos = $(this).val();
            $('input[name="Places[images]"]').val(photos.toString());
        });

    });

    function formatOption (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="matpic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };


    function formatThumb (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if(!optimage){
            return opt.text;
        } else {
            var $opt = $(
                '<span ><img src="' + optimage + '" class="photopic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    };
</script>