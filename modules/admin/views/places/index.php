<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Cities;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="places-index">

    <h2><?= Html::encode($title) ?></h2>

    <div class="panel">
        <div class="panel-body">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-action'
                ],
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    'id',
                    [
                        'attribute' => 'city',
                        'value' => function($model) {
                                return ($model->getCity())?$model->getCity()->name:"-";
                            },
                        'filter'=> Cities::getArray('id','name')
                    ],
                    'name',
                    'sort',
              //      'address',
              //      'phone',
             // 'schedule',
             // 'images:image',
             // 'coords',
                    ['class' => 'app\components\make\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
