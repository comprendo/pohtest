<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Feedback;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AccessControl;
use app\components\yii2mpdf\Pdf;

/**
 * FeedbackController Реализовывает CRUD для модели Feedback.
 */
class FeedbackController extends Controller
 {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [ 'allow' => true, 'roles' => ['admin', 'manager'] ],
                ],
            ],
        ];
    }

    /**
     * Отображает все записи модели Feedback.
     * @return mixed
     */
    public function actionIndex($type = null) {
        $query = Feedback::find();

        if(!Yii::$app->getRequest()->getQueryParam('sort'))
            $query->orderBy(['id' => SORT_DESC]);

        if(isset($type))
            $query->where(['type' => intval($type)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' =>  Feedback::name(["МН","ИМ"])
        ]);
    }

    /**
     * Показывает одну запись модели Feedback.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Показывает одну запись модели Feedback.
     * @param integer $id
     * @return mixed
     */
    public function actionSave($id) {
        $response = Yii::$app->getResponse();
        $model = $this->findModel($id);

        $content = "Заявка: \r\n";
        //$content .= "ID: ".$model->id."\r\n";
        $content .= "Дата поступления: ".$model->getCreateDate("d MMMM y H:mm")."\r\n";
        //$content .= "Тип заявки: ".$model->getFeedbackType()."\r\n";
        $content .= "Имя: ".$model->name."\r\n";
        $content .= "Телефон: ".($model->phone?$model->phone:"Не указано")."\r\n";
        $content .= "Email: ".($model->email?$model->email:"Не указано")."\r\n";
        $content .= "Город: ".($model->city?$model->getCity()->name:"Не указано")."\r\n";
        $content .= "Салон: ".($model->place?$model->getPlace()->name:"Не указано")."\r\n";
        $content .= "Сообщение: ".$model->message."\r\n";

        if($model->catalog){
            $catalog = $model->getCatalog();

            $content .= "\r\nМодель каталога: \r\n";
            //$content .= "ID: ".$catalog->id."\r\n";
            $content .= "Название: ".$catalog->name."\r\n";
            $content .= "Модель: ".$catalog->model."\r\n";
            $content .= "Описание: ".$catalog->description."\r\n";
        }

        if($model->discount){
            $discount = $model->getDiscount();

            $content .= "\r\nМодель дисконта: \r\n";
            //$content .= "ID: ".$discount->id."\r\n";
            $content .= "Название: ".$discount->name."\r\n";
            $content .= "Модель: ".$discount->model."\r\n";
            $content .= "Новая цена: ".$discount->pricenew."\r\n";
            $content .= "Старая цена: ".$discount->priceold."\r\n";
            $content .= "Город: ".($discount->city?$discount->getCity()->name:"Не указано")."\r\n";
            $content .= "Салон: ".($discount->place?$discount->getPlace()->name:"Не указано")."\r\n";
        }

        $response->sendContentAsFile($content, "feedback_".$model->id.".txt", [
            'mimeType' => 'text/plain',
            'inline' => false,
        ]);

        $response->send();
        return true;
    }

    /**
     * Показывает одну запись модели Feedback.
     * @param integer $id
     * @return mixed
     */
    public function actionApprove($id) {
        $model = $this->findModel($id);

        $model->processed=1;
        $model->save();

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Создает запись запись модели Feedback.
     * Если создание прошло успешно, будет совершен редирект на страницу просмотра.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->runAction("view", ['id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет существующую запись модели Feedback.
     * Если обновление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('view', [
            'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Показывать измеения в модели Feedback.
    * @param integer $id
    * @return mixed
    */
    public function actionLog($id) {
        return $this->render('log', [
            'model' => $this->findModel($id),
            'log' => $this->findModel($id)->getLog(),
        ]);
    }

    /**
     * Удаляет запись из модели Feedback.
     * Если удаление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->runAction($this->defaultAction);

    }

    /**
     * Находит запись в модели Feedback основыаясь на внешнем ключе.
     * Если запись не найдена, выбрасывает ошибку 404.
     * @param integer $id
     * @return Feedback найденная запись
     * @throws NotFoundHttpException если модель не была найдена
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
