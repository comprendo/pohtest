<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Discount;
use app\models\Places;
use app\models\search\DiscountSearch;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AccessControl;

/**
 * DiscountController Реализовывает CRUD для модели Discount.
 */
class DiscountController extends Controller
 {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [ 'allow' => true, 'roles' => ['admin', 'manager'] ],
                ],
            ],
        ];
    }

    /**
     * Отображает все записи модели Discount.
     * @return mixed
     */
    public function actionIndex() {
        /*$query = Discount::find();

        if(!Yii::$app->getRequest()->getQueryParam('sort'))
            $query->orderBy(['active' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' =>  Discount::name(["МН","ИМ"])
        ]);*/

        $searchModel = new DiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Показывает одну запись модели Discount.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Создает запись запись модели Discount.
     * Если создание прошло успешно, будет совершен редирект на страницу просмотра.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Discount();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->runAction("view", ['id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет существующую запись модели Discount.
     * Если обновление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('view', [
            'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Показывать измеения в модели Discount.
    * @param integer $id
    * @return mixed
    */
    public function actionLog($id) {
        return $this->render('log', [
            'model' => $this->findModel($id),
            'log' => $this->findModel($id)->getLog(),
        ]);
    }

    /**
     * Удаляет запись из модели Discount.
     * Если удаление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->runAction($this->defaultAction);

    }

    /**
     * Находит запись в модели Discount основыаясь на внешнем ключе.
     * Если запись не найдена, выбрасывает ошибку 404.
     * @param integer $id
     * @return Discount найденная запись
     * @throws NotFoundHttpException если модель не была найдена
     */
    protected function findModel($id)
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }

    /**
     * Обновляет список салонов при изменении города.
     */
    public function actionSalonsupdate() {
        // $data=Places::model()->findAll('city=:cityid', array(':cityid'=>(int) $_GET['cityid']));
    
        // $data=CHtml::listData($data,'id','name');
        // foreach($data as $value=>$name)
        // {
        //     echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
        // }



        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Ничего не найдено.');
        }

        if(!$cityid = Yii::$app->getRequest()->getQueryParam('cityid') ) {
            return false;
        }

        $salonsList = array();

        $salonsList[] = Places::find()->select('id, name')->where(['city' => $cityid])->asArray()->all();

        echo json_encode($salonsList);
    }
}
