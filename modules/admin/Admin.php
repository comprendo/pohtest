<?php

namespace app\modules\admin;
use \yii\base\Module;
use \app\models;

class Admin extends Module {

    // Контроллеры, которые могут использоваться в админке
    public $controllers = [

        "default" => [
            "url" => ["/admin"],
            "title" => "Главная",
            "icon" => "icon-home",
        ],

        "meta-tags" => [
            "url" => ["/admin/meta-tags"],
            "title" => "Мета-теги"
        ],

        "users" => [
            "url" => ["/admin/users/"],
            "title" => "Пользователи",
            "icon" => "fa fa-group",
            /*"badge" => [
                "model" => 'app\models\Users',
                "condition" => ["status" => 1]
            ]*/
        ],

        "settings" => [
            "url" => ["/admin/settings/"],
            "title" => "Настройки",
            "icon" => "fa fa-group",
        ],

        "catalog" => [
            "url" => ["/admin/catalog/"],
            "title" => "Коллекции",
            "icon" => "fa fa-folder",
        ],
        /*"discount" => [
            "url" => ["/admin/discount/"],
            "title" => "Дисконт",
            "icon" => "fa fa-dollar",
            "badge" => [
                "model" => 'app\models\Discount',
                "condition" => ["active" => 0]
            ]
        ],*/
        "discount_1" => [
            "url" => ["/admin/discount/index?DiscountSearch%5Bissold%5D=0"],
            "title" => "Дисконт",
            "icon" => "fa fa-dollar",
            "badge" => [
                "model" => 'app\models\Discount',
                "condition" => ["active" => 0, 'issold' => 0]
            ]
        ],
        "discount_2" => [
            "url" => ["/admin/discount/index?DiscountSearch%5Bissold%5D=1"],
            "title" => "Продано",
            "icon" => "fa fa-dollar",
        ],
        "feedback_all" => [
            "url" => ["/admin/feedback/"],
            "title" => "Все заявки",
            "icon" => "fa fa-envelope",
        ],
        "feedback_1" => [
            "url" => ["/admin/feedback/index?type=0"],
            "title" => "Обратная связь",
            "icon" => "fa fa-envelope",
            "badge" => [
                "model" => 'app\models\Feedback',
                "condition" => ["type" => 0, 'processed' => 0]
            ]
        ],
        "feedback_2" => [
            "url" => ["/admin/feedback/index?type=1"],
            "title" => "Заявки",
            "icon" => "fa fa-envelope",
            "badge" => [
                "model" => 'app\models\Feedback',
                "condition" => ["type" => 1, 'processed' => 0]
            ]
        ],
        "feedback_3" => [
            "url" => ["/admin/feedback/index?type=2"],
            "title" => "Заявки на каталог",
            "icon" => "fa fa-envelope",
            "badge" => [
                "model" => 'app\models\Feedback',
                "condition" => ["type" => 2, 'processed' => 0]
            ]
        ],
        "offer" => [
            "url" => ["/admin/offer/"],
            "title" => "Сотрудничество",
            "icon" => "fa fa-envelope",
            "badge" => [
                "model" => 'app\models\Offer',
                "condition" => ['processed' => 0]
            ]
        ],
        "categories" => [
            "url" => ["/admin/categories/"],
            "title" => "Структура",
            "icon" => "fa fa-sitemap",
        ],
        "heros" => [
            "url" => ["/admin/heros/"],
            "title" => "Фоны",
            "icon" => "fa fa-sitemap",
        ],
        "cities" => [
            "url" => ["/admin/cities/"],
            "title" => "Города",
            "icon" => "fa fa-sitemap",
        ],
        "countries" => [
            "url" => ["/admin/countries"],
            "title" => "Страны",
            "icon" => "fa fa-sitemap",
        ],
        "gallery" => [
            "url" => ["/admin/gallery/"],
            "title" => "Галерея",
            "icon" => "fa fa-image",
        ],
        "materials" => [
            "url" => ["/admin/materials/"],
            "title" => "Материалы",
            "icon" => "fa fa-clone",
        ],
        "mechanisms" => [
            "url" => ["/admin/mechanisms/"],
            "title" => "Механизмы",
            "icon" => "fa fa-clone",
        ],
        "history" => [
            "url" => ["/admin/history/"],
            "title" => "История",
            "icon" => "fa fa-clone",
        ],
        "events" => [
            "url" => ["/admin/events/"],
            "title" => "Актуальное",
            "icon" => "fa fa-newspaper-o",
        ],
        "interiors" => [
            "url" => ["/admin/interiors/"],
            "title" => "Интерьеры",
            "icon" => "fa fa-newspaper-o",
        ],
        "designers" => [
            "url" => ["/admin/designers/"],
            "title" => "Дизайнеры",
            "icon" => "fa fa-newspaper-o",
        ],
        "partnership" => [
            "url" => ["/admin/partnership/"],
            "title" => "Сотрудничество",
            "icon" => "fa fa-newspaper-o",
        ],
        "places" => [
            "url" => ["/admin/places/"],
            "title" => "Салоны",
            "icon" => "fa fa-map-marker",
        ],

        "seotags" => [
            "url" => ["/admin/seotags/"],
            "title" => "Наборы мета-тегов",
            "icon" => "fa fa-folder",
        ],
        "matproducers" => [
            "url" => ["/admin/matproducers/"],
            "title" => "Производители",
            "icon" => "fa fa-folder",
        ],
    ];

    /**
     * @var array
     *
     * Пункты меню для отображения в layout. Формат:
     *
     * "default" => [
     *      "items" => ["card", "users"], - перечисление контроллеров из $this->controllers
     *      "icon" => "glyphicon glyphicon-off", - класс иконки, который получит категория
     *      "title" => "Пользователи" - название категории
     * ],
     *
     * Можно просто указать "users" для отображения контроллера без категории
     */
    public $menu = [
        /*"default" => [
            "items" => ["users", "users"],
            "icon" => "glyphicon glyphicon-off",
            "title" => "Пользователи"
        ],*/

        "default",
        "users",
        "catalog",
        //"discount",
        "discount" => [
            "items" => ["discount_1", "discount_2"],
            "icon" => "fa fa-dollar",
            "title" => "Дисконт",
        ],
        "gallery",
        "special" => [
            "items" => ["materials", "mechanisms", "matproducers"],
            "icon" => "fa fa-star",
            "title" => "Эксклюзив"
        ],
        "about" => [
            "items" => ["history"],
            "icon" => "fa fa-info-circle",
            "title" => "О компании"
        ],
        "places",
        "events",
        "interiors" => [
            "items" => ["interiors", "designers", "partnership"],
            "icon" => "fa fa-paint-brush",
            "title" => "Интерьеры"
        ],
        "feedback" => [
            "items" => ["feedback_all", "feedback_1", "feedback_2", "feedback_3"],
            "icon" => "fa fa-envelope",
            "title" => "Заявки",
        ],
        "offer",
        "additional" => [
            "items" => ["categories", "countries", "cities", "heros", "seotags"],
            "icon" => "fa fa-sitemap",
            "title" => "Система",
        ]

    ];
}