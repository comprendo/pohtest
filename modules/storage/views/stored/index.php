<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Хранилище файлов';

?>

<div class="stored-index">


    <?= Html::a('Загрузить файл', ['create'], ['class' => 'btn btn-success']) ?>
    <div class="item-list">
        <? if(!$items):?>
            <p>Ничего не найдено.</p>
        <? endif?>
        <div class="row">
            <? foreach($items as $item):?>
                <div class="col-xs-6 col-md-3">
                    <div class="item">
                            <a href="<?=$item->file?>" target="_blank" class="thumb no-pjax" <?=($item->cover)?'style="background-image: url('.$item->cover.')"':($item->isImage()?'style="background-image: url('.$item->file.')"':'')?>>
                                <? if(!$item->isImage()):?>
                                    <span class="ext"><i class="fa <?=$item->getIcon()?>" aria-hidden="true"></i></span>
                                <? endif?>
                                <span class="ovr"><i class="fa fa-download" aria-hidden="true"></i></span>
                            </a>

                        <div class="desc">
                            <div class="name"><b>Имя: </b><?=$item->name?></div>
                            <div class="type"><b>Тип: </b><?=$item->getType()?></div>
                            <? if($item->user==$current_user):?>
                                    <a class="file-edit-btn" href="/storage/stored/view?id=<?=$item->id?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <? endif?>
                        </div>
                    </div>
                </div>
            <? endforeach?>
        </div>

    </div>

</div>

