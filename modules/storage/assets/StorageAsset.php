<?php

namespace app\modules\storage\assets;

use yii\web\AssetBundle;

class StorageAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/storage/assets/';
    public $css = [
        'css/style.css',
        'css/test.scss',
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
