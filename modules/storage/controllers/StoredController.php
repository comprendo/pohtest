<?php

namespace app\modules\storage\controllers;

use Yii;
use app\models\Stored;
use app\models\Users;
use yii\data\ActiveDataProvider;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\AccessControl;

/**
 * StoredController Реализовывает CRUD для модели Stored.
 */
class StoredController extends Controller
 {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [ 'allow' => true, 'roles' => ['admin', "manager", 'user'] ],
                ],
            ],
        ];
    }

    /**
     * Отображает все записи модели Stored.
     * @return mixed
     */
/*    public function actionIndex($type = null) {
        $query = Stored::find();

        if(!Yii::$app->getRequest()->getQueryParam('sort'))
            $query->orderBy(['id' => SORT_DESC]);

        if(isset($type))
            $query->where(['type' => intval($type)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'title' =>  Stored::name(["МН","ИМ"])
        ]);
    }*/

    public function actionIndex($type = 0) {
        $query = Stored::find();
        $user = Users::findOne(Yii::$app->user->id);

        switch ($type) {
            case 0:
                $query->andWhere(['user' => $user->id]);
                break;
            case 1:
                $query->andWhere(['type' => 1]);
                break;
            case 2:
                $query->andWhere(['type' => 2]);
                break;
            default:
                $query->andWhere(['user' => $user->id]);
        }

        return $this->render('index', [
            'current_user' => $user->id,
            'items' => $query->all(),
            'title' =>  Stored::name(["МН","ИМ"])
        ]);
    }

    /**
     * Показывает одну запись модели Stored.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $user = Users::findOne(Yii::$app->user->id);

        if($user->id!=$model->user)
            return $this->redirect('/storage/stored');

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Создает запись запись модели Stored.
     * Если создание прошло успешно, будет совершен редирект на страницу просмотра.
     * @return mixed
     */
    public function actionCreate() {
        $user = Users::findOne(Yii::$app->user->id);

        $model = new Stored();

        if ($model->load(Yii::$app->request->post())) {
            $model->user = $user->id;

            if($model->save()){
                return $this->runAction("view", ['id' => $model->id]);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Обновляет существующую запись модели Stored.
     * Если обновление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $user = Users::findOne(Yii::$app->user->id);

        if($user->id!=$model->user)
            return $this->redirect('/storage/stored');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->render('view', [
            'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
    * Показывать измеения в модели Stored.
    * @param integer $id
    * @return mixed
    */
   /* public function actionLog($id) {
        return $this->render('log', [
            'model' => $this->findModel($id),
            'log' => $this->findModel($id)->getLog(),
        ]);
    }*/

    /**
     * Удаляет запись из модели Stored.
     * Если удаление прошло успешно, будет совершен редирект на страницу просмотра.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $user = Users::findOne(Yii::$app->user->id);

        if($user->id!=$model->user)
            return $this->redirect('/storage/stored');

        $model->delete();
        return $this->runAction($this->defaultAction);

    }

    /**
     * Находит запись в модели Stored основыаясь на внешнем ключе.
     * Если запись не найдена, выбрасывает ошибку 404.
     * @param integer $id
     * @return Stored найденная запись
     * @throws NotFoundHttpException если модель не была найдена
     */
    protected function findModel($id)
    {
        if (($model = Stored::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
