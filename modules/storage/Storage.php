<?php

namespace app\modules\storage;
use \yii\base\Module;
use \app\models;

class Storage extends Module {

    // Контроллеры, которые могут использоваться в админке
    public $controllers = [

        "default" => [
            "url" => ["/storage"],
            "title" => "Личный кабинет",
            "icon" => "icon-home",
        ],

        "private_stored" => [
            "url" => ["/storage/stored/index?type=0"],
            "title" => "Мои файлы",
            "icon" => "fa fa-lock",
        ],
        "internal_stored" => [
            "url" => ["/storage/stored/index?type=1"],
            "title" => "Внутренние файлы",
            "icon" => "fa fa-unlock-alt",
        ],
        "public_stored" => [
            "url" => ["/storage/stored/index?type=2"],
            "title" => "Публичные файлы",
            "icon" => "fa fa-upload",
        ],
    ];

    /**
     * @var array
     *
     * Пункты меню для отображения в layout. Формат:
     *
     * "default" => [
     *      "items" => ["card", "users"], - перечисление контроллеров из $this->controllers
     *      "icon" => "glyphicon glyphicon-off", - класс иконки, который получит категория
     *      "title" => "Пользователи" - название категории
     * ],
     *
     * Можно просто указать "users" для отображения контроллера без категории
     */
    public $menu = [
        /*"default" => [
            "items" => ["users", "users"],
            "icon" => "glyphicon glyphicon-off",
            "title" => "Пользователи"
        ],*/
        //"default",
        "private_stored",
        "internal_stored",
        "public_stored",


    ];
}