<?php

namespace app\controllers;

use app\models\Events;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class EventsController extends Controller {

    public $catparent = 0;
    public $herocustom;
    public $herobg = 4;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex($category = null) {
        $this->catparent = 43; // Родительская категория - Актуальное
        $query = Events::find();

        if( $category ){
            $query->andWhere(['category' => $category]);
            $this->herocustom = '<a class="btn-back" href="/events/"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться к статьям</a>';

            switch ($category) {
                case 3:
                    $cattitle = "Акции";
                    break;
                case 2:
                    $cattitle = "Новости";
                    break;
                case 1:
                    $cattitle = "Интересное";
                    break;
            }
        }

        $query->orderBy(['id' => SORT_DESC]);
        $query->limit(6);

        return $this->render('index', [
            'items' => $query->all(),
            'category' => ($category) ? $category : false,
            'title' => (isset($cattitle)) ? $cattitle : "Актуальное"
        ]);
    }

    public function actionSpecial() {
        return $this->redirect('/events/?category=3');
    }
    public function actionNews() {
        return $this->redirect('/events/?category=2');
    }
    public function actionInteresting() {
        return $this->redirect('/events/?category=1');
    }


    /**
     * Показывает одну запись модели Event.
     * @param integer $id
     * @return mixed
     */
    public function actionArticle($id) {
        $this->herocustom = '<a class="btn-back" href="/events/"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться к статьям</a>';

        //$item = $this->findModel($id);
        $item = $this->findModelByAlias($id);
        $item->views = $item->views+1;
        $item->save();

        return $this->render('article', [
            'model' => $item
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }

    protected function findModelByAlias($alias)
    {
        if (($model = Events::findOne(['alias' => $alias])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
