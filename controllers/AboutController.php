<?php

namespace app\controllers;

use app\models\History;
use app\models\Materials;
use app\models\Mechanisms;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class AboutController extends Controller {

    public $catparent = 20;
    public $herocustom;
    public $herobg = 1;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect('/about/history');
    }

    public function actionHistory() {
        $slides = History::find();

        return $this->render('history', [
            'slides' => $slides->where(['type' => 0 ])->all(),
            'intro' => $slides->where(['type' => 1 ])->one(),
            'outro' => $slides->where(['type' => 2 ])->one(),
        ]);
    }

    public function actionMaterials() {
        return $this->redirect('/special/materials');
    }
    public function actionMechanisms() {
        return $this->redirect('/special/mechanisms');
    }
    public function actionResponsibility() {
        return $this->render('responsibility', [
        ]);
    }

}
