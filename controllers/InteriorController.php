<?php

namespace app\controllers;

use app\models\OfferForm;
use app\models\Interiors;
use app\models\Stored;
use app\models\Designers;
use app\models\Partnership;
use app\models\FeedbackForm;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class InteriorController extends Controller {

    public $catparent = 25;
    public $herocustom;
    public $herobg = 6;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect('/interior/solutions');
    }

    public function actionSolutions() {
        $interioirs = Interiors::find();

        return $this->render('solutions', [
            'items' => $interioirs->limit(120)->orderBy(['id' => SORT_DESC])->all(),
        ]);
    }

    public function actionDesign($id) {
        $this->herocustom = '<a class="btn-back" href="/interior/solutions/"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться к интерьерам</a>';

        //$item = $this->findModel($id);
        $item = $this->findModelByAlias($id);

        return $this->render('design', [
            'model' => $item,
            'designer' => $item->getDesigner(),
        ]);
    }

    public function actionDesigners() {
        return $this->redirect('/interior/solutions');
        //раздел отключен поэтому редиректим в солюшнс

        $slides = Partnership::find();

        $order = new FeedbackForm();

        if ($order->load(Yii::$app->request->post()) && $order->message()) {

            return $this->render('designers', [
                'success' => true,
                'order' => $order,
                'slides' => $slides->where(['type' => 0 ])->all(),
            ]);

        }

        return $this->render('designers', [
            'order' => $order,
            'slides' => $slides->where(['type' => 0 ])->all(),
        ]);
    }

    public function actionOffer() {

        $model = new OfferForm();

        if ($model->load(Yii::$app->request->post()) && $model->message()) {

            return $this->render('offer', [
                'success' => true,
                'model' => $model,
            ]);

        }

        return $this->render('offer', [
            'model' => $model,
        ]);
    }

    public function actionContest() {
        return $this->render('contest', []);
    }
    public function action3dmodels() {
        $model = new FeedbackForm();

        if ($model->load(Yii::$app->request->post()) && $model->message()) {

            return $this->render('3dmodels', [
                'success' => true,
                'model' => $model,
            ]);

        }

        return $this->render('3dmodels', [
            'model' => $model,
        ]);
    }

    public function actionStorage() {
        $query = Stored::find();

        $query->andWhere(['type' => 2]);

        return $this->render('storage', [
            'items' => $query->all(),
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Interiors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }

    protected function findModelByAlias($alias)
    {
        if (($model = Interiors::findOne(['alias' => $alias])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }

}
