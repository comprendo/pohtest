<?php

namespace app\controllers;

use app\models\FeedbackForm;
use Yii;
use app\components\AccessControl;
use app\models\ContactForm;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class FeedbackController extends Controller {

    public $catparent = 0;
    public $herocustom;
    public $herobg = 5;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex($catalog = null, $discount = null) {

        $model = new FeedbackForm();

        if ($model->load(Yii::$app->request->post()) && $model->message()) {

            return $this->render('index', [
                'success' => true,
                'model' => $model,
                'catalog' => $catalog ? htmlspecialchars($catalog) : false,
                'discount' => $discount ? htmlspecialchars($discount) : false
            ]);

        }

        return $this->render('index', [
            'model' => $model,
            'catalog' => $catalog ? htmlspecialchars($catalog) : false,
            'discount' => $discount ? htmlspecialchars($discount) : false
        ]);
    }

}
