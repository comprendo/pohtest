<?php

namespace app\controllers;

use app\models\Materials;
use app\models\Mechanisms;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class SpecialController extends Controller {

    public $catparent = 62;
    public $herocustom;
    public $herobg = 1;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex() {
        return $this->redirect('/about/materials');
        //return $this->render('index', []);
    }

    public function actionMaterials($producer = null) {
        $this->herocustom = '<input id="search" data-search="material" class="search" name="search" type="text" placeholder="Поиск по материалам"/>';

        $materials = Materials::find();

        if( $producer ) {
            return $this->render('materials', [
                'materials' => $materials->andWhere(['matproducer' => $producer])->limit(16)->all(),
                'cloth' => $materials->where(['type' => 1 ])->andWhere(['matproducer' => $producer])->limit(16)->all(),
                'leather' => $materials->where(['type' => 2 ])->andWhere(['matproducer' => $producer])->limit(16)->all(),
                'wood' => $materials->where(['type' => 3 ])->andWhere(['matproducer' => $producer])->limit(16)->all(),
                'producer' => ($producer) ? $producer : false,
            ]);
        }

        return $this->render('materials', [
            'materials' => $materials->limit(16)->all(),
            'cloth' => $materials->where(['type' => 1 ])->limit(16)->all(),
            'leather' => $materials->where(['type' => 2 ])->limit(16)->all(),
            'wood' => $materials->where(['type' => 3 ])->limit(16)->all(),
            'producer' => ($producer) ? $producer : false,
        ]);
    }
    public function actionMechanisms() {
        $mechanisms = Mechanisms::find();

        return $this->render('mechanisms', [
            'mechanisms' => $mechanisms->limit(6)->all(),
            'transformations' => $mechanisms->where(['type' => 1 ])->limit(6)->all(),
            'recliners' => $mechanisms->where(['type' => 2 ])->limit(6)->all(),
            'additional' => $mechanisms->where(['type' => 3 ])->limit(6)->all(),
        ]);
    }

    public function actionMaterial($id = null) {
        if(!$id){
            return $this->redirect('/special/materials');
        }

        $this->herocustom = '<a class="btn-back" href="/special/materials/"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться к материалам</a>';

        $item = $this->findMaterial($id);

        return $this->render('material', [
            'model' => $item,
        ]);
    }

    public function actionSearch() {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Ничего не найдено.');
        }

        $query = Yii::$app->getRequest()->getQueryParam('query');

        $response = [];

        foreach(Materials::find()->all() as $model){
            $heading = preg_replace('/[^\p{L}\p{N}]/u', ' ', $model->name." ".$model->code);
            $heading_extended = preg_replace('/[^\p{L}\p{N}]/u', ' ', $model->name." ".$model->code);

            if (strpos(mb_strtolower ($heading_extended), mb_strtolower ($query)) !== false) {
                $item['value'] = $heading;
                $item['data']['id'] = $model->id;
                //$item['data']['alias'] = $model->alias;
                $item['data']['img'] = $model->image;

                $response[] = $item;
            }
        }

        $result['suggestions'] = array_slice($response, 0, 4, true);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }

    public function actionFrames() {
        return $this->render('frames', []);
    }

    protected function findMaterial($id)
    {
        if (($model = Materials::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
