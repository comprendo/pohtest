<?php

namespace app\controllers;

use app\models\Catalog;
use app\models\Categories;
use app\models\FeedbackForm;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use app\models\Settings;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller {

    public $catparent = 0;
    public $herocustom;
    public $herobg = 2;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex($category = null, $sort = null, $q = null) {
        $this->catparent = 1; // Родительская категория - каталог
        $this->herocustom = '<input id="search" data-search="catalog" class="search" name="search" type="text" placeholder="Поиск по коллекциям"/>';
        $display = Yii::$app->getRequest()->getQueryParam('display');
        $page = Yii::$app->getRequest()->getQueryParam('page');
        $limit = 9;

        $query = Catalog::find();
        // Имя текущей категории. Изначально это просто каталог
        $catname = "Коллекции";

        // Ищем категорию
        if($category){
            if (($catquery = Categories::findOne(['alias' => $category])) !== null) {
                // если нашли, то фильтруем и меняем имя категории

                //$query->andWhere(['LIKE', 'category', $catquery->id]);
                $query->andWhere(['LIKE', "concat(',',`category`,',')", ','.$catquery->id.',']);

                //var_dump($query->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);die;

                $catname = $catquery->name;
            }
        }

        if(!$category){
            switch ($display) {
                case 0:
                    $query->orWhere([explode(',', 'category')[0] => 6]);
                    $query->orWhere([explode(',', 'category')[0] => 7]);
                    $query->orWhere([explode(',', 'category')[0] => 8]);
                    $query->orWhere([explode(',', 'category')[0] => 50]);
                    $query->orWhere([explode(',', 'category')[0] => 51]);
                    $query->orWhere([explode(',', 'category')[0] => 52]);
                    $query->orWhere([explode(',', 'category')[0] => 58]);
                    $query->orWhere([explode(',', 'category')[0] => 59]);
                    break;
                case 1:
                    break;
                default:
                    $query->orWhere([explode(',', 'category')[0] => 6]);
                    $query->orWhere([explode(',', 'category')[0] => 7]);
                    $query->orWhere([explode(',', 'category')[0] => 8]);
                    $query->orWhere([explode(',', 'category')[0] => 50]);
                    $query->orWhere([explode(',', 'category')[0] => 51]);
                    $query->orWhere([explode(',', 'category')[0] => 52]);
                    $query->orWhere([explode(',', 'category')[0] => 58]);
                    $query->orWhere([explode(',', 'category')[0] => 59]);
            }
        }

        switch ($sort) {
            case 0:
                $query->orderBy(['views' => SORT_DESC]);
                break;
            case 1:
                $query->orderBy(['model' => SORT_ASC]);
                break;
            case 2:
                $query->andWhere(['isnew' => 1]);
                break;
            case 3:
                $query->orderBy(['modelsort' => SORT_DESC]);
                break;
            case 4:
                $query->orderBy([Categories::find(['id' => explode(',', 'category')[0]])->select('sort')->all() => SORT_DESC]);
                break;
            default:
                $query->orderBy(['model' => SORT_ASC]);
        }

        // $query->orderBy([Categories::find(['id' => explode(',', 'category')[0]])->select('parent')->all() => SORT_ASC]);
        // $query->where([explode(',', 'category')[0] => 7]);

        if($q){
            $query->andWhere(['like', "concat(`name`,' ',`model`,' ',`modelru`)", htmlspecialchars(mb_strtolower($q))]);
        }

        if ($page > 1) {
            $limit = $page * $limit;
        }
        
        $num_rows = $query->count();
        
        $query->limit($limit);
        
        return $this->render('index', [
            'items' => $query->all(),
            'catname' => $catname,
            'display' => ($display) ? $display : false,
            'sort' => ($sort) ? $sort : false,
            'q' => ($q) ? $q : false,
            'category' => ($catquery) ? $catquery->id : false,
            'page' => ($page) ? $page : '1',
            'num_rows' => ($num_rows) ? $num_rows : false,
        ]);
    }

    public function actionSearch() {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Ничего не найдено.');
        }

        $query = Yii::$app->getRequest()->getQueryParam('query');

        $response = [];

        foreach(Catalog::find()->all() as $model){
            $heading = preg_replace('/[^\p{L}\s]/u', '', $model->name." ".$model->model);
            $heading_extended = preg_replace('/[^\p{L}\s]/u', '', $model->name." ".$model->model." ".$model->modelru);

            if (strpos(mb_strtolower ($heading_extended), mb_strtolower ($query)) !== false) {
                $item['value'] = $heading;
                //$item['data']['id'] = $model->id;
                $item['data']['alias'] = $model->alias;
                $item['data']['img'] = $model->getPhotos()[0]->image;

                $response[] = $item;
            }
        }

        $result['suggestions'] = array_slice($response, 0, 4, true);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }


    /**
     * Показывает одну запись модели Catalog.
     * @param integer $id
     * @return mixed
     */
    public function actionModel($id) {
        $item = $this->findModelByAlias($id);


        $this->seotag = $item->seotag;

        //$category = $item->getCategory();
        //$this->herocustom = '<a class="btn-back" href="/catalog'.($category?('/'.$category->alias):'').'" onclick="historyBackFallback($(this).attr(\'href\')); return false;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться в каталог</a>';
        $this->herocustom = '<a class="btn-back" href="/catalog/" onclick="historyBackFallback($(this).attr(\'href\')); return false;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться в каталог</a>';

       // $item = $this->findModel($id);
        $item->views = $item->views+1;
        $item->save();

        $additional = Catalog::find();

        $additional->orderBy(['views' => SORT_DESC]);
        $additional->limit(3);

        //----------------------form---------------------------
        $order = new FeedbackForm();

        if ($order->load(Yii::$app->request->post()) && $order->message()) {

            return $this->render('model', [
                'model' => $item,
                'success' => true,
                'order' => $order,
                'items' => $additional->all()
            ]);

        }
        //----------------------form---------------------------

        return $this->render('model', [
            'model' => $item,
            'order' => $order,
            'items' => $additional->all()
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Catalog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
    protected function findModelByAlias($alias)
    {
        if (($model = Catalog::findOne(['alias' => $alias])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
