<?php

namespace app\controllers;

use app\models\Discount;
use app\models\Categories;
use app\models\FeedbackForm;
use Yii;
use app\components\AccessControl;
use yii\web\Controller;
use app\models\Settings;
use yii\web\Response;
use yii\web\NotFoundHttpException;

class DiscountController extends Controller
{

    public $catparent = 28;
    public $herocustom;
    public $herobg = 3;
    public $seotag;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex($category = null, $city = null, $q = null)
    {
        $this->catparent = 28; // Родительская категория - каталог
        $this->herocustom = '<input id="search" data-search="discount" class="search" name="search" type="text" placeholder="Поиск модели"/>';

        $query = Discount::find();
        // Имя текущей категории. Изначально это просто каталог
        $catname = "Дисконт";

        $query->andWhere(['active' => 1]);
        // $query->andWhere(['issold' => 0]);
        // Ищем категорию
        if ($category) {
            if (($catquery = Categories::findOne(['alias' => $category])) !== null) {
                // если нашли, то фильтруем и меняем имя категории
                $query->andWhere(['category' => $catquery->id]);
                $catname = $catquery->name;
            }
        }
        if ($city) {
            $query->andWhere(['city' => $city]);
        }/* else {
            $query->andWhere(['city' => 1]);
        }*/

        if ($q) {
            $query->andWhere(['like', "concat(`name`,' ',`model`,' ',`modelru`,' ',`itemcode`)", htmlspecialchars(mb_strtolower($q))]);
        }

        $query->orderBy(['issold' => SORT_ASC, 'views' => SORT_DESC]);
        $query->limit(9);

        return $this->render('index', [
            'items' => $query->all(),
            'catname' => $catname,
            'category' => ($catquery) ? $catquery->id : false,
            'q' => ($q) ? $q : false,
            'city' => ($city) ? $city : false,
        ]);
    }

    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Ничего не найдено.');
        }

        $query = Yii::$app->getRequest()->getQueryParam('query');

        $response = [];

        $discounts = Discount::find();
        $discounts->andWhere(['active' => 1]);
        $discounts->andWhere(['issold' => 0]);

        foreach ($discounts->all() as $model) {
            $heading = preg_replace('/[^\p{L}\s]/u', '', $model->name . " " . $model->model);
            $heading_extended = preg_replace('/[^\p{L}\s]/u', '', $model->name . " " . $model->model . " " . $model->modelru . " " . $model->itemcode);

            if (strpos(mb_strtolower($heading_extended), mb_strtolower($query)) !== false) {
                $item['value'] = $heading;
                //$item['data']['id'] = $model->id;
                $item['data']['alias'] = $model->alias;
                $item['data']['img'] = $model->getPhotos()[0]->image;
                $item['data']['pricenew'] = number_format($model->pricenew, 0, '', ' ');
                $item['data']['priceold'] = number_format($model->priceold, 0, '', ' ');

                $response[] = $item;
            }
        }

        $result['suggestions'] = array_slice($response, 0, 4, true);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $result;
    }


    /**
     * Показывает одну запись модели Discount.
     * @param integer $id
     * @return mixed
     */
    public function actionModel($id)
    {
        $item = $this->findModelByAlias($id);
        $category = $item->getCategory();
        $this->herocustom = '<a class="btn-back" href="/discount' . ($category ? ('/' . $category->alias) : '') . '" onclick="window.history.go(-1); return false;"><i class="fa fa-long-arrow-left" aria-hidden="true"></i> Вернуться в каталог</a>';

        //$item = $this->findModel($id);

        if (!$item->active)
            throw new NotFoundHttpException('Ничего не найдено.');
        // if ($item->issold)
        //     throw new NotFoundHttpException('Ничего не найдено.');

        $item->views = $item->views + 1;
        $item->save();

        $additional = Discount::find();
        $additional->andWhere(['active' => 1]);
        $additional->andWhere(['issold' => 0]);

        $additional->orderBy(['views' => SORT_DESC]);
        $additional->limit(3);

        //----------------------form---------------------------
        $order = new FeedbackForm();

        if ($order->load(Yii::$app->request->post()) && $order->message()) {

            return $this->render('model', [
                'model' => $item,
                'success' => true,
                'order' => $order,
                'items' => $additional->all()
            ]);
        }
        //----------------------form---------------------------

        return $this->render('model', [
            'model' => $item,
            'order' => $order,
            'items' => $additional->all()
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
    protected function findModelByAlias($alias)
    {
        if (($model = Discount::findOne(['alias' => $alias])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Ничего не найдено.');
        }
    }
}
