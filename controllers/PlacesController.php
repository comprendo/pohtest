<?php

namespace app\controllers;

use app\models\Places;
use app\models\Cities;
use Yii;
use app\components\AccessControl;
use app\models\ContactForm;
use yii\web\Controller;
use app\models\Settings;
use app\models\Countries;
use yii\web\NotFoundHttpException;

class PlacesController extends Controller {

    public $catparent = 0;
    public $herocustom;
    public $herobg = 7;
    public $seotag;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],

        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex($city = null, $country = 1) {
        /* Вместо этой штуки у нас теперь все города по дефолту
         * if(!$city)
            $city=1;*/

        $query = Places::find();
        $query->orderBy(['sort' => SORT_ASC]);

        if($countryId = intval(Yii::$app->getRequest()->getQueryParam('country')))
            $country = $countryId;

        if($country)
            $query->where(['country' => $country]);
        if($city)
            $query->where(['city' => $city]);

        return $this->render('index', [
            'itemsmap' => $query->all(),
            'items' => $query->limit(6)->all(),
            'city' => ($city) ? $city : false,
            'country' => ($country) ? $country : false,
        ]);
    }

}
