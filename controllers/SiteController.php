<?php

namespace app\controllers;

use app\models\Places;
use app\models\Catalog;
use app\models\Categories;
use app\models\Discount;
use app\models\Events;
use app\models\Interiors;
use app\models\Materials;
use app\models\Mechanisms;
use Yii;
use app\components\AccessControl;
use app\models\ContactForm;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{

    public $catparent = 0;
    public $herocustom;
    public $hero = '<p>Etiam nec suscipit odio. Cras dictum nisl quis sollicitudin iaculis. Fusce efficitur arcu, id aliquam purus interdum.</p>';
    public $seotag;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin', 'manager'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin', 'manager'],
                    ],
                ],
            ],

        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',

            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            /*'captcha' => [
                'class' => 'app\components\captcha\CaptchaAction',
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],*/
            'metrika' => [
                'class' => 'app\components\metrika\MetrikaAction',
                'token' => Settings::get("yandex_token"),
                'counter' => Settings::get("yandex_counter")
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = '//start';
        return $this->render('index', []);
    }

    public function actionCatalog($category = null, $display = null, $sort = null, $q = null)
    {
        return Yii::$app->runAction('catalog/index', [
            'category' => $category,
            'display' => $display,
            'sort' => $sort,
            'q' => $q,
        ]);
    }
    public function actionDiscount($category = null, $city = null, $q = null)
    {
        return Yii::$app->runAction('discount/index', [
            'category' => $category,
            'city' => $city,
            'q' => $q,
        ]);
    }

    public function actionAbout()
    {
        return Yii::$app->runAction('about/index', []);
    }

    public function actionSpecial()
    {
        return Yii::$app->runAction('special/index', []);
    }

    public function actionEvents($category = null)
    {
        return Yii::$app->runAction('events/index', [
            'category' => $category,
        ]);
    }

    public function actionInterior()
    {
        return Yii::$app->runAction('interior/index', []);
    }

    public function actionPlaces($city = null, $country = 1)
    {
        return Yii::$app->runAction('places/index', [
            'city' => $city,
        ]);
    }

    public function actionFeedback($catalog = null, $discount = null)
    {
        return Yii::$app->runAction('feedback/index', [
            'catalog' => $catalog,
            'discount' => $discount,
        ]);
    }

    public function actionAdmin()
    {
        return Yii::$app->runAction('admin/default/index', []);
    }
    public function actionCabinet()
    {
        return Yii::$app->runAction('cabinet/default/index', []);
    }
    public function actionStorage()
    {
        return Yii::$app->runAction('storage/default/index', []);
    }

    public function actionDpa()
    {
        $this->herocustom = "";
        return $this->render('dpa', []);
    }

    public function actionSitemap()
    {
        $this->herocustom = "";
        return $this->render('sitemap', []);
    }

    public function actionNewcaptcha()
    {
        $this->herocustom = "";
        return $this->render('newcaptcha', []);
    }
    /* public function actionPuzzlecaptcha() {
        $this->herocustom = "";
        return $this->render('puzzlecaptcha', []);
    }*/

    public function actionLazyload()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('Ничего не найдено.');
        }

        if (!$modelcode = Yii::$app->getRequest()->getQueryParam('code')) {
            return false;
        }
        if (!$limit = intval(Yii::$app->getRequest()->getQueryParam('limit'))) {
            return false;
        }
        if (!$page = intval(Yii::$app->getRequest()->getQueryParam('page'))) {
            return false;
        }

        switch ($modelcode) { // ленты разных моделей

            case "fegehebug8": // catalog

                $query = Catalog::find();
                // Ищем категорию
                if ($category = intval(Yii::$app->getRequest()->getQueryParam('category'))) {
                    if (($catquery = Categories::findOne($category)) !== null) {
                        // если нашли, то фильтруем и меняем имя категории
                        //$query->andWhere(['LIKE', 'category', $catquery->id]);
                        $query->andWhere(['LIKE', "concat(',',`category`,',')", ',' . $catquery->id . ',']);
                    }
                }

                if ($q = Yii::$app->getRequest()->getQueryParam('q')) {
                    $query->andWhere(['like', "concat(`name`,' ',`model`,' ',`modelru`)", htmlspecialchars(mb_strtolower($q))]);
                }

                if (!$category = intval(Yii::$app->getRequest()->getQueryParam('category'))) {
                    switch (intval(Yii::$app->getRequest()->getQueryParam('display'))) {
                        case 0:
                            $query->orWhere([explode(',', 'category')[0] => 6]);
                            $query->orWhere([explode(',', 'category')[0] => 7]);
                            $query->orWhere([explode(',', 'category')[0] => 8]);
                            $query->orWhere([explode(',', 'category')[0] => 50]);
                            $query->orWhere([explode(',', 'category')[0] => 51]);
                            $query->orWhere([explode(',', 'category')[0] => 52]);
                            $query->orWhere([explode(',', 'category')[0] => 58]);
                            $query->orWhere([explode(',', 'category')[0] => 59]);
                            break;
                        case 1:
                            break;
                        default:
                            $query->orWhere([explode(',', 'category')[0] => 6]);
                            $query->orWhere([explode(',', 'category')[0] => 7]);
                            $query->orWhere([explode(',', 'category')[0] => 8]);
                            $query->orWhere([explode(',', 'category')[0] => 50]);
                            $query->orWhere([explode(',', 'category')[0] => 51]);
                            $query->orWhere([explode(',', 'category')[0] => 52]);
                            $query->orWhere([explode(',', 'category')[0] => 58]);
                            $query->orWhere([explode(',', 'category')[0] => 59]);
                    }
                }

                switch (intval(Yii::$app->getRequest()->getQueryParam('sort'))) {
                    case 0:
                        $query->orderBy(['views' => SORT_DESC]);
                        break;
                    case 1:
                        $query->orderBy(['model' => SORT_ASC]);
                        break;
                    case 2:
                        $query->andWhere(['isnew' => 1]);
                        break;
                    case 3:
                        $query->orderBy(['modelsort' => SORT_DESC]);
                        break;
                    case 4:
                        $query->orderBy([Categories::find(['id' => explode(',', Catalog::find()->select('category'))[0]])->select('sort')->all() => SORT_DESC]);
                        break;
                    case 5:
                        $query->orWhere([explode(',', 'category')[0] => 6]);
                        $query->orWhere([explode(',', 'category')[0] => 7]);
                        $query->orWhere([explode(',', 'category')[0] => 8]);
                        $query->orWhere([explode(',', 'category')[0] => 50]);
                        $query->orWhere([explode(',', 'category')[0] => 51]);
                        $query->orWhere([explode(',', 'category')[0] => 52]);
                        $query->orWhere([explode(',', 'category')[0] => 58]);
                        $query->orWhere([explode(',', 'category')[0] => 59]);
                        $query->orderBy(['model' => SORT_ASC]);
                        break;
                    default:
                        $query->orderBy(['model' => SORT_ASC]);
                }

                // $query->orderBy([Categories::find(['id' => explode(',', Catalog::find()->select('category'))[0]])->select('parent')->all() => SORT_ASC]);
                // $query->where([explode(',', Catalog::find()->select('category'))[0] => 2]);

                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);


                $this->layout = '//empty';
                return $this->render('/lazyload/catalog', [
                    'items' => $query->all(),
                ]);

                break;

            case "xubaxust4k": // discount

                $query = Discount::find();
                // Ищем категорию
                if ($category = intval(Yii::$app->getRequest()->getQueryParam('category'))) {
                    if (($catquery = Categories::findOne($category)) !== null) {
                        // если нашли, то фильтруем и меняем имя категории
                        $query->andWhere(['category' => $category]);
                    }
                }
                if ($city = intval(Yii::$app->getRequest()->getQueryParam('city'))) {
                    $query->andWhere(['city' => $city]);
                }/* else {
                    $query->andWhere(['city' => 1]);
                }*/

                if ($q = Yii::$app->getRequest()->getQueryParam('q')) {
                    $query->andWhere(['like', "concat(`name`,' ',`model`,' ',`modelru`)", htmlspecialchars(mb_strtolower($q))]);
                }

                $query->andWhere(['active' => 1]);
                // $query->andWhere(['issold' => 0]);
                $query->orderBy(['issold' => SORT_ASC, 'views' => SORT_DESC]);
                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);


                $this->layout = '//empty';
                return $this->render('/lazyload/discount', [
                    'items' => $query->all(),
                ]);

                break;

            case "thapequ3ug": // events

                $query = Events::find();
                // Ищем категорию
                if ($category = intval(Yii::$app->getRequest()->getQueryParam('category'))) {
                    $query->andWhere(['category' => intval($category)]);
                }

                $query->orderBy(['id' => SORT_DESC]);
                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);

                $this->layout = '//empty';
                return $this->render('/lazyload/events', [
                    'items' => $query->all(),
                ]);

                break;

            case "thut7ewusp": // materials

                $query = Materials::find();
                // Ищем категорию
                if ($type = intval(Yii::$app->getRequest()->getQueryParam('type'))) {
                    $query->andWhere(['type' => intval($type)]);
                }
                // Ищем производителя
                if ($producer = intval(Yii::$app->getRequest()->getQueryParam('producer'))) {
                    $query->andWhere(['matproducer' => intval($producer)]);
                }

                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);

                $this->layout = '//empty';
                return $this->render('/lazyload/materials', [
                    'items' => $query->all(),
                ]);

                break;

            case "f3trarenux": // mechanisms

                $query = Mechanisms::find();
                // Ищем категорию
                if ($type = intval(Yii::$app->getRequest()->getQueryParam('type'))) {
                    $query->andWhere(['type' => intval($type)]);
                }

                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);

                $this->layout = '//empty';
                return $this->render('/lazyload/mechanisms', [
                    'items' => $query->all(),
                ]);

                break;

            case "hecra3uwra": // places

                $query = Places::find();
                $query->orderBy(['sort' => SORT_ASC]);

                if ($country = intval(Yii::$app->getRequest()->getQueryParam('country'))) {
                    $query->andWhere(['country' => $country]);
                }
                if ($city = intval(Yii::$app->getRequest()->getQueryParam('city'))) {
                    $query->andWhere(['city' => $city]);
                }

                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);

                $this->layout = '//empty';
                return $this->render('/lazyload/places', [
                    'items' => $query->all(),
                ]);

                break;

            case "xubt7ew3ua": // interior

                $query = Interiors::find();

                if (intval(Yii::$app->getRequest()->getQueryParam('all')) == 1) {
                    $query->limit(-1);
                } else {
                    $query->limit($limit);
                }
                $query->offset($limit * $page);

                $this->layout = '//empty';
                return $this->render('/lazyload/interior', [
                    'items' => $query->all(),
                ]);

                break;

            default:
                return false;
        }
    }

    public function actionMessageclose()
    {
        setcookie('message_close', 1, time() + 86400);
    }
}
