# Настройка проекта
1. Создайте свой файлы конфигурации на основе .example файлов

- /web/.htaccess.example ==> /.htaccess 
- /web/robots.txt.example ==> /robots.txt
- /composer.json.example ==> /composer.json
- /composer.lock.example ==> /composer.lock

2. Отредактируйте файлы в папке /config (db.php)
3. Импортируйте базу данных 
4. Запросите необходимые файлы

- /commands
- /migrations
- /components
- /tests
- /web/uploads
- /web/vendors
- /web/video
- /web/images
- /web/docs
- /yii (yii script)
- /yii.bat

5. Получите недостающие ресурсы для /components/**
6. Установите /composer.phar
7. Получите /vendor при помощи composer или запросите
8. Опубликуйте ресурсы проекта в /web/assets при помощи yii или запросите